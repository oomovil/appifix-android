package oomovil.com.appifix.principal

import android.app.Activity
import android.app.AlertDialog
import android.app.ProgressDialog
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import dmax.dialog.SpotsDialog
import io.realm.Realm
import kotlinx.android.synthetic.main.popup_codigo.*
import oomovil.com.appifix.R
import oomovil.com.appifix.general.*
import oomovil.com.appifix.models.Carrito
import oomovil.com.appifix.models.Model
import oomovil.com.appifix.oomoviews.OToast
import oomovil.com.appifix.retrofit.ApiInterface
import retrofit2.Call
import retrofit2.Response

/**
 * Created by Alan on 04/12/2017.
 */
class PopUpCodigo : Activity() {

    private var pedidoId: String? = null
    private val dialogLo: AlertDialog by lazy {
        this.dialogView()
    }

    override fun attachBaseContext(newBase: Context) {
        super.attachBaseContext(MyContextWrapper.wrap(newBase, AppConfig(newBase).language))
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.popup_codigo)
        window.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        window.attributes.windowAnimations = R.style.dialog_animation

        pedidoId = intent.getStringExtra("pedidoID")

        btn_cancelar.setOnClickListener {
            onBackPressed()
        }

        btn_aplicar.setOnClickListener {
            if (isEmpty(caja_codigo)) {
                OToast.makeText(this, getString(R.string.ingresa_codigo_valido)).show()
            } else {
                registerCupon(caja_codigo.text.toString())
            }
        }
    }

    private fun registerCupon(text: String) {
        dialogLo.show()
        val map = HashMap<String, String>()
        map.put("cupon", text)
        map.put("lang", AppConfig(this).language)
        map.put("pedido_id", pedidoId!!)
        println("PopUpCodigo.registerCupon --> $map")
        ApiInterface.create(this).regCupon(map).enqueue(object : retrofit2.Callback<Model.ResponseCupon> {
            override fun onResponse(call: Call<Model.ResponseCupon>?, response: Response<Model.ResponseCupon>?) {
                try {
                    println("PopUpCodigo.onResponse --> ${response!!.body()}")
                } catch (e: Exception) {
                    e.printStackTrace()
                }

                if (response != null && response.isSuccessful && response.body() != null) {
                    if (response.body()!!.status == 200) {
                        Realm.getDefaultInstance().executeTransactionAsync({ realmBg ->
                            val carrito = realmBg.where(Carrito::class.java).findFirst()
                            if(carrito != null) {
                                carrito.cuponId = response.body()!!.carrito.cupon_id
                                carrito.cupon = response.body()!!.carrito.cupon
                                carrito.cuponDesc = response.body()!!.carrito.cupon_desc
                                realmBg.copyToRealmOrUpdate(carrito)
                            }
                        }, {
                            dialogLo.dismiss()
                            OToast.makeText(this@PopUpCodigo, getString(R.string.cupon_registro_exito)).show()
                            onBackPressed()
                        }, {
                            dialogLo.dismiss()
                            OToast.makeText(this@PopUpCodigo, getString(R.string.error_registrar_cupon)).show()
                        })

                    } else if (response.body()!!.message == "No existe ese cupón." || response.body()!!.message == "No such coupon.") {
                        dialogLo.dismiss()
                        OToast.makeText(this@PopUpCodigo, getString(R.string.cupon_no_valido)).show()
                    } else {
                        dialogLo.dismiss()
                        val message = response.body()?.message ?: getString(R.string.error_registrar_cupon)
                        OToast.makeText(this@PopUpCodigo, message).show()
                    }
                } else {
                    dialogLo.dismiss()
                    OToast.makeText(this@PopUpCodigo, getString(R.string.error_registrar_cupon)).show()
                }
            }

            override fun onFailure(call: Call<Model.ResponseCupon>?, t: Throwable?) {
                dialogLo.dismiss()
                OToast.makeText(this@PopUpCodigo, R.string.error_conexion).show()
                t!!.printStackTrace()
            }
        })
    }

    override fun onStart() {
        super.onStart()
        KeyboardManager.init(this)
    }

}
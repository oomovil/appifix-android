package oomovil.com.appifix.principal

import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import io.realm.Realm
import io.realm.Sort
import kotlinx.android.synthetic.main.fragment_notificaciones.*
import oomovil.com.appifix.R
import oomovil.com.appifix.general.AppConfig
import oomovil.com.appifix.general.ApplicationBase
import oomovil.com.appifix.models.ImgNotis
import oomovil.com.appifix.models.Model
import oomovil.com.appifix.models.Notificacion
import oomovil.com.appifix.models.UserModel
import oomovil.com.appifix.retrofit.ApiInterface
import oomovil.com.appifix.views.AdapterNotis
import retrofit2.Call
import retrofit2.Response

/**
 * Created by Alan on 26/10/2017.
 */
class FragmentNotificaciones : Fragment() {

    private var listener: OnFragmentListener? = null

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        try {
            listener = context as OnFragmentListener
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    interface OnClickNoti {
        fun onClick(id: String)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?):
            View? = inflater!!.inflate(R.layout.fragment_notificaciones, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        recyclerNotis.layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)

        loadNotifications()
    }

    private fun loadNotifications() {
        progressNotis.visibility = View.VISIBLE
        laySinNotis.visibility = View.GONE
        recyclerNotis.visibility = View.GONE

        val map = HashMap<String, String>()
        map.put("email", Realm.getDefaultInstance().where(UserModel::class.java).findFirst()!!.email)
        map.put("lang", AppConfig(context!!).language)

        ApiInterface.create(context!!).getNotifications(map).enqueue(object : retrofit2.Callback<Model.ResponseNotis> {
            override fun onResponse(call: Call<Model.ResponseNotis>?, response: Response<Model.ResponseNotis>?) {
                try {
                    println("FragmentNotificaciones.onResponse --> ${response!!.body()}")
                } catch (e: Exception) {
                    e.printStackTrace()
                }
                if (activity != null && !activity!!.isFinishing && context != null) {
                    if (response != null && response.isSuccessful && response.body() != null && response.body()!!.status == 200) {
                        saveNotifications(response.body()!!.notis)
                    } else
                        showNotifications()
                }
            }

            override fun onFailure(call: Call<Model.ResponseNotis>?, t: Throwable?) {
                if (activity != null && !activity!!.isFinishing && context != null) {
                    showNotifications()
                }
            }

        })
    }

    private fun saveNotifications(notis: List<Notificacion>) {
        Realm.getDefaultInstance().executeTransactionAsync({ realmBG ->
            realmBG.where(Notificacion::class.java).findAll().deleteAllFromRealm()

            if (notis.isNotEmpty()) {
                notis.forEach { n ->
                    val chida = realmBG.createObject(Notificacion::class.java, ApplicationBase.idNextNoti.incrementAndGet())
                    chida.titulo = n.titulo
                    chida.mensaje = n.mensaje
                    chida.title = n.title
                    chida.message = n.message
                    chida.fechaNotif = n.fechaNotif

                    chida.fechaNotif = n.fechaNotif
                    if (n.imagen1 != null && !n.imagen1!!.isEmpty()) {
                        val im = realmBG.createObject(ImgNotis::class.java)
                        im.url = n.imagen1!!
                        chida.images.add(im)
                    }
                    if (n.imagen2 != null && !n.imagen2!!.isEmpty()) {
                        val im = realmBG.createObject(ImgNotis::class.java)
                        im.url = n.imagen2!!
                        chida.images.add(im)
                    }
                    if (n.imagen3 != null && !n.imagen3!!.isEmpty()) {
                        val im = realmBG.createObject(ImgNotis::class.java)
                        im.url = n.imagen3!!
                        chida.images.add(im)
                    }
                    if (n.imagen4 != null && !n.imagen4!!.isEmpty()) {
                        val im = realmBG.createObject(ImgNotis::class.java)
                        im.url = n.imagen4!!
                        chida.images.add(im)
                    }
                }
            }

        }, {
            showNotifications()
        }, { t ->
            t.printStackTrace()
            showNotifications()
        })
    }

    private fun showNotifications() {
        val list = Realm.getDefaultInstance().where(Notificacion::class.java).findAll().sort("fechaNotif", Sort.DESCENDING)
        if (list.isEmpty()) {
            showEmpty()
        } else {
            progressNotis.visibility = View.GONE
            laySinNotis.visibility = View.GONE
            recyclerNotis.visibility = View.VISIBLE
            recyclerNotis.adapter = AdapterNotis(list, context!!, object : OnClickNoti {
                override fun onClick(id: String) {
                    listener?.changeView(OnFragmentListener.FRAGMENT_NOTIFICACIONES_DETAIL, id)
                }

            })
        }
    }

    private fun showEmpty() {
        progressNotis.visibility = View.GONE
        laySinNotis.visibility = View.VISIBLE
        recyclerNotis.visibility = View.GONE
    }
}
package oomovil.com.appifix.principal

import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import io.realm.Realm
import oomovil.com.appifix.R
import oomovil.com.appifix.oomoviews.OToast
import oomovil.com.appifix.retrofit.ApiInterface
import retrofit2.Call
import retrofit2.Response
import android.widget.ArrayAdapter
import dmax.dialog.SpotsDialog
import kotlinx.android.synthetic.main.activity_add_location.*
import oomovil.com.appifix.general.*
import oomovil.com.appifix.models.*

class ActivityAddLocation : AppCompatActivity() {

    private var listPaises: List<Pais>? = null
    private var listEstados: List<Estado>? = null
    private var isNewLocation = false
    private var idLocation: String? = null
    private var selectedCountryID: String? = ""
    private var arrayAux: MutableList<Estado>? = null
    private val dialogLo: AlertDialog by lazy {
        this.dialogView()
    }

    override fun attachBaseContext(newBase: Context) {
        super.attachBaseContext(MyContextWrapper.wrap(newBase, AppConfig(newBase).language))

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_location)

        isNewLocation = intent.getBooleanExtra("isNewLocation", false)

        if (!isNewLocation) {
            txt_title.text = getString(R.string.editar_ubicacion)
        }

        btn_back.setOnClickListener { onBackPressed() }
        loadCountriesSpinner()

        btn_add_location.setOnClickListener {
            validarDatos()
        }

        til_pais?.onItemSelectedListener = object : AdapterView.OnItemSelectedListener{
            override fun onNothingSelected(p0: AdapterView<*>?) {
                TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
            }

            override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
                loadStatesSpinner()
            }

        }
    }

    override fun onStart() {
        super.onStart()
        KeyboardManager.init(this)
    }

    private fun loadCountriesSpinner() {
        listPaises = Realm.getDefaultInstance().where(Pais::class.java).findAll()

        if (listPaises != null && listPaises!!.isNotEmpty()) {
            val array = ArrayList<String>(listPaises!!.size)
            val config = AppConfig(applicationContext)

            if(config.language == "ES")
                listPaises = listPaises!!.sortedWith(compareBy({it.name}))
            else
                listPaises = listPaises!!.sortedWith(compareBy({it.name_eng}))

            listPaises!!.forEach {
                if(config.language == "ES") array.add(it.name!!) else array.add(it.name_eng!!)
            }


            val dataAdapter = ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, array)
            dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            til_pais.adapter = dataAdapter
            // si no es un nuevo registro, quiere decir que es de edición, por lo que hay que poner los datos del usuario
            if (!isNewLocation) {
                showDataUser()
            }
        } else {
            downloadCountries()
        }
    }

    private fun loadStatesSpinner() {

        val newSelectedCountry = listPaises!![til_pais.selectedItemPosition].id
        println("ActivityAddLocation. Selected country is $selectedCountryID and new selected country is $newSelectedCountry")

        if(!selectedCountryID!!.equals(newSelectedCountry)){
             selectedCountryID = newSelectedCountry
             val realm = Realm.getDefaultInstance()
             realm.beginTransaction()
             realm.delete(Estado::class.java)
             realm.commitTransaction()
             selectedCountryID?.let { downloadStates(it) }
         }else{
            listEstados = Realm.getDefaultInstance().where(Estado::class.java).equalTo("id_pais", selectedCountryID).findAll()
             if(listEstados != null && listEstados!!.isNotEmpty()) {
                 println("^^ Loading states for country id: $selectedCountryID")
                 val array = ArrayList<String>(listEstados!!.size)
                 val config = AppConfig(applicationContext)

                 listEstados!!.forEach{
                     if(config.language == "ES") {
                         array.add(it.name!!)
                     } else {
                         array.add(it.name_eng!!)
                     }

                 }

                 val arrayTemp = array.sorted()

                 val dataAdapter = ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, arrayTemp)
                 dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
                 til_estado.adapter = dataAdapter

                 if (!isNewLocation) {
                     val loca = Realm.getDefaultInstance().where(Locations::class.java).equalTo("id", idLocation).findFirst()
                     val estado = Realm.getDefaultInstance().where(Estado::class.java).equalTo("id", loca!!.estadoId).findFirst()

                     if (estado != null) {
                         if(config.language.equals("ES"))
                             til_estado.setSelection((til_estado.adapter as ArrayAdapter<String>).getPosition(estado.name))
                         else
                             til_estado.setSelection((til_estado.adapter as ArrayAdapter<String>).getPosition(estado.name_eng))
                     }


                     println("Location loaded: ${loca.toMyString()}")
                     println("State loaded: ${estado?.toMyString()}")


                 }
             }
         }
    }

    private fun downloadStates(countryID: String) {
        val map = HashMap<String, String>()
        println("ActivityAddLocation.onFailure --> comenzando a descargar estados")


        map.put("id_pais",countryID)
        ApiInterface.create(this).getStates(map).enqueue(object : retrofit2.Callback<Model.ResponseStates> {
            override fun onFailure(call: Call<Model.ResponseStates>?, t: Throwable?) {
                println("ActivityAddLocation.onFailure --> error al descargar estados")
                OToast.makeText(this@ActivityAddLocation, getString(R.string.error_descargar_estados)).show()
                til_estado.adapter = null
            }

            override fun onResponse(call: Call<Model.ResponseStates>?, response: Response<Model.ResponseStates>?) {

                if (response != null && response.isSuccessful && response.body() != null && response.body()!!.status == 200 && response.body()!!.states.isNotEmpty()) {
                    Realm.getDefaultInstance().use { realm ->
                        realm.executeTransactionAsync({ realmBg ->
                            System.out.println("----- Estados -----")
                            response.body()!!.states.forEach {
                                System.out.println("ID:" + it.id + "Name:" + it.name)
                                //arrayAux!!.add(it)
                                realmBg.copyToRealmOrUpdate(it)
                            }
                            System.out.println("----- Fin de estados ----")
                        }, {
                            println("ActivityAddLocation.onResponse --> estados guardados correctamente")
                            loadStatesSpinner()
                        }, { t ->
                            println("ActivityAddLocation.onResponse --> error al guardar estados")
                            t.printStackTrace()
                        })
                    }
                } else
                    til_estado.adapter = null
            }
        })
    }


    private fun showDataUser() {
        idLocation = intent.getStringExtra("idLocation")

        if (idLocation != null) {
            val loca = Realm.getDefaultInstance().where(Locations::class.java).equalTo("id", idLocation).findFirst()
            val pais = Realm.getDefaultInstance().where(Pais::class.java).equalTo("id", loca!!.paisId).findFirst()
            val state = Realm.getDefaultInstance().where(Estado::class.java).equalTo("name", loca.estado).findFirst()
            /*
            Realm.getDefaultInstance().beginTransaction()
            if (loca.estadoId == null)
                loca.estadoId = state!!.id
            Realm.getDefaultInstance().commitTransaction()
            */
            //loadStatesSpinner()
            campo_alias.setText(loca.alias)
            campo_direccion.setText(loca.direccion)
            //campo_colonia.setText(loca.colonia)
            campo_telefono.setText(loca.telefono)
            campo_ciudad.setText(loca.ciudad)
            campo_cp.setText(loca.cp)
            checkPrincipal.isChecked = loca.principal == "SI"
            if (listPaises != null && listPaises!!.isNotEmpty()) {

                if(AppConfig(applicationContext).language.equals("ES")){
                    til_pais.setSelection((til_pais.adapter as ArrayAdapter<String>).getPosition(pais?.name))
                } else {
                    til_pais.setSelection((til_pais.adapter as ArrayAdapter<String>).getPosition(pais?.name_eng))
                }
            }
            //loadStatesSpinner()
            //listEstados = Realm.getDefaultInstance().where(Estado::class.java).equalTo("id_pais", loca.paisId).findAll()
            if(listEstados != null && listEstados!!.isNotEmpty()) {

                if(AppConfig(applicationContext).language.equals("ES")) {
                    til_estado.setSelection((til_estado.adapter as ArrayAdapter<String>).getPosition(state!!.name))
                } else {
                    til_estado.setSelection((til_estado.adapter as ArrayAdapter<String>).getPosition(state!!.name_eng))
                }
            }
        }
    }

    private fun downloadCountries() {
        ApiInterface.create(this).getCountries().enqueue(object : retrofit2.Callback<Model.ResponsePaises> {
            override fun onFailure(call: Call<Model.ResponsePaises>?, t: Throwable?) {
                println("ActivityAddLocation.onFailure --> error al descargar paises")
                OToast.makeText(this@ActivityAddLocation, getString(R.string.error_descargar_paises)).show()
            }

            override fun onResponse(call: Call<Model.ResponsePaises>?, response: Response<Model.ResponsePaises>?) {
                if (response != null && response.isSuccessful && response.body() != null && response.body()!!.status == 200 && response.body()!!.paises.isNotEmpty()) {
                    Realm.getDefaultInstance().use { realm ->
                        realm.executeTransactionAsync({ realmBg ->
                            realmBg.where(Pais::class.java).findAll().deleteAllFromRealm()
                            response.body()!!.paises.forEach {
                                realmBg.copyToRealmOrUpdate(it)
                            }
                        }, {
                            println("ActivityAddLocation.onResponse --> paises guardados correctamente")
                            loadCountriesSpinner()
                        }, { t ->
                            println("ActivityAddLocation.onResponse --> error al guardar paises")
                            t.printStackTrace()
                        })
                    }
                }
            }
        })
    }

    private fun validarDatos() {
        when {
            isEmpty(campo_alias) -> OToast.makeText(this, getString(R.string.ingresa_alias_correcto)).show()
            isEmpty(campo_direccion) -> OToast.makeText(this, getString(R.string.ingresa_dire_correcta)).show()
            //isEmpty(campo_colonia) -> OToast.makeText(this, getString(R.string.ingresa_colonia_correcta)).show()
            isEmpty(campo_telefono) -> OToast.makeText(this, getString(R.string.ingresa_telefono_correcto)).show()
            isEmpty(campo_ciudad) -> OToast.makeText(this, getString(R.string.ingresa_ciudad_correcta)).show()
            isEmpty(campo_cp) -> OToast.makeText(this, getString(R.string.ingresa_cp_correcto)).show()
            (til_pais.selectedItemPosition < 0) -> OToast.makeText(this, "Selecciona un pais")
            existSameName(getText(campo_alias)!!) and isNewLocation -> OToast.makeText(this, R.string.ubicacion_mismo_nombre).show()
            else -> {
                if (isNewLocation) {
                    addLocation()
                } else {
                    updateLocation()
                }
            }
        }
    }

    private fun existSameName(name: String): Boolean = Realm.getDefaultInstance().where(Locations::class.java).equalTo("alias", name).findFirst() != null

    private fun updateLocation() {
        dialogLo.show()
        val map = HashMap<String, String>()
        map.put("lang", AppConfig(this).language)
        map.put("email", Realm.getDefaultInstance().where(UserModel::class.java).findFirst()!!.email)
        map.put("ubicacion_id", idLocation!!)
        map.put("alias", campo_alias.text.toString())
        map.put("direccion", campo_direccion.text.toString())
        //map.put("colonia", campo_colonia.text.toString())
        map.put("telefono", campo_telefono.text.toString())
        map.put("ciudad", campo_ciudad.text.toString())
        map.put("cp", campo_cp.text.toString())
        map.put("estado", til_estado.selectedItem.toString())
        map.put("estado_id", listEstados!![til_estado.selectedItemPosition].id!!)
        map.put("dir_principal", if (checkPrincipal.isChecked) "SI" else "NO")
        map.put("pais_id", listPaises!![til_pais.selectedItemPosition].id!!)
        map.put("pais", til_pais.selectedItem.toString())
        println("ActivityAddLocation.updateLocation --> $map")
        ApiInterface.create(this).updateLocation(map).enqueue(object : retrofit2.Callback<Model.Response> {
            override fun onResponse(call: Call<Model.Response>?, response: Response<Model.Response>?) {
                try {
                    println("ActivityAddLocation.onResponse --> ${response!!.body()}")
                } catch (e: Exception) {
                    e.printStackTrace()
                }
                if (response != null && response.isSuccessful && response.body() != null && response.body()!!.status == 200) {
                    saveNewLocation(idLocation!!, listPaises!![til_pais.selectedItemPosition].id!!, listEstados!![til_estado.selectedItemPosition].id!!,true)
                } else if (response != null && response.body() != null && (response.body()!!.message == "Nada actualizado en la base de datos." || response.body()!!.message == "Nothing updated in database.")) {
                    dialogLo.dismiss()
                    OToast.makeText(this@ActivityAddLocation, getString(R.string.no_cambios_ubicacion)).show()
                } else {
                    dialogLo.dismiss()
                    OToast.makeText(this@ActivityAddLocation, getString(R.string.error_actualizar_ubicacion)).show()
                }
            }

            override fun onFailure(call: Call<Model.Response>?, t: Throwable?) {
                dialogLo.dismiss()
                OToast.makeText(this@ActivityAddLocation, getString(R.string.error_conexion)).show()
            }

        })
    }

    private fun addLocation() {
        dialogLo.show()
        val map = HashMap<String, String>()
        map.put("lang", AppConfig(this).language)
        map.put("email", Realm.getDefaultInstance().where(UserModel::class.java).findFirst()!!.email)
        map.put("alias", campo_alias.text.toString())
        map.put("direccion", campo_direccion.text.toString())
        //map.put("colonia", campo_colonia.text.toString())
        map.put("telefono", campo_telefono.text.toString())
        map.put("ciudad", campo_ciudad.text.toString())
        map.put("cp", campo_cp.text.toString())
        map.put("estado", til_estado.selectedItem.toString())
        map.put("estado_id",  listEstados!![til_estado.selectedItemPosition].id!!)
        map.put("dir_principal", if (checkPrincipal.isChecked) "SI" else "NO")
        map.put("pais_id", listPaises!![til_pais.selectedItemPosition].id!!)
        map.put("pais", til_pais.selectedItem.toString())

        ApiInterface.create(this).addLocation(map).enqueue(object : retrofit2.Callback<Model.ResponseAddLocation> {
            override fun onResponse(call: Call<Model.ResponseAddLocation>?, response: Response<Model.ResponseAddLocation>?) {
                if (response != null && response.isSuccessful && response.body() != null && response.body()!!.status == 200) {
                    saveNewLocation(response.body()!!.id, listPaises!![til_pais.selectedItemPosition].id!!, listEstados!![til_estado.selectedItemPosition].id!!,false)
                } else {
                    dialogLo.dismiss()
                    println("response is {${response!!.body().toString()}}")
                    if(response.body()!!.message.contains("postal/Zip")){
                        if(AppConfig(applicationContext).language.equals("ES"))
                            OToast.makeText(this@ActivityAddLocation, "Valida el código postal").show()
                        else
                            OToast.makeText(this@ActivityAddLocation, "Review the ZIP code").show()
                    }
                    else
                        OToast.makeText(this@ActivityAddLocation, response.body()!!.message).show()
                }
            }

            override fun onFailure(call: Call<Model.ResponseAddLocation>?, t: Throwable?) {
                dialogLo.dismiss()
                OToast.makeText(this@ActivityAddLocation, getString(R.string.error_conexion)).show()
            }
        })
    }

    private fun saveNewLocation(id: String, paisId: String, estadoId: String, update: Boolean) {
        Realm.getDefaultInstance().executeTransactionAsync({ realmBG ->

            if (checkPrincipal.isChecked) {
                val allLocas = realmBG.where(Locations::class.java).findAll()
                if (allLocas != null && allLocas.isNotEmpty()) {
                    allLocas.forEach { l ->
                        l.principal = "NO"
                        realmBG.copyToRealmOrUpdate(l)
                    }
                }
            }

            var location = realmBG.where(Locations::class.java).equalTo("id", id).findFirst()
            if (!update) {
                location = realmBG.createObject(Locations::class.java, id)
            }
            location!!.alias = campo_alias.text.toString()
            location.direccion = campo_direccion.text.toString()
            location.ciudad = campo_ciudad.text.toString()
            //location.colonia = campo_colonia.text.toString()
            location.telefono = campo_telefono.text.toString()
            location.cp = campo_cp.text.toString()
            location.estado = til_estado.selectedItem.toString()
            location.estadoId = estadoId
            location.principal = if (checkPrincipal.isChecked) "SI" else "NO"
            location.paisId = paisId
            location.pais = til_pais.selectedItem.toString()
            if (update) {
                realmBG.copyToRealmOrUpdate(location)
            }
            println("New location: ${location.toString()}")
        }, {
            dialogLo.dismiss()
            txt_title.text = getString(R.string.agregar_ubicacion)
            clearCampos()
            val intent = Intent(this@ActivityAddLocation, PopUpSuccess::class.java)
            if (update)
                intent.putExtra(PopUpSuccess.TYPE, PopUpSuccess.UBICACION_UPDATE)
            else
                intent.putExtra(PopUpSuccess.TYPE, PopUpSuccess.UBICACION)
            startActivity(intent)
        }, { t ->
            dialogLo.dismiss()
            println("ActivityAddLocation.saveNewLocation --> error al guardar internamente")
            t.printStackTrace()
        })
    }

    private fun clearCampos() {
        isNewLocation = true
        campo_alias.text = null
        campo_direccion.text = null
        campo_ciudad.text = null
        campo_cp.text = null
        //campo_estado.text = null
        //campo_colonia.text = null
        campo_telefono.text = null
        checkPrincipal.isChecked = false
    }

}

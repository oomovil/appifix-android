package oomovil.com.appifix.principal

import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.app.AlertDialog
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import io.realm.Realm
import io.realm.Sort
import kotlinx.android.synthetic.main.fragment_mis_tarjetas.*
import oomovil.com.appifix.R
import oomovil.com.appifix.models.Tarjeta
import oomovil.com.appifix.oomoviews.OToast
import oomovil.com.appifix.views.AdapterTarjetas

/**
 * Created by Alan on 26/10/2017.
 */
class FragmentMisTarjetas : Fragment() {

    private var tarjetas: List<Tarjeta>? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?):
            View? = inflater.inflate(R.layout.fragment_mis_tarjetas, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        recyclerTarjetas.layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
        recyclerTarjetas.itemAnimator = DefaultItemAnimator()
        btn_add_tarjeta.setOnClickListener {
            val intent = Intent(context, ActivityAddCreditCard::class.java)
            intent.putExtra("isNewTarjeta", true)
            context!!.startActivity(intent)
        }
    }

    override fun onResume() {
        super.onResume()
        loadTarjetas()
    }

    private fun loadTarjetas() {
        tarjetas = Realm.getDefaultInstance().where(Tarjeta::class.java).findAll().sort("isFavorite", Sort.DESCENDING).toList()
        if (tarjetas != null && tarjetas!!.isNotEmpty()) {
            recyclerTarjetas.visibility = View.VISIBLE
            laySinTarjetas.visibility = View.GONE
            recyclerTarjetas.adapter = AdapterTarjetas(tarjetas, context!!, object : AdapterTarjetas.OnClick {
                override fun onCLick(id: Int, delete: Boolean) {
                    if (delete) {
                        askIfDelete(id)
                    } else {
                        setFavorite(id)
                    }
                }

            })
        } else {
            recyclerTarjetas.visibility = View.GONE
            laySinTarjetas.visibility = View.VISIBLE
        }
    }

    private fun setFavorite(id: Int) {
        val realm = Realm.getDefaultInstance()
        realm.beginTransaction()
        tarjetas!!.forEach { t ->
            t.isFavorite = t.id == id
            realm.copyToRealmOrUpdate(t)
        }
        realm.commitTransaction()
        loadTarjetas()
    }

    private fun askIfDelete(id: Int) {
        val builder = AlertDialog.Builder(context!!)
                .setIcon(R.mipmap.alerta_pop)
                .setTitle(getString(R.string.title_eliminat_tarjeta))
                .setMessage(getString(R.string.preg_eliminar_tarjeta))
                .setPositiveButton(getString(R.string.eliminar), { dialog, x ->
                    println("FragmentMisTarjetas.askIfDelete --> $x")
                    dialog.dismiss()
                    deleteTarjeta(id)
                })
                .setNegativeButton(R.string.cancelar, { dialog, x ->
                    println("FragmentMisTarjetas.askIfDelete --> $x")
                    dialog.dismiss()
                })
        val alert = builder.create()
        alert!!.show()
    }

    private fun deleteTarjeta(id: Int) {
        Realm.getDefaultInstance().executeTransactionAsync({ realmBG ->
            val tarj = realmBG.where(Tarjeta::class.java).equalTo("id", id).findFirst()
            tarj?.deleteFromRealm()
        }, {
            loadTarjetas()
        }, { t ->
            t.printStackTrace()
            OToast.makeText(context!!, R.string.error_eliminar_tarjeta).show()
        })
    }

}
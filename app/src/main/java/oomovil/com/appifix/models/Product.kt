package oomovil.com.appifix.models

import com.google.gson.annotations.SerializedName
import io.realm.RealmObject
import io.realm.annotations.PrimaryKey
import org.json.JSONObject

/**
 * Created by Alan on 24/11/2017.
 */
open class Product : RealmObject() {
    @SerializedName("stock")
    var stock: Int = 0
    @SerializedName("id")
    var id: String = "-1"
    @SerializedName("producto")
    var nameEs: String = ""
    @SerializedName("prod_eng")
    var nameEng: String = ""
    @SerializedName("detalles")
    var detalleEs: String = ""
    @SerializedName("details")
    var detalleEng: String = ""
    @SerializedName("imagen")
    var imagen: String = ""
    @SerializedName("precio")
    var precioPublico: String = "0.0"
    @SerializedName("precio_distri")
    var precioDistribuidores: String = "0.0"
    @SerializedName("categoria_id")
    var categoriaID: String = "-1"
    @SerializedName("descuento")
    var descuento: String? = null

    companion object {
        fun create(json: JSONObject): Product {
            val p = Product()
            try {
                p.categoriaID = json.getString("categoria_id")
                p.descuento = json.getString("descuento")
                p.detalleEng = json.getString("details")
                p.detalleEs = json.getString("detalles")
                p.precioDistribuidores = json.getString("precio_distri")
                p.precioPublico = json.getString("precio")
                p.imagen = json.getString("imagen")
                p.nameEng = json.getString("prod_eng")
                p.nameEs = json.getString("producto")
                p.id = json.getString("id")
                p.stock = json.getInt("stock")
            } catch (e: Exception){
                e.printStackTrace()
            }
            return p
        }
    }

}
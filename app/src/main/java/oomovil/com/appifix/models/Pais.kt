package oomovil.com.appifix.models

import com.google.gson.annotations.SerializedName
import io.realm.RealmObject
import io.realm.annotations.PrimaryKey

/**
 * Created by Alan on 13/11/2017.
 */
open class Pais: RealmObject() {
    @PrimaryKey
    @SerializedName("id")
    var id: String? = null
    @SerializedName("pais")
    var name: String? = null
    @SerializedName("pais_eng")
    var name_eng: String? = null
}
package oomovil.com.appifix.views

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.chauthai.swipereveallayout.ViewBinderHelper
import io.realm.Realm
import io.realm.RealmList
import kotlinx.android.synthetic.main.item_carrito.view.*
import kotlinx.android.synthetic.main.item_locations.view.*
import oomovil.com.appifix.R
import oomovil.com.appifix.general.*
import oomovil.com.appifix.models.Product
import oomovil.com.appifix.models.ProductoCarrito
import oomovil.com.appifix.models.UserModel
import oomovil.com.appifix.models.UserType
import oomovil.com.appifix.principal.ActivityAddCarrito
import java.util.*
import android.os.Handler

/**
 * Created by Alan on 30/11/2017.
 */
class AdapterCarrito(private var items: RealmList<ProductoCarrito>, val context: Context, val click: OnClick) : RecyclerView.Adapter<AdapterCarrito.ViewHolder>() {
    override fun onBindViewHolder(p0: ViewHolder, p1: Int) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    private val config = AppConfig(context)
    private val user = Realm.getDefaultInstance().where(UserModel::class.java).findFirst()
    var flag = false
    var cont = 0

    interface OnClick {
        fun onCLick(id: String, idCategoria: String)
        fun onCLick(id: String, idCategoria: String, count: Int)
    }

    private var viewBinderHelper = ViewBinderHelper()

    init {
        viewBinderHelper.setOpenOnlyOne(true)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder = ViewHolder(parent.inflate(R.layout.item_carrito, false))

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: ViewHolder, position: Int, payloads: MutableList<Any>) {
        // producto del carrito
        val prodC = items[position]
        // producto chido original
        val pro = Realm.getDefaultInstance().where(Product::class.java).equalTo("id", prodC!!.id).findFirst()

        viewBinderHelper.bind(holder.itemView.swipeRevealLayoutCar, items[position]!!.id)

        Handler().postDelayed({
            viewBinderHelper.openLayout(items[position]!!.id)
            Handler().postDelayed({
                viewBinderHelper.closeLayout(items[position]!!.id)
            }, 500)
        }, 250)

        holder.itemView.btn_editar_c.setColorFilter(Color.WHITE)
        holder.itemView.btn_borrar_c.setColorFilter(Color.WHITE)

        holder.itemView.img_prod.loadUrl(pro!!.imagen, R.drawable.img_no_disponible, null)
        holder.itemView.txt_name_prod.text = if (config.language == "ES") pro.nameEs else pro.nameEng
        val descrip = if (config.language == "ES") pro.detalleEs else pro.detalleEng
        holder.itemView.descrip_prod.text = getTextFromHtml(descrip)
        holder.itemView.txt_count.text = prodC!!.count
        calculateTotal(pro, prodC.count.toInt(), holder.itemView.total_prod)



        holder.itemView.btn_editar_c.setOnClickListener {
            holder.itemView.btn_editar_c.isClickable = !(holder.itemView.btn_editar_c.hasOnClickListeners())
            println("AdapterCarrito.onBindViewHolder --> ${prodC.id} --> ${prodC.count}")
            click.onCLick(prodC.id, pro.categoriaID, prodC.count.toInt())

        }
        holder.itemView.btn_borrar_c.setOnClickListener {
            click.onCLick(pro.id, pro.categoriaID)
        }
    }

    @SuppressLint("SetTextI18n")
    private fun calculateTotal(prod: Product, numProdus: Int, txt_total: TextView) {
        var descuento = 0.0
        val precio: Double = if (user!!.getUserType() == UserType.NORMAL) prod.precioPublico.toDouble() else prod.precioDistribuidores.toDouble() //TODO WATCH THIS
        /**
         * Quite multiplicacion de descuento
         */
        /*
        if (prod.descuento != null && prod.descuento != "0.00") {
            var desc = round(prod.descuento!!)
            desc /= 100
            descuento = desc * precio
        }
        */
        var total = precio - descuento
        total *= numProdus
        total = total //TODO WATCH THIS
        val descChido = "" + if (isDoubleInteger(total)) formatNumberDecimal(total.toInt()) else {
            formatNumberDecimal(total)
        }
        txt_total.text = "$$descChido"
    }

    private fun isDoubleInteger(value: Double): Boolean {
        val integer = (value % 1)
        return integer == 0.0
    }

    override fun getItemCount() = items.size

    class ViewHolder(val view: View) : RecyclerView.ViewHolder(view)

}
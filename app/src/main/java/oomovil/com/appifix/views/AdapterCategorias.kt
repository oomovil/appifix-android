package oomovil.com.appifix.views

import android.annotation.SuppressLint
import android.content.Context
import android.os.Build
import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import com.squareup.picasso.Picasso
import io.realm.RealmResults
import oomovil.com.appifix.R
import oomovil.com.appifix.models.Categoria
import kotlinx.android.synthetic.main.item_categorias.view.*
import oomovil.com.appifix.general.AppConfig
import oomovil.com.appifix.general.inflate
import oomovil.com.appifix.general.loadUrl
import oomovil.com.appifix.principal.OnFragmentListener

/**
 * Created by Alan on 23/10/2017.
 */
class AdapterCategorias(private var items: ArrayList<Categoria>, val context: Context, private val itemClick: OnRecyclerItemClick) : RecyclerView.Adapter<AdapterCategorias.ViewHolder>() {
    override fun onBindViewHolder(p0: ViewHolder, p1: Int) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    private val config = AppConfig(context)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder = ViewHolder(parent.inflate(R.layout.item_categorias, false))

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: ViewHolder, position: Int, payloads: MutableList<Any>) {
        val cate = items[position]

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            holder.itemView.txt_cate.transitionName = "transitionTxt" + holder.adapterPosition
        }

        holder.itemView.txt_cate.text = if (config.language == "ES") cate.name else cate.nameEn
        holder.itemView.txt_cate_count.text = "(" + cate.cantProds + " " + context.getString(R.string.articulos) + ")"
        holder.itemView.txt_cate_count.visibility = View.GONE
        Picasso.get().load(cate.urlFoto!!).error(R.drawable.degradado_azul).into(holder.itemView.img_cate)
        //holder.itemView.img_cate.loadUrl(cate.urlFoto!!, R.drawable.img_no_disponible, null)

        holder.itemView.layContainer.setOnClickListener {
            if (cate.padre == "1") {
                itemClick.onClick(OnFragmentListener.FRAGMENT_CATEGORIA, cate.id!!, cate.cantProds!!, cate.name!!)
            } else {
                itemClick.onClick(OnFragmentListener.FRAGMENT_PRODUCTOS, cate.id!!,
                        listOf(Pair(holder.itemView.txt_cate, "transitionTxt" + holder.adapterPosition)),
                        listOf(Pair("titulo", if (config.language == "ES") cate.name else cate.nameEn), Pair("transitionNameTxt", "transitionTxt" + holder.adapterPosition)))
            }
        }
    }

    override fun getItemCount() = items!!.size

    class ViewHolder(val view: View) : RecyclerView.ViewHolder(view)
}
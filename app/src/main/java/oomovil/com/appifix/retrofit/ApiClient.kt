package oomovil.com.appifix.retrofit

import android.content.Context
import com.google.android.gms.security.ProviderInstaller
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.security.NoSuchAlgorithmException
import javax.net.ssl.SSLContext
import com.google.gson.GsonBuilder
import com.google.gson.Gson
import retrofit2.converter.scalars.ScalarsConverterFactory
import java.util.concurrent.TimeUnit

/**
 * Created by Alan on 16/10/2017.
 */
class ApiClient {
    companion object {
        val BASE_URL: String = "http://appifixdev.oomovil.x10.mx/adm/KUFF_Api/"
        //val BASE_URL: String = "http://appifix.com/adm/KUFF_Api/"
        //private val BASE_URL: String = "https://porvenirapps.com/appifix/KUFF_Api/"
        //private val BASE_URL: String = "http://192.168.100.20:8888/appifix/KUFF_Api/"

        fun getClient(cxt: Context): Retrofit {
            initializeSSLContext(cxt)
            val httpClient: OkHttpClient.Builder = OkHttpClient.Builder()
            httpClient.addInterceptor(interceptor)
            httpClient.connectTimeout(1,TimeUnit.MINUTES)
            httpClient.readTimeout(1,TimeUnit.MINUTES)
            httpClient.writeTimeout(1,TimeUnit.MINUTES)
            val client = httpClient.build()
            val gson = GsonBuilder()
                    .setLenient()
                    .create()
            val retro: Retrofit.Builder = Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .client(client)
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .addConverterFactory(ScalarsConverterFactory.create())
            return retro.build()
        }

        private fun initializeSSLContext(cxt: Context) {
            try {
                SSLContext.getInstance("TLSv1.2")
            } catch (e: Exception) {
                e.printStackTrace()
            }
            try {
                ProviderInstaller.installIfNeeded(cxt.applicationContext)
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }

        private val interceptor = Interceptor { chain ->
            val request = chain.request()
            request
                    ?.newBuilder()
                    ?.header("Accept", "application/json")
                    ?.addHeader("Accept", "application/x-www-form-urlencoded")
                    ?.addHeader("Content-Type", "application/x-www-form-urlencoded")
                    ?.addHeader("Authorization", "auth-token")
                    ?.method(request.method(), request.body())
                    ?.build()
            chain.proceed(request)
        }
    }

}
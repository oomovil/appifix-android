package oomovil.com.appifix.views

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.os.Build
import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import com.squareup.picasso.Picasso
import io.realm.Realm
import kotlinx.android.synthetic.main.item_categorias.view.*
import kotlinx.android.synthetic.main.item_producto.view.*
import oomovil.com.appifix.R
import oomovil.com.appifix.general.*
import oomovil.com.appifix.models.Categoria
import oomovil.com.appifix.models.Product
import oomovil.com.appifix.models.UserModel
import oomovil.com.appifix.models.UserType
import oomovil.com.appifix.principal.ActivityAddCarrito
import oomovil.com.appifix.principal.OnFragmentListener

class AdapterCatAndProds(private val items: ArrayList<Any>, val context: Context, private val itemClick: OnRecyclerItemClick, private val addCarrito: (String, String) -> Unit) : RecyclerView.Adapter<AdapterCatAndProds.ViewHolder>() {

    private val config = AppConfig(context)
    private val user = Realm.getDefaultInstance().where(UserModel::class.java).findFirst()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return if(viewType == 1)
            ViewHolder(parent.inflate(R.layout.item_categorias))
        else
            ViewHolder(parent.inflate(R.layout.item_producto))
    }

    override fun getItemViewType(position: Int): Int {
        return if (items[position] is Categoria)
            1
        else
            2
    }

    override fun getItemCount(): Int = items.size

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        if(holder.itemViewType == 1) {
            val cate = items[position] as Categoria

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                holder.itemView.txt_cate.transitionName = "transitionTxt" + holder.adapterPosition
            }

            holder.itemView.txt_cate.text = if (config.language == "ES") cate.name else cate.nameEn
            holder.itemView.txt_cate_count.text = "(" + cate.cantProds + " " + context.getString(R.string.articulos) + ")"
            holder.itemView.txt_cate_count.visibility = View.GONE
            Picasso.get().load(cate.urlFoto!!).error(R.drawable.degradado_azul).into(holder.itemView.img_cate)

            holder.itemView.layContainer.setOnClickListener {
                if (cate.padre == "1") {
                    itemClick.onClick(OnFragmentListener.FRAGMENT_CATEGORIA, cate.id!!, cate.cantProds!!, cate.name!!)
                } else {
                    itemClick.onClick(OnFragmentListener.FRAGMENT_PRODUCTOS, cate.id!!,
                            listOf(Pair(holder.itemView.txt_cate, "transitionTxt" + holder.adapterPosition)),
                            listOf(Pair("titulo", if (config.language == "ES") cate.name else cate.nameEn), Pair("transitionNameTxt", "transitionTxt" + holder.adapterPosition)))
                }
            }

        } else {
            val pro = items[position] as Product

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                holder.itemView.img_prod.transitionName = "transitionImg" + holder.adapterPosition
                holder.itemView.txt_precio.transitionName = "transitionTxtPrecio" + holder.adapterPosition
                holder.itemView.title_prod.transitionName = "transitionTxtTitle" + holder.adapterPosition
            }

            val precio: Double = if (user!!.getUserType() == UserType.NORMAL) round(pro.precioPublico) else round(pro.precioDistribuidores)

            holder.itemView.txt_precio.text = "$" + if (isDoubleInteger(precio)) formatNumberDecimal(precio.toInt()) else formatNumberDecimal(precio)
            if (pro.descuento != null && pro.descuento != "0.00") {
                val desc = round(pro.descuento!!)
                val descChido = "" + if (isDoubleInteger(desc)) formatNumberDecimal(desc.toInt()) else {
                    formatNumberDecimal(desc)
                }
                holder.itemView.txt_desc.text = "$descChido% ${if (config.language == "ES") " desc" else " off"}"
            } else {
                holder.itemView.txt_desc.visibility = View.GONE
            }
            holder.itemView.img_prod.loadUrl(pro.imagen, R.drawable.img_no_disponible, null)
            holder.itemView.title_prod.text = if (config.language == "ES") pro.nameEs else pro.nameEng

            holder.itemView.btn_add_carrito.setOnClickListener {
                addCarrito.invoke(pro.id, pro.categoriaID)
                /*val intent = Intent(context, ActivityAddCarrito::class.java)
                intent.putExtra("id", pro.id)
                intent.putExtra("idCategoria", pro.categoriaID)
                intent.flags = Intent.FLAG_ACTIVITY_NO_ANIMATION
                intent.putExtra(ActivityAddCarrito.INTENT_ACTION, ActivityAddCarrito.ADD)
                context.startActivity(intent)*/
            }

            holder.itemView.cardProd.setOnClickListener {
                itemClick.onClick(OnFragmentListener.FRAGMENT_PRODUCTO_DETALLE, pro.id + "¬oOMovil¬" + pro.categoriaID,
                        listOf(Pair(holder.itemView.img_prod, "transitionImg" + holder.adapterPosition), Pair(holder.itemView.txt_precio, "transitionTxtPrecio" + holder.adapterPosition), Pair(holder.itemView.title_prod, "transitionTxtTitle" + holder.adapterPosition)),
                        listOf(Pair("imgUrl", pro.imagen), Pair("transitionNameImg", "transitionImg" + holder.adapterPosition), Pair("transitionTxtPrecio", "transitionTxtPrecio" + holder.adapterPosition), Pair("transitionTxtTitle", "transitionTxtTitle" + holder.adapterPosition)))
            }
        }
    }

    private fun isDoubleInteger(value: Double): Boolean {
        val integer = (value % 1)
        println("AdapterProducto.isDoubleInteger $integer --> ${integer == 0.0}")
        return integer == 0.0
    }

    class ViewHolder(val view: View) : RecyclerView.ViewHolder(view)
}
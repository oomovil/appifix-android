package oomovil.com.appifix.principal

import android.animation.AnimatorSet
import android.animation.ObjectAnimator
import android.annotation.SuppressLint
import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.os.Handler
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import android.view.animation.Animation
import android.view.animation.TranslateAnimation
import android.widget.ImageView
import android.widget.RelativeLayout
import com.android.volley.DefaultRetryPolicy
import com.android.volley.Request
import com.android.volley.toolbox.StringRequest
import dmax.dialog.SpotsDialog
import io.realm.Realm
import kotlinx.android.synthetic.main.activity_realizar_compra.*
import kotlinx.android.synthetic.main.custom_toast.*
import oomovil.com.appifix.R
import oomovil.com.appifix.general.*
import oomovil.com.appifix.models.*
import oomovil.com.appifix.oomoviews.OToast
import oomovil.com.appifix.retrofit.ApiClient
import oomovil.com.appifix.retrofit.ApiInterface
import oomovil.com.appifix.retrofit.VolleyS
import oomovil.com.appifix.views.AdapterLocationsBuy
import oomovil.com.appifix.views.AdapterPayment
import oomovil.com.appifix.views.AdapterShippingBuy
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Response

/**
 * Created by Alan on 05/12/2017.
 */
class ActivityRealizarCompra : AppCompatActivity(), View.OnClickListener, PopUpPaqueterias.onPaqSelectedPopup {

    private var sections: Sections = Sections.ONLY // porque solo se utiliza esta vez
    private var submitPressed: Boolean = false

    enum class Sections {
        ONLY, LOCATIONS, SHIPPING, PAYMENT
    }

    interface OnClickSelected {
        fun onCLick(id: String, selected: Boolean)
    }

    private val dialogLo: AlertDialog by lazy {
        this.dialogView()
    }

    private var cardseletect: Boolean = false
    private var cargoAct: Double = 0.0
    private var idEnvio: String? = null
    private var shortName: String? = null
    private var cargo: String? = null
    private var dias: String? = null
    private var namePaqueteria: String = ""
    private var defaultID = "-1"
    private var idLocationSelected = defaultID
    private var idCardSelected = defaultID
    private var canFinish = false
    private var total = ""
    private val realm: Realm by lazy {
        Realm.getDefaultInstance()
    }
    private val user: UserModel? by lazy {
        realm.where(UserModel::class.java).findFirst()
    }

    override fun attachBaseContext(newBase: Context) {
        super.attachBaseContext(MyContextWrapper.wrap(newBase, AppConfig(newBase).language))
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_realizar_compra)

        recyclerSelector.layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)

        btn_back.setOnClickListener(this)
        btn_dire.setOnClickListener(this)
        btn_envio.setOnClickListener(this)
        btn_pago.setOnClickListener(this)
        btn_cambiar_envio.setOnClickListener(this)
        btn_finalizar.setOnClickListener(this)

        this.total = intent.extras.getString("total").replace("$", "", false).replace(",", "", false)
    }

    override fun onPostCreate(savedInstanceState: Bundle?) {
        super.onPostCreate(savedInstanceState)
        showLayLocations()
    }

    override fun onResume() {
        super.onResume()

        if (recyclerSelector.adapter != null) {
            if (sections == Sections.LOCATIONS)
                recyclerSelector.adapter = AdapterLocationsBuy(getLocations(), this, listener)
            else if (sections == Sections.PAYMENT)
                recyclerSelector.adapter = AdapterPayment(getPayments(), this, listener)
        }
        validateToFinish()
    }

    private fun validateToFinish() {
        canFinish = (idCardSelected != defaultID && idLocationSelected != defaultID)
        moveBtnFinish(canFinish)
    }

    override fun onClick(v: View) {
        when (v.id) {
            R.id.btn_back -> {
                onBackPressed()
            }
            R.id.btn_dire -> {
                showLayLocations()
            }
            R.id.btn_envio -> {
                showLayShipping()
            }
            R.id.btn_pago -> {
                showLayPayment()
            }
            R.id.btn_cambiar_envio -> {
                PopUpPaqueterias.delegatePop = this
                val intent = Intent(this@ActivityRealizarCompra, PopUpPaqueterias::class.java)
                intent.putExtra("idOrden", realm.where(Carrito::class.java).findFirst()!!.pedidoID.toString())
                intent.putExtra("idUbicacion", idLocationSelected)
                startActivity(intent)
            }
            R.id.btn_finalizar -> {
                if (canFinish) {
                    realizarCompra()
                }
            }
        }
    }

    private fun realizarCompra() {

        validateShipping()

        dialogLo.show()
        val tj = realm.where(Tarjeta::class.java).equalTo("id", idCardSelected.toInt()).findFirst()
        val tarjeta = Tarjeta()
        tarjeta.cardOwnerName = tj!!.cardOwnerName
        tarjeta.cardName = tj.cardName
        tarjeta.cardNumber = tj.cardNumber
        tarjeta.cardCvv = tj.cardCvv
        tarjeta.expiryDate = tj.expiryDate
        tarjeta.idLoc = tj.idLoc
        val userId = user!!.idUser.toString()

        val post = object : StringRequest(Request.Method.POST, ApiClient.BASE_URL + "addOrden", { response ->
            if (response.isNotEmpty()) {
                dialogLo.dismiss()
                println("ActivityRealizarCompra.realizarCompra --> $response")
                val msg = response
                try {
                    val json: JSONObject = JSONObject(response)

                    if (json.has("status") && json.getInt("status") == 200 && json.getString("ok") == "1") {
                        realm.beginTransaction()
                        realm.where(Carrito::class.java).findAll().deleteAllFromRealm()
                        realm.where(ProductoCarrito::class.java).findAll().deleteAllFromRealm()
                        realm.commitTransaction()
                        OToast.makeText(this@ActivityRealizarCompra, getString(R.string.exito_compra)).show()
                        this@ActivityRealizarCompra.finish()
                    } else {
                        var msgs: String = ""
                        when(json.getString("mensaje")){
                            "The credit card number is invalid." -> msgs = getString(R.string.The_credit_card_number_is_invalid)
                            "A duplicate transaction has been submitted." -> msgs = getString(R.string.A_duplicate_transaction_has_been_submitted)
                        }
                        //.replace(" ","_")
                        println("Error Response --> $msgs")
                        OToast.makeText(this@ActivityRealizarCompra, msgs).show()
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                    OToast.makeText(this@ActivityRealizarCompra, getString(R.string.error_procesar_pago)).show()
                }
            } else {
                dialogLo.dismiss()
                OToast.makeText(this@ActivityRealizarCompra, getString(R.string.error_procesar_pago)).show()
            }
        }, {
            it.printStackTrace()
            dialogLo.dismiss()
            OToast.makeText(this@ActivityRealizarCompra, it.toString()).show()
        }) {
            override fun getParams(): MutableMap<String, String> {
                val map = HashMap<String, String>()
                map["idUsuario"] = userId
                map["tipoPago"] = "1"
                if (idLocationSelected == "0") {
                    map["idUbicacion"] = "NO"
                    map["ProductShortName"] = "Entrega en local"
                    map["ShippingCharge"] = "1"
                    map["GlobalProductCode"] = "1"
                    map["TotalTransitDays"] = "1"
                } else {
                    map["idUbicacion"] = idLocationSelected
                    map["ProductShortName"] = shortName!!
                    map["ShippingCharge"] = cargo!!
                    map["GlobalProductCode"] = idEnvio!!
                    map["TotalTransitDays"] = if (dias == null || dias == "") "- -" else dias!!
                }
                map["idUbicacion_tarjeta"] = tarjeta.idLoc ?: ""
                map["ShipmentType"] = namePaqueteria
                map["aliasc"] = tarjeta.cardName!!
                map["nombre"] = tarjeta.cardOwnerName!!
                map["n"] = tarjeta.cardNumber!!.replace(" ", "")
                val cardDate = tarjeta.expiryDate!!.split("/")
                map["m"] = cardDate[0]
                map["y"] = "20" + cardDate[1]
                map["c"] = tarjeta.cardCvv!!
                map["lang"] = "ES"
                println("ActivityRealizarCompra.getParams --> $map")
                return map
            }

        }
        post.retryPolicy = DefaultRetryPolicy(20 * 1000, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT)
        VolleyS(this).getRequest().add(post)

    }

    private fun showLayLocations() {
        if (sections == Sections.LOCATIONS)
            return
        sections = Sections.LOCATIONS
        hideAndGray()
        img_dire.colorFilter = null
        txt_dire.setTextColor(Color.parseColor("#006eba"))
        img_dire.animateView()
        btn_dire.getRelativeXLocationOnScreen()
        recyclerSelector.adapter = AdapterLocationsBuy(getLocations(), this, listener)
    }

    private fun showLayShipping() {
        if (sections == Sections.SHIPPING)
            return
        sections = Sections.SHIPPING
        hideAndGray()
        img_envio.colorFilter = null
        txt_envio.setTextColor(Color.parseColor("#006eba"))
        img_envio.animateView()
        btn_envio.getRelativeXLocationOnScreen()
        recyclerSelector.adapter = AdapterShippingBuy(getShippings(), this)
    }

    private fun showLayPayment() {
        if (sections == Sections.PAYMENT)
            return
        sections = Sections.PAYMENT
        hideAndGray()
        img_pago.colorFilter = null
        txt_pago.setTextColor(Color.parseColor("#006eba"))
        img_pago.animateView()
        btn_pago.getRelativeXLocationOnScreen()
        recyclerSelector.adapter = AdapterPayment(getPayments(), this, listener)
    }

    private val listener: OnClickSelected = object : OnClickSelected, PopUpPaqueterias.onPaqSelectedPopup {
        override fun onPaqSelectedPopup(idPaqueteria: String, shortName: String, cargo: String, dias: String, name: String) {
            TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
        }

        override fun onCLick(id: String, selected: Boolean) {
            if (sections == Sections.LOCATIONS) {
                idEnvio = null
                layTotalesFin.visibility = View.GONE
                idLocationSelected = if (selected) id else defaultID
                if (idLocationSelected != defaultID) showLayPayment()
                if (idLocationSelected != defaultID && idCardSelected != defaultID) realizarCompra()
            } else if (sections == Sections.PAYMENT) {
                idCardSelected = if (selected) id else defaultID
                if (idCardSelected != defaultID && idLocationSelected == defaultID) {
                    showLayLocations()
                } else if (idCardSelected != defaultID && idLocationSelected != defaultID && !cardseletect) {
                    validateShipping()
                    cardseletect = true
                } else if (idCardSelected != defaultID && idLocationSelected != defaultID && cardseletect) {
                    reloadServie()
                }

            }
            validateToFinish()
        }
    }

    private fun hideAndGray() {
        img_dire.animation?.cancel()
        img_envio.animation?.cancel()
        img_pago.animation?.cancel()
        img_dire.setColorFilter(Color.GRAY)
        txt_dire.setTextColor(Color.GRAY)
        img_envio.setColorFilter(Color.GRAY)
        txt_envio.setTextColor(Color.GRAY)
        img_pago.setColorFilter(Color.GRAY)
        txt_pago.setTextColor(Color.GRAY)
    }

    private fun ImageView.animateView() {
        val anim1 = ObjectAnimator.ofFloat(this, "rotation", 0f, -50f)
        val anim2 = ObjectAnimator.ofFloat(this, "rotation", -50f, 50f)
        val anim4 = ObjectAnimator.ofFloat(this, "rotation", 50f, 0f)
        val animatorSet = AnimatorSet()
        animatorSet.duration = 150
        animatorSet.playSequentially(anim1, anim2, anim4)
        animatorSet.start()
    }

    private fun RelativeLayout.getRelativeXLocationOnScreen() {
        this.post {
            val location = kotlin.IntArray(2)
            this.getLocationOnScreen(location)
            val w = this.width
            val realPosition = ((w / 2) + location[0]) - oomovil.com.appifix.general.dpToPx(10, context)
            val anim = ObjectAnimator.ofFloat(circle_indicator, "x", realPosition.toFloat())
            anim.duration = 200
            anim.start()
        }
    }

    private fun moveBtnFinish(canF: Boolean) {
        val startScale = if (canF) 1.5f else 0f
        val endScale = if (canF) 0f else 1.5f
        val visibility = if (canF) View.VISIBLE else View.GONE
        if (visibility != btn_finalizar.visibility) {
            val anim = TranslateAnimation(
                    Animation.RELATIVE_TO_SELF, 0f,
                    Animation.RELATIVE_TO_SELF, 0f,
                    Animation.RELATIVE_TO_SELF, startScale,
                    Animation.RELATIVE_TO_SELF, endScale)
            anim.duration = 300
            anim.setAnimationListener(object : Animation.AnimationListener {
                override fun onAnimationRepeat(p0: Animation?) {}
                override fun onAnimationEnd(p0: Animation?) {
                    btn_finalizar.visibility = visibility
                }

                override fun onAnimationStart(p0: Animation?) {
                    if (visibility == View.VISIBLE)
                        btn_finalizar.visibility = visibility
                    Handler().postDelayed({
                        btn_finalizar.visibility = visibility
                    }, anim.duration)
                }
            })
            btn_finalizar.startAnimation(anim)
        }
    }

    private fun getLocations(): List<MakePayment.Address> {
        val locas = realm.where(Locations::class.java).findAll()
        val chidas = ArrayList<MakePayment.Address>()
        chidas.add(MakePayment.Address(null, MakePayment.AddressType.TITLE, false))
        if (locas.isNotEmpty()) {
            locas.forEach {
                chidas.add(MakePayment.Address(it, MakePayment.AddressType.EXISTING, it.id == idLocationSelected))
            }
        }
        //chidas.add(MakePayment.Address(null, MakePayment.AddressType.LOCAL, idLocationSelected == "0"))
        chidas.add(MakePayment.Address(null, MakePayment.AddressType.NEW, false))
        return chidas
    }

    private fun getShippings(): List<MakePayment.Shipping> {
        val chidas = ArrayList<MakePayment.Shipping>()
        chidas.add(MakePayment.Shipping(MakePayment.ShippingType.TITLE, "", 0, 0.0, false))
        chidas.add(MakePayment.Shipping(MakePayment.ShippingType.NORMAL, "DHL", R.mipmap.logo_dhl, 68.0, false))
        return chidas
    }

    private fun getPayments(): List<MakePayment.Payment> {
        val chidas = ArrayList<MakePayment.Payment>()
        val tarjetas = realm.where(Tarjeta::class.java).findAll()
        chidas.add(MakePayment.Payment(MakePayment.PaymentType.TITLE, false, Tarjeta()))
        if (tarjetas.isNotEmpty()) {
            tarjetas.forEach {
                chidas.add(MakePayment.Payment(MakePayment.PaymentType.CREDIT_CARD, it.id.toString() == idCardSelected, it))
            }
        }
        chidas.add(MakePayment.Payment(MakePayment.PaymentType.NEW, false, Tarjeta()))
        return chidas
    }

    private fun isDoubleInteger(value: Double): Boolean {
        val integer = (value % 1)
        return integer == 0.0
    }

    @SuppressLint("SetTextI18n")
    override fun onPaqSelectedPopup(idPaqueteria: String, shortName: String, cargo: String, dias: String, name: String) {


        val tarjeta = realm.where(Tarjeta::class.java).equalTo("id", idCardSelected.toInt()).findFirst()
        println("IdLOC: " + tarjeta?.idLoc.toString())
        this.idEnvio = idPaqueteria
        this.shortName = shortName
        this.cargo = cargo
        this.cargoAct = cargo.toDouble()
        this.dias = dias
        this.namePaqueteria = name
        txt_envio_costo.text = "$" + formatNumberDecimal(this.cargo!!.toDouble())

        showProgress()
        val map = HashMap<String, String?>()
        map["idUsuario"] = user!!.idUser.toString()
        map["idUbicacion"] = tarjeta?.idLoc.toString()
        val call = ApiInterface.create(this).getOrderTotal(map)
        call.enqueue(object : retrofit2.Callback<Model.ResponseOrderTotal> {
            override fun onFailure(call: Call<Model.ResponseOrderTotal>?, t: Throwable?) {
                hideProgress()
                OToast.makeText(this@ActivityRealizarCompra, R.string.error_conexion).show()
            }

            override fun onResponse(call: Call<Model.ResponseOrderTotal>?, response: Response<Model.ResponseOrderTotal>?) {
                try {
                    println("ActivityAddCarrito.onResponse --> ${response!!.body()}")
                } catch (e: Exception) {
                    e.printStackTrace()
                }
                hideProgress()
                if (response != null && response.isSuccessful && response.body() != null && response.body()!!.status == 200) {
                    val totalOrder = response.body()!!.total
                    total = totalOrder.toString()
                    val total = total.toDouble() + cargo.toDouble()
                    val totalT = "" + if (isDoubleInteger(total)) formatNumberDecimal(total.toInt()) else formatNumberDecimal(total)
                    txt_total_fin.text = "$$totalT"
                    layTotalesFin.visibility = View.VISIBLE

                } else {
                    OToast.makeText(this@ActivityRealizarCompra, R.string.error_servidor).show()
                }
            }

        })

    }

    private fun showProgress() {
        dialogLo.show()
        btn_finalizar.visibility = View.GONE
    }

    private fun hideProgress() {
        dialogLo.dismiss()
        btn_finalizar.visibility = View.VISIBLE
    }

    private fun reloadServie() {

        val tarjeta = realm.where(Tarjeta::class.java).equalTo("id", idCardSelected.toInt()).findFirst()

        showProgress()
        val map = HashMap<String, String?>()
        map["idUsuario"] = user!!.idUser.toString()
        map["idUbicacion"] = tarjeta?.idLoc.toString()
        val call = ApiInterface.create(this).getOrderTotal(map)
        call.enqueue(object : retrofit2.Callback<Model.ResponseOrderTotal> {
            override fun onFailure(call: Call<Model.ResponseOrderTotal>?, t: Throwable?) {
                hideProgress()
                OToast.makeText(this@ActivityRealizarCompra, R.string.error_conexion).show()
            }

            override fun onResponse(call: Call<Model.ResponseOrderTotal>?, response: Response<Model.ResponseOrderTotal>?) {
                try {
                    println("ActivityAddCarrito.onResponse --> ${response!!.body()}")
                } catch (e: Exception) {
                    e.printStackTrace()
                }
                hideProgress()
                if (response != null && response.isSuccessful && response.body() != null && response.body()!!.status == 200) {
                    val totalOrder = response.body()!!.total
                    total = totalOrder.toString()
                    val total = total.toDouble() + cargoAct
                    val totalT = "" + if (isDoubleInteger(total)) formatNumberDecimal(total.toInt()) else formatNumberDecimal(total)
                    var txtaux = "$$totalT"
                    txt_total_fin.text = txtaux
                    layTotalesFin.visibility = View.VISIBLE

                } else {
                    OToast.makeText(this@ActivityRealizarCompra, R.string.error_servidor).show()
                }
            }

        })
    }

    private fun validateShipping() {
        if (idEnvio == null) {
            PopUpPaqueterias.delegatePop = this
            val intent = Intent(this@ActivityRealizarCompra, PopUpPaqueterias::class.java)
            intent.putExtra("idOrden", realm.where(Carrito::class.java).findFirst()!!.pedidoID.toString())
            intent.putExtra("idUbicacion", idLocationSelected)
            startActivity(intent)
            return
        }
    }
}
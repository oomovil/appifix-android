package oomovil.com.appifix.principal

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.view.ActionMode
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import io.realm.Realm
import kotlinx.android.synthetic.main.fragment_categorias.*
import oomovil.com.appifix.R
import oomovil.com.appifix.retrofit.ApiInterface
import oomovil.com.appifix.views.AdapterCategorias
import oomovil.com.appifix.views.OnRecyclerItemClick
import retrofit2.Call
import retrofit2.Response
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.GridLayoutManager
import com.facebook.internal.Mutable
import io.realm.Sort
import kotlinx.android.synthetic.main.popup_codigo.*
import oomovil.com.appifix.general.AppConfig
import oomovil.com.appifix.models.*
import oomovil.com.appifix.views.AdapterCatAndProds


/**
 * Created by Alan on 23/10/2017.
 */
class FragmentCategorias : Fragment(), OnRecyclerItemClick, Principal.onRemoveLast {

    private var listenerFragment: OnFragmentListener? = null
    private val FRAGMENT_STATE = "FragmentCategorias"

    companion object {
        var listSubcat: MutableList<String> = mutableListOf()
        var listSubcatCount: MutableList<String> = mutableListOf()
        var listNombresSubcat: MutableList<String> = mutableListOf()
    }

    private val config: AppConfig by lazy {
        AppConfig(context!!)
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        try {
            listenerFragment = context as OnFragmentListener
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? = inflater!!.inflate(R.layout.fragment_categorias, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        Principal.delegateRemove = this
        Principal.inProducts = false

        if (arguments!!.getBoolean("clean", false)) {
            listSubcat.clear()
            listNombresSubcat.clear()
            listSubcatCount.clear()
        }

        if (listNombresSubcat.size > 0) {
            Principal.inSubs = true
            (activity as Principal).txt_title.text = listNombresSubcat.last()
        }

        recyclerCate.layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
        refresh.setOnRefreshListener {
            loadCategorias()
        }

        btn_reload.setOnClickListener {
            loadCategorias()
        }
        if (!Principal.bundle.containsKey(FRAGMENT_STATE)) {
            println("FragmentCategorias.onViewCreated --> instance null")
            loadCategorias()
            Principal.bundle.putBoolean(FRAGMENT_STATE, true)

        } else {
            println("FragmentCategorias.onViewCreated --> instance not null")
            showCategorias(false)
        }
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        println("FragmentCategorias.onSaveInstanceState --> guardar el estado")
        outState.putString(FRAGMENT_STATE, "creado")
    }

    override fun onDestroyView() {
        println("FragmentCategorias.onDestroyView --> vista destruida")
        onSaveInstanceState(Bundle())
        super.onDestroyView()
    }

    private fun loadCategorias() {

        layErrorCates.visibility = View.GONE
        recyclerCate.visibility = View.GONE
        progressCate.visibility = View.VISIBLE

        val map = HashMap<String, String>()
        if (listSubcat.size > 0) {
            map["id_papa"] = listSubcat.last()
        } else {
            map["id_papa"] = "2"
            listSubcatCount.clear()
        }
        ApiInterface.create(context!!).getCategorias(map).enqueue(object : retrofit2.Callback<Model.ResponseCategorias> {
            override fun onFailure(call: Call<Model.ResponseCategorias>?, t: Throwable?) {
                println("FragmentCategorias.onFailure")
                t!!.printStackTrace()
                if (activity != null && !activity!!.isFinishing && context != null) {
                    showCategorias(true)
                }
            }

            override fun onResponse(call: Call<Model.ResponseCategorias>?, response: Response<Model.ResponseCategorias>?) {
                if (activity != null && !activity!!.isFinishing && context != null) {
                    if (response != null && response.isSuccessful && response.body() != null ) {
                        Realm.getDefaultInstance().use { realm ->
                            realm.executeTransactionAsync({ realmBg ->
                                realmBg.where(Categoria::class.java).findAll().deleteAllFromRealm()
                                response.body()!!.prefs.forEach {
                                    println("FragmentCategorias.onResponse --> ${it.id} --> ${it.name} --> ${it.nameEn} --> ${it.padre}")
                                    if (it.name != null && it.name!!.toLowerCase() != "root") {
                                        realmBg.copyToRealmOrUpdate(it)
                                    }
                                }
                            }, {
                                println("FragmentCategorias.onResponse --> categorias guardadas")
                                if (listSubcat.size > 0) {
                                    getProdsFromCats(listSubcat.last())
                                } else {
                                    showCategorias(false)
                                }
                            }, { t ->
                                println("FragmentCategorias.onResponse --> error al guardar las categorias")
                                t.printStackTrace()
                                showCategorias(false)
                            })
                        }
                    } else {
                        println("FragmentCategorias.onResponse --> no se descargaron las categorias")
                        showCategorias(false)
                    }
                }
            }
        })
    }

    private fun getProdsFromCats(last: String) {
        println("FragmentCategorias.getProdsFromCats --> $last")
        println("FragmentCategorias.getProdsFromCats --> Name: ${listSubcat.last()} --> Prods: ${listSubcatCount.last()}")
        if (listSubcatCount.last().toInt() > 0) {
            println("FragmentCategorias.getProdsFromCats --> buscar productos")
            loadProductos(last)
        } else {
            println("FragmentCategorias.getProdsFromCats --> buscar categorías")
            showCategorias(false)
        }
    }

    private fun loadProductos(idCat: String) {
        val map = HashMap<String, String>()
        map.put("cat_id", idCat)
        map.put("lang", "ES") // porque siempre manda las dos versiones
        map.put("email", Realm.getDefaultInstance().where(UserModel::class.java).findFirst()!!.email)
        val call = ApiInterface.create(context!!).getProdsFromCat(map)
        call.enqueue(object : retrofit2.Callback<Model.ResponseProductFromCategory> {
            override fun onResponse(call: Call<Model.ResponseProductFromCategory>?, response: Response<Model.ResponseProductFromCategory>?) {
                if (activity != null && !activity!!.isFinishing && context != null) {
                    if (response != null && response.isSuccessful && response.body() != null && response.body()!!.status == 200 && response.body()!!.productos.isNotEmpty()) {
                        saveProductsFromCat(response.body()!!.productos, idCat)
                    } else {
                        println("FragmentCategorias.onResponse --> error al buscar productos")
                        showCategorias(false)
                    }
                }
            }

            override fun onFailure(call: Call<Model.ResponseProductFromCategory>?, t: Throwable?) {
                t?.printStackTrace()
                if (activity != null && !activity!!.isFinishing && context != null) {
                    showCategorias(false)
                }
            }
        })
    }

    private fun saveProductsFromCat(productos: List<Product>, idCat: String) {
        Realm.getDefaultInstance().executeTransactionAsync({ realmBg ->
            realmBg.where(Product::class.java).equalTo("categoriaID", idCat).findAll().deleteAllFromRealm()
            productos.forEach { pro ->
                val p = realmBg.createObject(Product::class.java)
                p.id = pro.id
                p.nameEs = pro.nameEs
                p.nameEng = pro.nameEng
                p.detalleEs = pro.detalleEs
                p.detalleEng = pro.detalleEng
                p.imagen = pro.imagen
                p.precioPublico = pro.precioPublico
                p.precioDistribuidores = pro.precioDistribuidores
                p.categoriaID = pro.categoriaID
                p.descuento = pro.descuento
            }
        }, {
            showProducts(idCat)
        }, { t ->
            t.printStackTrace()
            showProducts(idCat)
        })
    }

    private fun showProducts(idCat: String) {
        val categorias = Realm.getDefaultInstance().where(Categoria::class.java).findAll()
        if (categorias != null && categorias.isNotEmpty()) {
            println("FragmentCategorias.showProducts --> mostrar categorías: ${categorias.size} y productos: ${listSubcatCount.last()}")
            val pros = Realm.getDefaultInstance().where(Product::class.java).equalTo("categoriaID", idCat).findAll()
            if (pros != null && pros.isNotEmpty()) {
                val layoutManager = GridLayoutManager(context, 2)
                val list: ArrayList<Any> = arrayListOf()
                categorias.forEach { c ->
                    list.add(c)
                }
                pros.forEach { p ->
                    list.add(p)
                }

                layoutManager.spanSizeLookup = object : GridLayoutManager.SpanSizeLookup() {
                    override fun getSpanSize(position: Int): Int {
                        return try {
                            if (list[position] is Categoria)
                                2
                            else
                                1
                        } catch (e: Exception) {
                            e.printStackTrace()
                            2
                        }
                    }
                }

                recyclerCate.layoutManager = layoutManager

                progressCate.visibility = View.GONE
                refresh.visibility = View.VISIBLE
                recyclerCate.visibility = View.VISIBLE
                layErrorCates.visibility = View.GONE
                refresh.isRefreshing = false
                recyclerCate.adapter = AdapterCatAndProds(list, context!!, this) { id, categoriaID ->
                    val intent = Intent(context, ActivityAddCarrito::class.java)
                    intent.putExtra("id", id)
                    intent.putExtra("idCategoria", categoriaID)
                    intent.flags = Intent.FLAG_ACTIVITY_NO_ANIMATION
                    intent.putExtra(ActivityAddCarrito.INTENT_ACTION, ActivityAddCarrito.ADD)
                    startActivityForResult(intent, 150)
                }
            } else {
                showCategorias(false)
            }
        } else {
            println("FragmentCategorias.showProducts --> mostrar categorías")
            showEmpty()
        }
    }

    private fun showCategorias(isErroServer: Boolean) {
        // ordenar por preferencias
        val nameToSort = if (config.language == "ES") "name" else "nameEn"
        val categorias = Realm.getDefaultInstance().where(Categoria::class.java).findAll().sort(nameToSort, Sort.ASCENDING)
        if (categorias != null && categorias.isNotEmpty()) {
            val preferencias = Realm.getDefaultInstance().where(Preferences::class.java).equalTo("isSelected", true).findAll().sort(nameToSort, Sort.ASCENDING)
            val newArray = ArrayList<Categoria>()
            if (preferencias.isNotEmpty()) {
                preferencias.forEach { p ->
                    val cate = categorias.where().equalTo("id", p.id).findFirst()
                    if (cate != null)
                        newArray.add(cate)
                }
            }
            categorias.forEach { c ->
                if (!newArray.contains(c))
                    newArray.add(c)
            }

            progressCate.visibility = View.GONE
            refresh.visibility = View.VISIBLE
            recyclerCate.visibility = View.VISIBLE
            layErrorCates.visibility = View.GONE
            refresh.isRefreshing = false
            recyclerCate.layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
            recyclerCate.adapter = AdapterCategorias(newArray, context!!, this)
        } else if (isErroServer) {
            showError()
        } else {
            showEmpty()
        }
    }

    private fun showError() {
        refresh.isRefreshing = false
        refresh.visibility = View.GONE
        progressCate.visibility = View.GONE
        layErrorCates.visibility = View.VISIBLE
        txt_error.text = getString(R.string.error_servidor)
    }

    private fun showEmpty() {
        refresh.isRefreshing = false
        refresh.visibility = View.GONE
        progressCate.visibility = View.GONE
        layErrorCates.visibility = View.VISIBLE
        txt_error.text = getString(R.string.error_cargar_categorias)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 150 && resultCode == Activity.RESULT_OK) {
            listenerFragment?.changeView(OnFragmentListener.FRAGMENT_CARRITO, "-1")
        }
    }

    override fun onClick(fragment: Int, id: String, viewList: List<Pair<View, String>>, data: List<Pair<String, String?>>) {
        Principal.bundle.remove(FRAGMENT_STATE)
        Principal.inProducts = true
        if (listenerFragment != null) {
            listenerFragment!!.changeViewTransition(fragment, id, viewList, data)
        }
    }

    override fun onClick(fragment: Int, id: String, idCategoria: String, nombreCat: String) {
        (activity as Principal).txt_title.text = nombreCat
        Principal.inSubs = true
        listSubcat.add(id)
        // en este caso idCategoria es la cantidad de productos
        listSubcatCount.add(idCategoria)
        listNombresSubcat.add(nombreCat)
        loadCategorias()
    }

    override fun onRemoveLast() {
        listSubcat.removeAt(listSubcat.size - 1)
        listSubcatCount.removeAt(listSubcatCount.size - 1)
        listNombresSubcat.removeAt(listNombresSubcat.size - 1)
        if (listNombresSubcat.size == 0) {
            (activity as Principal).txt_title.text = getString(R.string.nav_home)
        } else {
            (activity as Principal).txt_title.text = listNombresSubcat.last()
        }
        if (listSubcat.size == 0) {
            Principal.inSubs = false
        }
        loadCategorias()
    }

    override fun onRemoveAll() {
        listSubcat.clear()
        listSubcatCount.clear()
        listNombresSubcat.clear()
        Principal.inSubs = false
        if (context != null && activity != null)
            loadCategorias()
    }

}
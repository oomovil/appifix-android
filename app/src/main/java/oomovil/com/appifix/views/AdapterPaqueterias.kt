package oomovil.com.appifix.views

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Color
import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.item_location_buy.view.*
import kotlinx.android.synthetic.main.item_paqueteria.view.*
import kotlinx.android.synthetic.main.item_recoger_local.view.*
import oomovil.com.appifix.R
import oomovil.com.appifix.general.formatNumberDecimal
import oomovil.com.appifix.general.inflate
import oomovil.com.appifix.models.Envio

/**
 * Created by Martin on 23/02/18.
 */
class AdapterPaqueterias(private val items: List<Envio>?, val context: Context, val delegate: onPaqSelected) : RecyclerView.Adapter<AdapterPaqueterias.ViewHolder>() {

    override fun onBindViewHolder(p0: ViewHolder, p1: Int) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    var last: Boolean = false

    interface onPaqSelected {
        fun onPaqSelected(idPaqueteria: String?, shortName: String, cargo: String, days: String, name: String)
    }

    override fun getItemViewType(position: Int): Int {
        if (position == items!!.size) {
            return 100
        }
        return super.getItemViewType(position)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        if (viewType == 100) {
            return AdapterPaqueterias.ViewHolder(parent.inflate(R.layout.item_recoger_local, false))
        } else {
            return AdapterPaqueterias.ViewHolder(parent.inflate(R.layout.item_paqueteria, false))
        }
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: ViewHolder, position: Int, payloads: MutableList<Any>) {
        if (position == items!!.size) {
            if (last) {
                holder.itemView.btn_check_local.setColorFilter(Color.parseColor("#3e4e94"))
            } else {
                holder.itemView.btn_check_local.colorFilter = null
            }
            holder.itemView.layLocal.setOnClickListener {
                items.forEach { l ->
                    l.isSelected = false
                }
                last = !last
                notifyDataSetChanged()

                if (last) {
                    delegate.onPaqSelected("LOCAL", "LOCAL", "0.0", "0.0", "local")
                } else {
                    delegate.onPaqSelected(null, "LOCAL", "", "", "local")
                }
            }
        } else {
            val envio = items[position]
            if (envio.paqueteria!!.toLowerCase() == "dhl") {
                holder.itemView.img_paqueteria.setImageResource(R.mipmap.logo_dhl)
            } else if (envio.paqueteria!!.toLowerCase() == "fedex") {
                holder.itemView.img_paqueteria.setImageResource(R.mipmap.logo_fedex)
            }
            holder.itemView.txt_name_envio.text = envio.nombre
            holder.itemView.txt_dias.text = "${envio.dias} día de entrega aprox."
            val price: Double = (envio.precio ?: "0.0").toDouble()
            holder.itemView.txt_precio.text = "Costo: $${formatNumberDecimal(price)}"

            if (envio.isSelected) {
                holder.itemView.btn_check_paq.setColorFilter(Color.parseColor("#3e4e94"))
            } else {
                holder.itemView.btn_check_paq.colorFilter = null
            }

            holder.itemView.layPaqueterias.setOnClickListener {
                val select = !envio.isSelected
                last = false
                items.forEach { l ->
                    l.isSelected = false
                }
                envio.isSelected = select
                notifyDataSetChanged()
                if (select) {
                    delegate.onPaqSelected(envio.codigo!!, envio.nombre!!, envio.precio!!, envio.dias!!, envio.paqueteria!!.toLowerCase())
                } else {
                    delegate.onPaqSelected(null, envio.nombre!!, envio.precio!!, envio.dias!!, envio.paqueteria!!.toLowerCase())
                }

            }
        }
    }

    override fun getItemCount() = items!!.size + 1

    class ViewHolder(val view: View) : RecyclerView.ViewHolder(view)
}
package oomovil.com.appifix.views

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.support.v4.content.ContextCompat.startActivity
import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.item_carrito.view.*
import kotlinx.android.synthetic.main.item_heade_detalle_pedido.view.*
import kotlinx.android.synthetic.main.item_info_pedido.view.*
import oomovil.com.appifix.R
import oomovil.com.appifix.general.inflate
import oomovil.com.appifix.general.loadUrl
import oomovil.com.appifix.models.DetallePedido
import oomovil.com.appifix.models.Producto
import android.content.ClipData
import android.content.ClipboardManager
import android.widget.Toast
import oomovil.com.appifix.general.setUnderLine


/**
 * Created by Martin on 22/02/18.
 */
class AdapterDetallePedido(private var items: List<Producto>?, private var detalle: DetallePedido?, val context: Context) : RecyclerView.Adapter<AdapterDetallePedido.ViewHolder>() {
    override fun onBindViewHolder(p0: ViewHolder, p1: Int) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun getItemViewType(position: Int): Int {
        if (position == 0 || position == (items!!.size + 1)) {
            return 100
        } else if (position > (items!!.size + 1)) {
            return 101
        }
        return super.getItemViewType(position)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return when (viewType) {
            100 -> AdapterDetallePedido.ViewHolder(parent.inflate(R.layout.item_heade_detalle_pedido, false))
            101 -> AdapterDetallePedido.ViewHolder(parent.inflate(R.layout.item_info_pedido, false))
            else -> AdapterDetallePedido.ViewHolder(parent.inflate(R.layout.item_producto_historial, false))
        }
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: ViewHolder, position: Int, payloads: MutableList<Any>) {
        when (position) {
            0 -> holder.itemView.tituloHeader.text = context.getString(R.string.productos_gio)
            (items!!.size + 1) -> holder.itemView.tituloHeader.text = context.getString(R.string.resumen)
            (items!!.size + 2) -> {
                holder.itemView.tituloInfo.text = context.getString(R.string.numero_orden)
                holder.itemView.mensajeInfo.text = detalle!!.orden
            }
            (items!!.size + 3) -> {
                holder.itemView.tituloInfo.text = context.getString(R.string.date)
                holder.itemView.mensajeInfo.text = detalle!!.fecha
            }
            (items!!.size + 4) -> {
                holder.itemView.tituloInfo.text = context.getString(R.string.alias_c)
                holder.itemView.mensajeInfo.text = detalle!!.aliasTarjeta
            }
            (items!!.size + 5) -> {
                holder.itemView.tituloInfo.text = context.getString(R.string.location_alias)
                holder.itemView.mensajeInfo.text = detalle!!.aliasUbicacion
            }
            (items!!.size + 6) -> {
                holder.itemView.tituloInfo.text = "Impuestos"
                holder.itemView.mensajeInfo.text = "$${detalle!!.impuestos}"
            }
            (items!!.size + 7) -> {
                holder.itemView.tituloInfo.text = "Subtotal"
                holder.itemView.mensajeInfo.text = "$${detalle!!.total_prod}"
            }
            (items!!.size + 8) -> {
                holder.itemView.tituloInfo.text = context.getString(R.string.tipo_envio)
                holder.itemView.mensajeInfo.text = detalle!!.tipo_envio
            }
            (items!!.size + 9) -> {
                holder.itemView.tituloInfo.text = context.getString(R.string.costo_envio)
                holder.itemView.mensajeInfo.text = "$${detalle!!.total_envio}"
            }
            (items!!.size + 10) -> {
                holder.itemView.tituloInfo.text = context.getString(R.string.tiempo_entrega)
                holder.itemView.mensajeInfo.text = detalle!!.dias
            }
            (items!!.size + 11) -> {
                holder.itemView.tituloInfo.text = "Cupon"
                holder.itemView.mensajeInfo.text = detalle!!.cupon
            }
            (items!!.size + 12) -> {
                holder.itemView.tituloInfo.text = "Descuento"
                holder.itemView.mensajeInfo.text = detalle!!.descuento
            }
            (items!!.size + 13) -> {
                holder.itemView.tituloInfo.text = "Total"
                holder.itemView.mensajeInfo.text = "$${detalle!!.total}"
            }
            (items!!.size + 14) -> {
                holder.itemView.tituloInfo.text = context.getString(R.string.num_ratreo)
                holder.itemView.mensajeInfo.text = context.resources.getString(R.string.numero_rastreo, detalle!!.num_rastreo)
                holder.itemView.mensajeInfo.setUnderLine()
                holder.itemView.mensajeInfo.setOnClickListener {
                    if (detalle!!.type == "fedex" || detalle!!.type == "dhl") {
                        val clipboard = context.getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager
                        val clip = ClipData.newPlainText("num_rastreo", detalle!!.num_rastreo)
                        clipboard.primaryClip = clip
                        Toast.makeText(context, context.getString(R.string.num_rastreo_copiado), Toast.LENGTH_LONG).show()
                        val urlRastreo = when(detalle!!.type) {
                            "fedex" -> "https://www.fedex.com/es-us/tracking.html"
                            "dhl" -> "http://www.dhl.com.mx/es/express/rastreo.html"
                            else -> ""
                        }
                        val i = Intent(Intent.ACTION_VIEW, Uri.parse(urlRastreo))
                        startActivity(context, i, null)
                    }
                }
            }
            else -> {
                val producto = items!![position - 1]
                holder.itemView.img_prod.loadUrl(producto!!.imagen, R.drawable.img_no_disponible, null)
                holder.itemView.txt_count.text = producto.cantidad
                holder.itemView.descrip_prod.text = producto.producto
                holder.itemView.total_prod.visibility = View.INVISIBLE
            }
        }
    }

    override fun getItemCount() = items!!.size + 15

    class ViewHolder(val view: View) : RecyclerView.ViewHolder(view)
}
package oomovil.com.appifix.general

import android.app.Activity
import android.view.MotionEvent
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.EditText

/**
 * Created by Alan on 02/11/2017.
 */
class KeyboardManager {
    companion object Factory {
        private var inputMethodManager: InputMethodManager? = null

        fun init(activity: Activity) {
            inputMethodManager = activity.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
            val view = activity.window.decorView.rootView
            setupUI(view, activity)
        }

        fun init(activity: Activity, view: View?) {
            inputMethodManager = activity.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
            setupUI(view, activity)
        }

        private fun setupUI(view: View?, activity: Activity) {
            if (view != null) {
                if (view !is EditText) {
                    var startX = 0f
                    var startY = 0f
                    view.setOnTouchListener { _, event ->
                        when (event.action) {
                            MotionEvent.ACTION_DOWN -> {
                                startX = event.x
                                startY = event.y
                            }
                            MotionEvent.ACTION_UP -> {
                                val endX = event.x
                                val endY = event.y
                                if (isAClick(startX, endX, startY, endY)) {
                                    showHide(activity)
                                }
                            }
                        }
                        false
                    }
                }
            }
        }

        private fun isAClick(startX: Float, endX: Float, startY: Float, endY: Float): Boolean {
            val differenceX = Math.abs(startX - endX)
            val differenceY = Math.abs(startY - endY)
            return !(differenceX > 5 || differenceY > 5)
        }

        private fun showHide(activity: Activity) {
            if (activity.currentFocus != null) {
                inputMethodManager!!.hideSoftInputFromWindow(activity.currentFocus!!.windowToken, 0)
            }
        }
    }
}
package oomovil.com.appifix.general

import android.content.Context
import android.os.StrictMode
import android.support.multidex.MultiDex
import android.support.multidex.MultiDexApplication
import com.facebook.FacebookSdk
import io.realm.Realm
import io.realm.RealmConfiguration
import oomovil.com.appifix.models.Notificacion
import oomovil.com.appifix.models.Tarjeta
import java.util.concurrent.atomic.AtomicInteger

/**
 * Created by Alan on 16/10/2017.
 */
class ApplicationBase : MultiDexApplication() {

    companion object {
        var idNext = AtomicInteger()
        var idNextNoti = AtomicInteger()
        fun getIdByTable() : AtomicInteger {
            val result = Realm.getDefaultInstance().where(Tarjeta::class.java).findAll()
            return if (result.isEmpty()) AtomicInteger() else AtomicInteger(result.max("id")!!.toInt())
        }
        fun getIdByNoti() : AtomicInteger {
            val result = Realm.getDefaultInstance().where(Notificacion::class.java).findAll()
            return if (result.isEmpty()) AtomicInteger() else AtomicInteger(result.max("id")!!.toInt())
        }
    }

    override fun onCreate() {
        super.onCreate()
        val builder: StrictMode.VmPolicy.Builder = StrictMode.VmPolicy.Builder()
        StrictMode.setVmPolicy(builder.build())

        Realm.init(this)

        val configuration: RealmConfiguration = RealmConfiguration.Builder()
                .schemaVersion(2)
                .name("db.appifix")
                .deleteRealmIfMigrationNeeded()
                .build()
        Realm.setDefaultConfiguration(configuration)

        idNext = getIdByTable()
        idNextNoti = getIdByNoti()
    }

    override fun attachBaseContext(base: Context?) {
        super.attachBaseContext(base)
        MultiDex.install(base)
    }

}
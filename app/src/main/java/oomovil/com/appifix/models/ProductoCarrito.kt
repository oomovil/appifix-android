package oomovil.com.appifix.models

import com.google.gson.annotations.SerializedName
import io.realm.RealmObject
import io.realm.annotations.PrimaryKey

/**
 * Created by Alan on 28/11/2017.
 */
open class ProductoCarrito : RealmObject() {
    @PrimaryKey
    @SerializedName("producto_id")
    var id: String = "-1"
    @SerializedName("cantidad")
    var count: String = "0"

    @SerializedName("producto")
    var nameEs: String = ""
    @SerializedName("prod_eng")
    var nameEng: String = ""
    @SerializedName("detalles")
    var detalleEs: String = ""
    @SerializedName("details")
    var detalleEng: String = ""
    @SerializedName("imagen")
    var imagen: String = ""
    @SerializedName("precio_publico")
    var precioPublico: String = "0.0"
    @SerializedName("precio_distri")
    var precioDistribuidores: String = "0.0"
    @SerializedName("categoria_id")
    var categoriaID: String = "-1"
    @SerializedName("descuento")
    var descuento: String? = null
    @SerializedName("stock")
    var stock: Int = 0

    override fun toString(): String = "ProductoCarrito(id='$id', count='$count')"
}
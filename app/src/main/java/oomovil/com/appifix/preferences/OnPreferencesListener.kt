package oomovil.com.appifix.preferences

/**
 * Created by Alan on 18/10/2017.
 */
interface OnPreferencesListener {
    fun onClick(position : Int)
}
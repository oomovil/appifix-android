package oomovil.com.appifix.firebase

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.graphics.BitmapFactory
import android.media.RingtoneManager
import android.os.Build
import android.support.v4.app.NotificationCompat
import android.support.v4.app.NotificationCompat.*
import android.support.v4.content.ContextCompat.getSystemService
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import io.realm.Realm
import oomovil.com.appifix.R
import oomovil.com.appifix.general.AppConfig
import oomovil.com.appifix.general.ApplicationBase
import oomovil.com.appifix.general.getCurrentDate
import oomovil.com.appifix.models.ImgNotis
import oomovil.com.appifix.models.Notificacion
import oomovil.com.appifix.principal.OnFragmentListener
import oomovil.com.appifix.principal.Principal

/**
 * Created by Alan on 16/11/2017.
 */
class MyFirebaseMessagingService : FirebaseMessagingService() {

    private val config: AppConfig by lazy {
        AppConfig(this)
    }

    override fun onMessageReceived(remoteMessage: RemoteMessage?) {
        if (remoteMessage != null) {
            val data = remoteMessage.data
            if (AppConfig(baseContext).userSession) {
                // TODO -  enviar notificacion
                try {
                    println("MyFirebaseMessagingService.onMessageReceived --> ${data.getValue("titulo")}")
                    println("MyFirebaseMessagingService.onMessageReceived --> ${data.getValue("mensaje")}")
                    println("MyFirebaseMessagingService.onMessageReceived --> ${data.getValue("title")}")
                    println("MyFirebaseMessagingService.onMessageReceived --> ${data.getValue("message")}")

                    if (data.containsKey("imagen1"))
                        println("MyFirebaseMessagingService.onMessageReceived --> ${data.getValue("imagen1")}")
                    if (data.containsKey("imagen2"))
                        println("MyFirebaseMessagingService.onMessageReceived --> ${data.getValue("imagen2")}")
                    if (data.containsKey("imagen3"))
                        println("MyFirebaseMessagingService.onMessageReceived --> ${data.getValue("imagen3")}")
                    if (data.containsKey("imagen4"))
                        println("MyFirebaseMessagingService.onMessageReceived --> ${data.getValue("imagen4")}")
                } catch (e: Exception) {
                    e.printStackTrace()
                    println(e.message) 
                }

                saveAndShowNotification(data)
            }
        }
    }

    private fun saveAndShowNotification(data: Map<String, String>) {
        val realm = Realm.getDefaultInstance()
        realm.beginTransaction()
        var notifi = realm.where(Notificacion::class.java)
                .equalTo("titulo", data.getValue("titulo"))
                .equalTo("mensaje", data.getValue("mensaje"))
                .equalTo("title", data.getValue("title"))
                .equalTo("message", data.getValue("message"))
                .findFirst()
        if (notifi == null) {
            notifi = realm.createObject(Notificacion::class.java, ApplicationBase.idNextNoti.incrementAndGet())
            notifi!!.titulo = data.getValue("titulo")
            notifi.mensaje = data.getValue("mensaje")
            notifi.title = data.getValue("title")
            notifi.message = data.getValue("message")
            notifi.fechaNotif = getCurrentDate("yyyy-MM-dd HH:MM:SS")
            if (data.containsKey("imagen1")) {
                val im = realm.createObject(ImgNotis::class.java)
                im.url = data.getValue("imagen1")
                notifi.images.add(im)
            }
            if (data.containsKey("imagen2")) {
                val im = realm.createObject(ImgNotis::class.java)
                im.url = data.getValue("imagen2")
                notifi.images.add(im)
            }
            if (data.containsKey("imagen3")) {
                val im = realm.createObject(ImgNotis::class.java)
                im.url = data.getValue("imagen3")
                notifi.images.add(im)
            }
            if (data.containsKey("imagen4")) {
                val im = realm.createObject(ImgNotis::class.java)
                im.url = data.getValue("imagen4")
                notifi.images.add(im)
            }
        } else {
            println("MyFirebaseMessagingService.saveAndShowNotification --> la notificacion ya existe, solo mandarla")
        }
        realm.commitTransaction()
        val titulo = if(config.language == "ES") data.getValue("titulo") else data.getValue("title")
        val mensaje = if(config.language == "ES") data.getValue("mensaje") else data.getValue("message")
        showNotifi(titulo, mensaje)
    }

    private fun showNotifi(titulo: String, mensaje: String) {

        val icon = BitmapFactory.decodeResource(this.resources, R.mipmap.icono_redondo)

        val notificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        val NOTIFICATION_CHANNEL_ID = "my_channel_id_01"
        val chanelName: CharSequence = "defaults"
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val mChannel = NotificationChannel(NOTIFICATION_CHANNEL_ID, chanelName, NotificationManager.IMPORTANCE_HIGH)
            notificationManager.createNotificationChannel(mChannel)
        } else {
            TODO("VERSION.SDK_INT < O")
        }
        val intent = Intent(this, Principal::class.java)
        intent.putExtra(Principal.INTENT_PRINCIPAL, OnFragmentListener.FRAGMENT_NOTIFICACIONES)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP and Intent.FLAG_ACTIVITY_NEW_TASK and Intent.FLAG_ACTIVITY_CLEAR_TASK)
        val pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_ONE_SHOT)
        val defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)

        val notificationBuilder = NotificationCompat.Builder(this, NOTIFICATION_CHANNEL_ID)
                .setSmallIcon(R.mipmap.icn_notificaciones_blanco)
                .setLargeIcon(icon)
                .setVibrate(longArrayOf(0, 500, 500, 500))
                .setContentTitle(titulo)
                .setContentText(mensaje)
                .setStyle(BigTextStyle().bigText(mensaje))
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setCategory(CATEGORY_PROMO)
                .setVisibility(VISIBILITY_PUBLIC)
                .setPriority(PRIORITY_MAX)
                .setContentIntent(pendingIntent)


        notificationManager.notify(1217, notificationBuilder.build())
    }

}
package oomovil.com.appifix.models

import io.realm.RealmObject
import io.realm.annotations.PrimaryKey

/**
 * Created by Alan on 27/10/2017.
 */
open class Tarjeta : RealmObject(){
    @PrimaryKey
    var id: Int = 0
    var cardOwnerName: String? = null
    var cardNumber: String? = null
    var cardName: String? = null
    var expiryDate: String? = null
    var cardCvv: String? = null
    var idLoc: String? = null
    var isFavorite : Boolean = false
}
package oomovil.com.appifix.introduccion

import android.content.Context
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.view.ViewPager
import android.util.DisplayMetrics
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import kotlinx.android.synthetic.main.activity_introduccion.*
import oomovil.com.appifix.R
import oomovil.com.appifix.general.AppConfig
import oomovil.com.appifix.general.MyContextWrapper
import oomovil.com.appifix.general.dpToPx
import oomovil.com.appifix.preferences.PreferencesActivity
import oomovil.com.appifix.registro.RegistroActivity
import oomovil.com.appifix.views.FragmentStatePagerAdapter

class Introduccion : AppCompatActivity(), View.OnClickListener {

    private val fragments: ArrayList<Fragment> = ArrayList()

    override fun attachBaseContext(newBase: Context) {
        super.attachBaseContext(MyContextWrapper.wrap(newBase, AppConfig(newBase).language))
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_introduccion)

        btn_saltar_intro.setOnClickListener(this)

        loadFragments()
        viewpagerIntro.adapter = FragmentStatePagerAdapter(fragments, supportFragmentManager)
        pagerIndicator.setViewPager(viewpagerIntro)
        viewpagerIntro.currentItem = 0
    }

    override fun onPostCreate(savedInstanceState: Bundle?) {
        super.onPostCreate(savedInstanceState)
        AppConfig(this).introShown = true
    }

    private fun loadFragments() {
        fragments.add(FragmentIntro.getInstance(FragmentIntro.FRAGMENT_FIRST))
        fragments.add(FragmentIntro.getInstance(FragmentIntro.FRAGMENT_SECOND))
        fragments.add(FragmentIntro.getInstance(FragmentIntro.FRAGMENT_THIRD))
    }

    override fun onClick(v: View?) {
        if (v!!.id == R.id.btn_saltar_intro) {
            val intent = Intent(this, RegistroActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
            startActivity(intent)
            this@Introduccion.finish()
        }
    }
}

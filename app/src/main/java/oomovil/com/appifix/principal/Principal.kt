package oomovil.com.appifix.principal

import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Color
import android.graphics.Typeface
import android.net.Uri
import android.os.Build
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.support.v4.app.Fragment
import android.support.v4.widget.DrawerLayout
import android.support.v7.app.ActionBar
import android.support.v7.app.AlertDialog
import android.support.v7.widget.LinearLayoutManager
import android.transition.TransitionInflater
import android.view.Gravity
import android.view.MenuItem
import android.view.View
import android.view.animation.AnimationUtils
import android.widget.Toast
import com.google.firebase.iid.FirebaseInstanceId
import com.squareup.picasso.Picasso
import io.realm.Realm
import io.realm.RealmChangeListener
import kotlinx.android.synthetic.main.activity_principal.*

import oomovil.com.appifix.R
import oomovil.com.appifix.firebase.TokenFireBase
import oomovil.com.appifix.general.*
import oomovil.com.appifix.models.Carrito
import oomovil.com.appifix.models.UserModel
import oomovil.com.appifix.oomoviews.OToast
import oomovil.com.appifix.registro.RegistroActivity
import oomovil.com.appifix.views.AdapterMenu

class Principal : AppCompatActivity(), OnFragmentListener, View.OnClickListener, FragmentPerfil.UserChangeListener, FragmentCarrito.OnArticlesChange {

    private var isSearching = false
    private var count = 0
    private var lastFragment = -1

    interface onRemoveLast {
        fun onRemoveLast()
        fun onRemoveAll()
    }

    companion object {
        // este bundle contendrá si los fragments ya han sido creados, guardando su ID
        val bundle = Bundle()
        val INTENT_PRINCIPAL = "intetnt_principal"
        var inSubs = false
        var inProducts = false
        var delegateRemove: onRemoveLast? = null
        var banderaCar = false
    }

    override fun attachBaseContext(newBase: Context) {
        super.attachBaseContext(MyContextWrapper.wrap(newBase, AppConfig(newBase).language))
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_principal)

        setToolbar()
        supportDrawerContent()
        configureListOfSearch()

        btn_search.setOnClickListener(this)
        btn_close.setOnClickListener(this)
        btn_carrito.setOnClickListener(this)

        supportFragmentManager.addOnBackStackChangedListener {
            validateFragmentToHeader()
        }

        count = searchProductsCount()

        val viewToShow = intent.getIntExtra(INTENT_PRINCIPAL, OnFragmentListener.FRAGMENT_CATEGORIA)
        changeView(viewToShow, "-1")

    }

    private fun configureListOfSearch() {
        recyclerSearch.layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        ProductSearchManager(this, campoBusqueda, recyclerSearch, this)
    }

    override fun onStart() {
        super.onStart()
        KeyboardManager.init(this)



        FirebaseInstanceId.getInstance().instanceId.addOnSuccessListener {
            val token = it.token

            try {
                TokenFireBase().sendToken(token, this)
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
        //val token = FirebaseInstanceId.getInstance().token

    }

    override fun onResume() {
        super.onResume()
        onChangeDetected()
    }

    override fun onChangeDetected() {
        val newCount = searchProductsCount()
        if (newCount > 0) {
            countProds.text = "$newCount"
            countProds.visibility = View.VISIBLE
        } else {
            countProds.visibility = View.GONE
        }
        count = newCount
    }

    private fun searchProductsCount(): Int {
        val carrito = Realm.getDefaultInstance().where(Carrito::class.java).findFirst()
        if (carrito != null) {
            return carrito.productos.size
        }
        return 0
    }

    override fun onPostCreate(savedInstanceState: Bundle?) {
        super.onPostCreate(savedInstanceState)
        showDataUser()
    }

    // se manda llamar cada vez que el usuario hace un cambio en su perfil
    override fun userChange() {
        println("Principal.userChange --> cambios detectados")
        runOnUiThread {
            showDataUser()
        }
    }

    private fun showDataUser() {
        val user = Realm.getDefaultInstance().where(UserModel::class.java).findFirst()
        if (user != null) {
            txt_name_user.text = user.name
            txt_email_user.text = user.email
            if (user.photo != null && user.photo!!.isNotEmpty()) {
                Picasso.get().load(user.photo).into(fotoUser)
            } else {
                fotoUser.setImageResource(R.mipmap.img_perfil_default)
                fotoUser.borderWidth = 0
            }
        }
    }

    private fun setToolbar() {
        setSupportActionBar(toolbar)
        val ab: ActionBar? = supportActionBar
        if (ab != null) {
            ab.setHomeAsUpIndicator(R.mipmap.icn_menu)
            ab.setDisplayHomeAsUpEnabled(true)
            toolbar.setNavigationOnClickListener { drawer_layout.openDrawer(Gravity.START) }
        }
        grafico_perfil.setColorFilter(Color.parseColor("#d9ffffff"))
    }

    private fun supportDrawerContent() {

        recyclerMenu.layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        recyclerMenu.adapter = AdapterMenu(object : AdapterMenu.OnMenuClick {
            override fun selectedMenuItem(im: Int) {
                when (im) {
                    OnFragmentListener.FRAGMENT_CATEGORIA -> {
                        changeView(OnFragmentListener.FRAGMENT_CATEGORIA, "-1")
                    }
                    OnFragmentListener.FRAGMENT_PERFIL -> {
                        changeView(OnFragmentListener.FRAGMENT_PERFIL, "-1")
                    }
                    OnFragmentListener.FRAGMENT_TARJETAS -> {
                        changeView(OnFragmentListener.FRAGMENT_TARJETAS, "-1")
                    }
                    OnFragmentListener.FRAGMENT_UBICACIONES -> {
                        changeView(OnFragmentListener.FRAGMENT_UBICACIONES, "-1")
                    }
                    OnFragmentListener.FRAGMENT_HISTORIAL -> {
                        changeView(OnFragmentListener.FRAGMENT_HISTORIAL, "-1")
                    }
                    OnFragmentListener.FRAGMENT_SEGUIMIENTO -> {
                        changeView(OnFragmentListener.FRAGMENT_SEGUIMIENTO, "-1")
                    }
                    OnFragmentListener.FRAGMENT_NOTIFICACIONES -> {
                        changeView(OnFragmentListener.FRAGMENT_NOTIFICACIONES, "-1")
                    }
                    0 -> {
                        logout()
                    }
                }
            }

        })

        btn_facebook.setOnClickListener(this)
        btn_twitter.setOnClickListener(this)
        btn_instagram.setOnClickListener(this)

    }

    override fun onClick(v: View?) = when (v!!.id) {
        R.id.btn_search -> {
            isSearching = true
            listaBusqueda.visibility = View.VISIBLE
            revalViewSearch(containerSearch) // hace visible el layout para buscar
            drawer_layout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED, Gravity.START)
        }
        R.id.btn_close -> {
            unlockedDrawer()
        }
        R.id.btn_carrito -> {
            if (lastFragment != OnFragmentListener.FRAGMENT_CARRITO) {
                changeView(OnFragmentListener.FRAGMENT_CARRITO, "-1")
            } else {

            }
        }
        R.id.btn_facebook -> {
            goToUrl("https://www.facebook.com/ocamaster/")
        }
        R.id.btn_twitter -> {
            goToUrl("https://www.youtube.com/channel/UCQkdcvWoQO3rCAHLo8eyEoQ/videos?disable_polymer=1")
        }
        R.id.btn_instagram -> {
            sendEmail()
        }
        else -> {
            // do nothing
        }
    }

    private fun goToUrl(url: String) {
        val uriUrl = Uri.parse(url)
        val launchBrowser = Intent(Intent.ACTION_VIEW, uriUrl)
        launchBrowser.addCategory(Intent.CATEGORY_BROWSABLE)
        startActivity(launchBrowser)
    }

    private fun logout() {
        drawer_layout.closeDrawer(Gravity.START)
        val builder = AlertDialog.Builder(this)
        builder.setIcon(R.mipmap.alerta_pop)
                .setTitle(getString(R.string.cerrar_sesion_btn))
                .setCancelable(true)
                .setPositiveButton(getString(R.string.nav_logout).toUpperCase()) { dialog, _ ->
                    dialog.dismiss()
                    AppConfig(this@Principal).getPreferences().edit().clear().apply()
                    val realm = Realm.getDefaultInstance()
                    realm.executeTransactionAsync({ realmBg -> realmBg.deleteAll() }, { pasarinicio() }, { pasarinicio() })
                }
                .setNegativeButton(getString(R.string.cancelar)) { dialog, _ -> dialog.dismiss() }
        val dialog = builder.create()
        dialog.show()
    }

    private fun pasarinicio() {
        val intent = Intent(this, RegistroActivity::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
        startActivity(intent)
        this.finish()
    }

    /*private fun changeLanguage(lang: String) {
        drawer_layout.closeDrawer(Gravity.START)
        val langUser = AppConfig(this).language
        if (lang != langUser) {
            AppConfig(this).language = lang
            val intent = Intent()
            intent.setClass(this, this.javaClass)
            startActivity(intent)
            this.finish()
        }
    }*/

    override fun changeViewTransition(fragment: Int, id: String, viewList: List<Pair<View, String>>, data: List<Pair<String, String?>>) {
        // si está buscando, no se permiten los click en otras secciones
        if (isSearching)
            return
        if (lastFragment == fragment) {
            return
        }
        // porque s epuede acceder dos o más veces a esta pantalla por la búsqueda
        if (fragment != OnFragmentListener.FRAGMENT_PRODUCTO_DETALLE)
            lastFragment = fragment

        val f = createFragment(fragment)
        setupFragmentAnimation(f)
        val transaction = supportFragmentManager.beginTransaction()
        viewList.forEach {
            println("Principal.changeViewTransition --> ${it.second}")
            transaction.addSharedElement(it.first, it.second)
        }
        val bundle = Bundle()
        data.forEach {
            bundle.putString(it.first, it.second)
            println("Principal.changeViewTransition --> ${it.first} --> ${it.second}")
        }
        bundle.putString("id", id)
        f.arguments = bundle
        transaction.replace(R.id.container, f).addToBackStack(null).commitAllowingStateLoss()
    }

    override fun changeView(fragment: Int, id: String) {
        // si está buscando, no se permiten los click en otras secciones
        if (isSearching)
            return
        if (drawer_layout.isDrawerOpen(Gravity.START)) {
            drawer_layout.addDrawerListener(object : DrawerLayout.DrawerListener {
                override fun onDrawerStateChanged(newState: Int) {

                }

                override fun onDrawerSlide(p0: View, slideOffset: Float) {

                }

                override fun onDrawerClosed(p0: View) {
                    drawer_layout.removeDrawerListener(this)
                    showView(fragment, id)
                }

                override fun onDrawerOpened(p0: View) {

                }
            })
            drawer_layout.closeDrawer(Gravity.START)
        } else
            showView(fragment, id)
    }

    private fun showView(fragment: Int, id: String) {
        if (lastFragment == fragment) {
            if (Principal.inSubs) {
                Principal.delegateRemove!!.onRemoveAll()
                btn_search.visibility = View.VISIBLE
                txt_title.text = getString(R.string.nav_home)
            }
            return
        }

        // porque s epuede acceder dos o más veces a esta pantalla por la búsqueda
        if (fragment != OnFragmentListener.FRAGMENT_PRODUCTO_DETALLE)
            lastFragment = fragment

        val f = createFragment(fragment)
        val b = Bundle()
        b.putString("id", id)
        if (Principal.inSubs && fragment == OnFragmentListener.FRAGMENT_CATEGORIA) {
            Principal.bundle.remove("FragmentCategorias")
            b.putBoolean("clean", true)
            Principal.inSubs = false
        }
        f.arguments = b
        setupFragmentAnimation(f)
        supportFragmentManager.beginTransaction().replace(R.id.container, f).addToBackStack(null).commitAllowingStateLoss()
    }

    private fun validateFragmentToHeader() {
        if (supportFragmentManager.fragments == null || supportFragmentManager.fragments.size == 0) {
            return
        }
        val lastFragment = supportFragmentManager.fragments.last()
        when (lastFragment) {
            is FragmentCategorias -> {
                btn_search.visibility = View.VISIBLE
                if (FragmentCategorias.listSubcat.size <= 0) {
                    txt_title.text = getString(R.string.nav_home)
                }
            }
            is FragmentProductos -> {
                btn_search.visibility = View.VISIBLE
                txt_title.text = getString(R.string.productos)
            }
            is FragmentProductosDetalle -> {
                btn_search.visibility = View.VISIBLE
                txt_title.text = getString(R.string.detalle)
            }
            is FragmentPerfil -> {
                btn_search.visibility = View.INVISIBLE
                txt_title.text = getString(R.string.nav_perfil)
            }
            is FragmentMisTarjetas -> {
                btn_search.visibility = View.INVISIBLE
                txt_title.text = getString(R.string.nav_tarjetas)
            }
            is FragmentMisUbicaciones -> {
                btn_search.visibility = View.INVISIBLE
                txt_title.text = getString(R.string.nav_ubicaciones)
            }
            is FragmentHistorial -> {
                btn_search.visibility = View.INVISIBLE
                txt_title.text = getString(R.string.nav_historial)
            }
            is FragmentSeguimiento -> {
                btn_search.visibility = View.INVISIBLE
                txt_title.text = getString(R.string.nav_seguimiento)
            }
            is FragmentNotificaciones -> {
                btn_search.visibility = View.INVISIBLE
                txt_title.text = getString(R.string.nav_notificaciones)
            }
            is FragmentNotificacionesDetalle -> {
                btn_search.visibility = View.INVISIBLE
                txt_title.text = getString(R.string.nav_notificaciones)
            }
            is FragmentCarrito -> {
                btn_search.visibility = View.INVISIBLE
                txt_title.text = getString(R.string.carrito)
            }
            else -> btn_search.visibility = View.INVISIBLE
        }
    }

    private fun createFragment(fragmentView: Int): Fragment = when (fragmentView) {
        OnFragmentListener.FRAGMENT_CATEGORIA -> {
            FragmentCategorias()
        }
        OnFragmentListener.FRAGMENT_PRODUCTOS -> {
            FragmentProductos()
        }
        OnFragmentListener.FRAGMENT_PRODUCTO_DETALLE -> {
            FragmentProductosDetalle()
        }
        OnFragmentListener.FRAGMENT_PERFIL -> {
            FragmentPerfil()
        }
        OnFragmentListener.FRAGMENT_TARJETAS -> {
            FragmentMisTarjetas()
        }
        OnFragmentListener.FRAGMENT_UBICACIONES -> {
            FragmentMisUbicaciones()
        }
        OnFragmentListener.FRAGMENT_HISTORIAL -> {
            FragmentHistorial()
        }
        OnFragmentListener.FRAGMENT_NOTIFICACIONES -> {
            FragmentNotificaciones()
        }
        OnFragmentListener.FRAGMENT_NOTIFICACIONES_DETAIL -> {
            FragmentNotificacionesDetalle()
        }
        OnFragmentListener.FRAGMENT_CARRITO -> {
            FragmentCarrito()
        }
        OnFragmentListener.FRAGMENT_HISTORIAL_DETAIL -> {
            FragmentPedidoDetalle()
        }
        OnFragmentListener.FRAGMENT_SEGUIMIENTO -> {
            FragmentSeguimiento()
        }
        else -> {
            FragmentCategorias()
        }
    }

    private fun setupFragmentAnimation(fragment: Fragment) {
        if (supportFragmentManager.fragments == null || supportFragmentManager.fragments.size == 0) {
            return
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            // Inflate transitions to apply
            val changeTransform = TransitionInflater.from(this).inflateTransition(R.transition.change_image_transform)
            val explodeTransform = TransitionInflater.from(this).inflateTransition(android.R.transition.move)

            val fragmentOne: Fragment = supportFragmentManager.fragments.last()

            // Setup exit transition on first fragment
            fragmentOne.sharedElementReturnTransition = changeTransform
            fragmentOne.exitTransition = explodeTransform

            // Setup enter transition on second fragment
            fragment.sharedElementEnterTransition = changeTransform
            fragment.enterTransition = explodeTransform
        }
    }

    override fun onBackPressed() {
        if (Principal.inSubs && !inProducts) {
            Principal.delegateRemove!!.onRemoveLast()
        } else {
            if (!drawer_layout.isDrawerOpen(Gravity.START) && !isSearching) {
                if ((supportFragmentManager.fragments == null || supportFragmentManager.fragments.last() is FragmentCategorias))
                    this.finish()
                else {
                    super.onBackPressed()
                    if (supportFragmentManager.fragments == null || supportFragmentManager.fragments.isEmpty())
                        this.finish()
                    else
                        validatelastFragment(supportFragmentManager.fragments.last())
                }
            } else if (drawer_layout.isDrawerOpen(Gravity.START)) {
                drawer_layout.closeDrawer(Gravity.START)
            } else if (isSearching) {
                unlockedDrawer()
            }
        }
    }

    private fun validatelastFragment(last: Fragment?) {
        if (last != null) {
            println("Principal.validatelastFragment --> $last")
            when (last) {
                is FragmentCategorias -> {
                    lastFragment = OnFragmentListener.FRAGMENT_CATEGORIA
                }
                is FragmentProductos -> {
                    lastFragment = OnFragmentListener.FRAGMENT_PRODUCTOS
                }
                is FragmentProductosDetalle -> {
                    // en este punto no se hace nada, porque se puede regresar a la misma pantalla por la búsqueda
                    //lastFragment = OnFragmentListener.FRAGMENT_PRODUCTO_DETALLE
                }
                is FragmentPerfil -> {
                    lastFragment = OnFragmentListener.FRAGMENT_PERFIL
                }
                is FragmentMisTarjetas -> {
                    lastFragment = OnFragmentListener.FRAGMENT_TARJETAS
                }
                is FragmentMisUbicaciones -> {
                    lastFragment = OnFragmentListener.FRAGMENT_UBICACIONES
                }
                is FragmentHistorial -> {
                    lastFragment = OnFragmentListener.FRAGMENT_HISTORIAL
                }
                is FragmentNotificaciones -> {
                    lastFragment = OnFragmentListener.FRAGMENT_NOTIFICACIONES
                }
                is FragmentNotificacionesDetalle -> {
                    lastFragment = OnFragmentListener.FRAGMENT_NOTIFICACIONES_DETAIL
                }
                is FragmentCarrito -> {
                    lastFragment = OnFragmentListener.FRAGMENT_CARRITO
                }
                is FragmentSeguimiento -> {
                    lastFragment = OnFragmentListener.FRAGMENT_SEGUIMIENTO
                }
            }
        }
    }

    private fun unlockedDrawer() {
        isSearching = false
        listaBusqueda.visibility = View.GONE
        revalViewSearch(containerSearch) // hace invisible el layout para buscar
        drawer_layout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED, Gravity.START)
    }

    private fun sendEmail() {
        val tO = arrayOf("contact@appifix.com") //aquí pon tu correo
        val cC = arrayOf("")
        val emailIntent = Intent(Intent.ACTION_SEND)
        emailIntent.data = Uri.parse("mailto:")
        emailIntent.type = "text/plain"
        emailIntent.putExtra(Intent.EXTRA_EMAIL, tO)
        emailIntent.putExtra(Intent.EXTRA_CC, cC)
        // Esto podrás modificarlo si quieres, el asunto y el cuerpo del mensaje
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Asunto")
        emailIntent.putExtra(Intent.EXTRA_TEXT, "Escribe aquí tu mensaje")

        try {
            startActivity(Intent.createChooser(emailIntent, "Enviar email..."))
            finish()
        } catch (ex: android.content.ActivityNotFoundException) {
            Toast.makeText(this,
                    "No tienes clientes de email instalados.", Toast.LENGTH_SHORT).show()
        }

    }

}

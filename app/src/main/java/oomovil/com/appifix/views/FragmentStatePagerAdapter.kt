package oomovil.com.appifix.views

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter

/**
 * Created by Alan on 16/10/2017.
 */
class FragmentStatePagerAdapter(val fragments : ArrayList<Fragment>, fm : FragmentManager) : FragmentPagerAdapter(fm) {

    override fun getItem(position: Int): Fragment = fragments.get(position)

    override fun getCount(): Int = fragments.size

    //override fun getPageWidth(position: Int): Float = 0.93f
}
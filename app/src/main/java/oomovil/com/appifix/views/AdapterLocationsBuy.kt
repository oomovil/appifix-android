package oomovil.com.appifix.views

import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.graphics.PorterDuff
import android.os.Handler
import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import com.chauthai.swipereveallayout.ViewBinderHelper
import kotlinx.android.synthetic.main.item_button_add_buy.view.*
import kotlinx.android.synthetic.main.item_location_buy.view.*
import kotlinx.android.synthetic.main.item_recoger_local.view.*
import kotlinx.android.synthetic.main.item_text_header.view.*
import oomovil.com.appifix.R
import oomovil.com.appifix.general.AppConfig
import oomovil.com.appifix.general.inflate
import oomovil.com.appifix.models.MakePayment
import oomovil.com.appifix.principal.ActivityAddLocation
import oomovil.com.appifix.principal.ActivityRealizarCompra

/**
 * Created by Alan on 08/12/2017.
 */
class AdapterLocationsBuy(private val items: List<MakePayment.Address>, val context: Context, private val click: ActivityRealizarCompra.OnClickSelected) : RecyclerView.Adapter<AdapterLocationsBuy.ViewHolder>() {

    override fun onBindViewHolder(p0: ViewHolder, p1: Int) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    //private val config = AppConfig(context)
    private val titleItem = 0
    private val normalItem = 1
    private val recogerItem = 2
    private val newItem = 3

    private var viewBinderHelper = ViewBinderHelper()

    init {
        viewBinderHelper.setOpenOnlyOne(true)
    }

    override fun getItemViewType(position: Int): Int {
        val i = items[position]
        return when (i.type) {
            MakePayment.AddressType.TITLE -> titleItem
            MakePayment.AddressType.NEW -> newItem
            MakePayment.AddressType.LOCAL -> recogerItem
            else -> normalItem
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder = when (viewType) {
        titleItem ->
            ViewHolder(parent.inflate(R.layout.item_text_header, false))
        newItem ->
            ViewHolder(parent.inflate(R.layout.item_button_add_buy, false))
        recogerItem ->
            ViewHolder(parent.inflate(R.layout.item_recoger_local, false))
        else ->
            ViewHolder(parent.inflate(R.layout.item_location_buy, false))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int, payloads: MutableList<Any>) {
        when(holder.itemViewType) {
            titleItem -> {
                holder.itemView.txt_title_normal.visibility = View.VISIBLE
                holder.itemView.txt_title_shipping.visibility = View.GONE
                holder.itemView.txt_title_normal.text = context.getString(R.string.direccion_envio)
            }
            newItem -> {
                holder.itemView.btn_add.setOnClickListener {
                    val intent = Intent(context, ActivityAddLocation::class.java)
                    intent.putExtra("isNewLocation", true)
                    context.startActivity(intent)
                }
            }
            recogerItem -> {
                val loca = items[position]
                if (loca.isSelected) {
                    holder.itemView.btn_check_local.setColorFilter(Color.parseColor("#3e4e94"))
                } else {
                    holder.itemView.btn_check_local.colorFilter = null
                }
                holder.itemView.layLocal.setOnClickListener {
                    val select = !loca.isSelected
                    items.forEach { l ->
                        l.isSelected = false
                    }
                    loca.isSelected = select
                    notifyDataSetChanged()
                    click.onCLick("0", select)
                }
            }
             else -> {
                val loca = items[position]
                 viewBinderHelper.bind(holder.itemView.swipeRevealLayoutBuy, items[position].locations!!.id)

                 if (position == 1) {
                     Handler().postDelayed({
                         viewBinderHelper.openLayout(loca.locations!!.id)
                         Handler().postDelayed({
                             viewBinderHelper.closeLayout(loca.locations!!.id)
                         }, 500)
                     }, 250)
                 }

                 holder.itemView.txt_alias.text = loca.locations!!.alias
                 holder.itemView.txt_calle.text = loca.locations!!.direccion
                 holder.itemView.txt_colonia.text = loca.locations!!.colonia
                 val ciudad = "${loca.locations!!.ciudad}, ${loca.locations!!.estado}. C.P. ${loca.locations!!.cp}"
                 holder.itemView.txt_ciudad.text = ciudad

                 holder.itemView.btn_editar.setColorFilter(Color.WHITE)
                 if (loca.locations!!.principal == "SI") {
                     holder.itemView.btnFav.setColorFilter(Color.YELLOW, PorterDuff.Mode.MULTIPLY)
                 } else {
                     holder.itemView.btnFav.visibility = View.GONE
                 }

                 if (loca.isSelected) {
                     holder.itemView.btn_check.setColorFilter(Color.parseColor("#3e4e94"))
                 } else {
                     holder.itemView.btn_check.colorFilter = null
                 }

                 holder.itemView.layLoca.setOnClickListener {
                     val select = !loca.isSelected
                     items.forEach { l ->
                         l.isSelected = false
                     }
                     loca.isSelected = select
                     notifyDataSetChanged()
                     click.onCLick(loca.locations!!.id.toString(), select)
                 }

                 holder.itemView.btn_editar.setOnClickListener {
                     val intent = Intent(context, ActivityAddLocation::class.java)
                     intent.putExtra("isNewLocation", false)
                     intent.putExtra("idLocation", items[holder.adapterPosition].locations!!.id)
                     context.startActivity(intent)
                 }
             }
        }
    }

    override fun getItemCount(): Int = items.size

    class ViewHolder(val view: View) : RecyclerView.ViewHolder(view)
}
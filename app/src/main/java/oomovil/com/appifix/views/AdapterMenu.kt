package oomovil.com.appifix.views

import android.graphics.Color
import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.item_menu.view.*
import oomovil.com.appifix.R
import oomovil.com.appifix.general.inflate
import oomovil.com.appifix.models.Menu
import oomovil.com.appifix.principal.OnFragmentListener

class AdapterMenu(private var listener : OnMenuClick) : RecyclerView.Adapter<AdapterMenu.ViewHolder>() {

    interface OnMenuClick {
        fun selectedMenuItem(im : Int)
    }

    private val menu : ArrayList<Menu>  by lazy {
        arrayListOf(
                Menu(OnFragmentListener.FRAGMENT_CATEGORIA, R.string.nav_home, R.mipmap.logo_menu),
                Menu(OnFragmentListener.FRAGMENT_PERFIL, R.string.nav_perfil, R.mipmap.icn_perfil_blanco),
                Menu(OnFragmentListener.FRAGMENT_TARJETAS, R.string.nav_tarjetas, R.mipmap.icn_tarjetas_blanco),
                Menu(OnFragmentListener.FRAGMENT_UBICACIONES, R.string.nav_ubicaciones, R.mipmap.icn_ubicaciones_blanco),
                Menu(OnFragmentListener.FRAGMENT_HISTORIAL, R.string.nav_historial, R.mipmap.icn_historial_blanco),
                Menu(OnFragmentListener.FRAGMENT_SEGUIMIENTO, R.string.nav_seguimiento, R.mipmap.ic_technics),
                Menu(OnFragmentListener.FRAGMENT_NOTIFICACIONES, R.string.nav_notificaciones, R.mipmap.icn_notificaciones_blanco),

                Menu(0, R.string.nav_logout, R.mipmap.icn_logout_blanco)
        )
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder = ViewHolder(parent.inflate(R.layout.item_menu, false))

    override fun getItemCount(): Int = menu.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = menu[position]
        holder.itemView.img_menu.setImageResource(item.image)
        holder.itemView.txt_menu.setText(item.name)
        holder.itemView.img_menu.setColorFilter(Color.WHITE)
        holder.itemView.layMenu.setOnClickListener {
            listener.selectedMenuItem(menu[holder.adapterPosition].id)
        }
    }

    inner class ViewHolder(val view: View) : RecyclerView.ViewHolder(view)
}
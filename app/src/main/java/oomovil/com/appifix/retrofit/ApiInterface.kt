package oomovil.com.appifix.retrofit

import android.content.Context
import okhttp3.MultipartBody
import okhttp3.RequestBody
import oomovil.com.appifix.models.Model
import retrofit2.Call
import retrofit2.http.*

/**
 * Created by Alan on 16/10/2017.
 */
interface ApiInterface {

    companion object Factory {
        fun create(ctx: Context): ApiInterface {
            val retrofit = ApiClient.getClient(ctx)
            return retrofit.create(ApiInterface::class.java)
        }
    }

    @POST("regUserNew")
    fun regUserNew(@Body map: HashMap<String, String>): Call<Model.ResponseRegistro>

    @Multipart
    @POST("regUserNew")
    fun regUser(@Part("nombre") name: RequestBody?,
                @Part("email") email: RequestBody?,
                @Part("pass") pass: RequestBody?,
                @Part("fb_token") fb_token: RequestBody?,
                @Part("gp_token") gp_token: RequestBody?,
                @Part("lang") lang: RequestBody?,
                @Part file: MultipartBody.Part?): Call<Model.ResponseRegistro>

    @Multipart
    @POST("updateFotoPerfil")
    fun uploadNewPicture(@Part("email") email: RequestBody,
                         @Part("lang") lang: RequestBody?,
                         @Part file: MultipartBody.Part?): Call<Model.ResponseUploadPicture>

    @Headers("Content-Type: application/json; charset=utf-8")
    @POST("loginUserNew")
    fun login(@Body map: HashMap<String, String>): Call<Model.ResponseLogin>

    @Headers("Content-Type: application/json; charset=utf-8")
    @POST("savePreferencias")
    fun saveUserPreferences(@Body map: HashMap<String, String>): Call<Model.Response>

    @Headers("Content-Type: application/json; charset=utf-8")
    @POST("getCategorias")
    fun getPreferences(@Body map: HashMap<String, String>): Call<Model.ResponseCategorias>

    @Headers("Content-Type: application/json; charset=utf-8")
    @GET("getAllPaises")
    fun getCountries(): Call<Model.ResponsePaises>

    @Headers("Content-Type: application/json; charset=utf-8")
    @POST("recoverPassword")
    fun recoverPassword(@Body map: HashMap<String, String>): Call<Model.Response>

    @Headers("Content-Type: application/json; charset=utf-8")
    @POST("regToken")
    fun regToken(@Body map: HashMap<String, String>): Call<Model.Response>

    @Headers("Content-Type: application/json; charset=utf-8")
    @POST("getUserPreferences")
    fun getUserPreferences(@Body map: HashMap<String, String>): Call<Model.ResponseCategorias>

    @Headers("Content-Type: application/json; charset=utf-8")
    @POST("getCategorias")
    fun getCategorias(@Body map: HashMap<String, String>): Call<Model.ResponseCategorias>

    @Headers("Content-Type: application/json; charset=utf-8")
    @POST("saveUbicacion")
    fun addLocation(@Body map: HashMap<String, String>): Call<Model.ResponseAddLocation>

    @Headers("Content-Type: application/json; charset=utf-8")
    @POST("updtUbicacion")
    fun updateLocation(@Body map: HashMap<String, String>): Call<Model.Response>

    @Headers("Content-Type: application/json; charset=utf-8")
    @POST("delUbicacion")
    fun deleteLocation(@Body map: HashMap<String, String>): Call<Model.Response>

    @Headers("Content-Type: application/json; charset=utf-8")
    @POST("updatePerfil")
    fun updateProfile(@Body map: HashMap<String, String>): Call<Model.Response>

    @Headers("Content-Type: application/json; charset=utf-8")
    @POST("getProdsFromCat")
    fun getProdsFromCat(@Body map: HashMap<String, String>): Call<Model.ResponseProductFromCategory>

    @Headers("Content-Type: application/json; charset=utf-8")
    @POST("saveCarrito")
    fun saveNewCarrito(@Body map: HashMap<String, String>): Call<Model.ResponseSaveCarrito>

    @Headers("Content-Type: application/json; charset=utf-8")
    @POST("addToCart")
    fun addToCarrito(@Body map: HashMap<String, String>): Call<Model.ResponseSaveCarrito>

    @Headers("Content-Type: application/json; charset=utf-8")
    @POST("updToCart")
    fun updateToCarrito(@Body map: HashMap<String, String>): Call<Model.ResponseSaveCarrito>

    @Headers("Content-Type: application/json; charset=utf-8")
    @POST("delToCart")
    fun deleteProdFromCar(@Body map: HashMap<String, String>): Call<Model.ResponseSaveCarrito>

    @Headers("Content-Type: application/json; charset=utf-8")
    @POST("regCupon")
    fun regCupon(@Body map: HashMap<String, String>): Call<Model.ResponseCupon>

    @Headers("Content-Type: application/json; charset=utf-8")
    @POST("getNotifications")
    fun getNotifications(@Body map: HashMap<String, String>): Call<Model.ResponseNotis>

    @Headers("Content-Type: application/json; charset=utf-8")
    @POST("searchProducts")
    fun searchProduct(@Body map: HashMap<String, String>): Call<Model.ResponseSearch>

    @Headers("Content-Type: application/json; charset=utf-8")
    @POST("addOrden")
    fun addOrden(@Body map: HashMap<String, String>): Call<Model.ResponseOrden>

    @Headers("Content-Type: application/json; charset=utf-8")
    @POST("getOrders")
    fun getOrders(@Body map: HashMap<String, String>): Call<Model.ResponseHistorial>

    @Headers("Content-Type: application/json; charset=utf-8")
    @POST("getOrderDet")
    fun getOrderDet(@Body map: HashMap<String, String>): Call<Model.ResponseDetalleHistorial>

    @Headers("Content-Type: application/json; charset=utf-8")
    @POST("getCuotaDHL")
    fun getCuotaDHL(@Body map: HashMap<String, String>): Call<Model.ResponseCuotasPaqueterias>

    @Headers("Content-Type: application/json; charset=utf-8")
    @POST("getStates")
    fun getStates(@Body map: HashMap<String, String>): Call<Model.ResponseStates>

    //@Headers("Content-Type: application/json; charset=utf-8")
    @POST("getOrderTotal")
    fun getOrderTotal(@Body map: HashMap<String, String?>): Call<Model.ResponseOrderTotal>

    @POST("getProdsById")
    fun getProdsById(@Body map: HashMap<String, String?>): Call<Model.ResponseProductsById>
}
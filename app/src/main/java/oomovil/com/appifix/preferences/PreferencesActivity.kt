package oomovil.com.appifix.preferences

import android.app.AlertDialog
import android.content.Intent
import android.graphics.Color
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.StaggeredGridLayoutManager
import android.view.View
import dmax.dialog.SpotsDialog
import io.realm.Realm
import kotlinx.android.synthetic.main.activity_preferences.*

import oomovil.com.appifix.R
import oomovil.com.appifix.general.AppConfig
import oomovil.com.appifix.general.DownloadPreferences
import oomovil.com.appifix.general.dialogView
import oomovil.com.appifix.models.Model
import oomovil.com.appifix.models.Preferences
import oomovil.com.appifix.models.UserModel
import oomovil.com.appifix.oomoviews.OToast
import oomovil.com.appifix.principal.Principal
import oomovil.com.appifix.retrofit.ApiInterface
import oomovil.com.appifix.views.AdapterPreferences
import org.json.JSONArray
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Response

class PreferencesActivity : AppCompatActivity(), OnPreferencesListener, View.OnClickListener {

    private var preferences: ArrayList<Preferences>? = null
    private var isRegistro = false
    private var validPreferences = false
    private val dialogLo: AlertDialog by lazy {
        this.dialogView()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_preferences)

        recyclerPreferencias.layoutManager = StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL)
        btn_select_all.setOnClickListener(this)
        btn_save.setOnClickListener(this)
        btn_reload.setOnClickListener(this)
        isRegistro = intent.getBooleanExtra("isRegistro", false)

        if (isRegistro) {
            loadPreferences()
        } else {
            showPreferences()
        }
    }

    private fun loadPreferences() {
        layButtons.visibility = View.GONE
        layError.visibility = View.GONE
        recyclerPreferencias.visibility = View.GONE
        progress.visibility = View.VISIBLE
        progress.playAnimation()
        DownloadPreferences(this, null, object : DownloadPreferences.OnPreferencesListener {
            override fun onSuccess() {
                println("PreferencesActivity.onSuccess --> descarga de preferencias chidas")
                showPreferences()
            }

            override fun onServerError() {
                println("PreferencesActivity.onServerError --> error al descargarlas del servidor")
                showPreferences()
            }

            override fun onInternalError() {
                println("PreferencesActivity.onInternalError --> error al guardar las preferencias")
                showPreferences()
            }
        })
    }

    private fun showPreferences() {
        progress.cancelAnimation()
        progress.visibility = View.GONE
        val prefes = Realm.getDefaultInstance().where(Preferences::class.java).findAll()
        if (prefes.size > 0) {
            preferences = ArrayList()

            layButtons.visibility = View.VISIBLE
            recyclerPreferencias.visibility = View.VISIBLE

            prefes.forEach {
                preferences!!.add(Preferences(it.id, it.name, it.nameEn, it.urlFoto, it.isSelected))
            }

            recyclerPreferencias.adapter = AdapterPreferences(preferences, this, this)
            validate()
        } else {
            layError.visibility = View.VISIBLE
            layButtons.visibility = View.GONE
        }
    }

    override fun onClick(position: Int) {
        validate()
    }

    private fun validate() {
        var countSelected = 0
        preferences!!.forEach { pre ->
            if (pre.isSelected)
                countSelected += 1
        }

        if (countSelected > 0) {
            validPreferences = true
            btn_save.setTextColor(Color.parseColor("#3e4e94"))
        } else {
            validPreferences = false
            btn_save.setTextColor(Color.parseColor("#8b8b8b"))
        }

        if (countSelected == preferences!!.size) {
            btn_select_all.text = getString(R.string.deseleccionar_todo)
        } else {
            btn_select_all.text = getString(R.string.seleccionar_todo)
        }
    }

    override fun onClick(p0: View?) {
        when (p0!!.id) {
            R.id.btn_select_all -> {
                if (btn_select_all.text == getString(R.string.seleccionar_todo)) selectAll(true) else selectAll(false)
            }
            R.id.btn_save -> {
                if (validPreferences) {
                    updatePreferences()
                } else
                    OToast.makeText(this, getString(R.string.seleccionar_al_menos_uno)).show()

            }
            R.id.btn_reload -> {
                loadPreferences()
            }
        }
    }

    private fun updatePreferences() {
        dialogLo.show()
        val map = HashMap<String, String>()
        map.put("lang", AppConfig(this).language)
        val array = JSONArray()
        preferences!!.forEach {
            if (it.isSelected) {
                val obj = JSONObject()
                obj.put("pref_id", it.id)
                array.put(obj)
            }
        }
        map.put("preferencias", array.toString())
        val user = Realm.getDefaultInstance().where(UserModel::class.java).findFirst()
        if (user != null) {
            map.put("email", user.email)
        }

        println("PreferencesActivity.updatePreferences --> $map")

        ApiInterface.create(this).saveUserPreferences(map).enqueue(object : retrofit2.Callback<Model.Response> {
            override fun onResponse(call: Call<Model.Response>?, response: Response<Model.Response>?) {
                try {
                    println("PreferencesActivity.onResponse --> ${response!!.body()}")
                    println("PreferencesActivity.onResponse --> ${response.message()}")
                } catch (e: Exception) {
                    e.printStackTrace()
                }
                if (response != null && response.isSuccessful && response.body() != null && response.body()!!.status == 200) {
                    saveUserPrefs()
                } else {
                    dialogLo.dismiss()
                    println("PreferencesActivity.onResponse --> error al guardar")
                    OToast.makeText(this@PreferencesActivity, response!!.body()!!.message).show()
                }
            }

            override fun onFailure(call: Call<Model.Response>?, t: Throwable?) {
                dialogLo.dismiss()
                t!!.printStackTrace()
                OToast.makeText(this@PreferencesActivity, R.string.error_conexion).show()
            }

        })
    }

    private fun saveUserPrefs() {
        Realm.getDefaultInstance().use { realm ->
            realm.executeTransactionAsync({ bgRealm ->

                //primero ponemos todas en FALSE, para validar las nuevas opciones
                val allPref = bgRealm.where(Preferences::class.java).findAll()
                if (allPref.isNotEmpty()) {
                    allPref.forEach {
                        it.isSelected = false
                        bgRealm.copyToRealmOrUpdate(it)
                    }
                }

                // ahora se ponen en TRUE las que el usuario seleccionó
                preferences!!.forEach {
                    val pre = bgRealm.where(Preferences::class.java).equalTo("id", it.id).findFirst()
                    if (pre != null) {
                        pre.isSelected = it.isSelected
                        bgRealm.copyToRealmOrUpdate(pre)
                    }
                }
            }, {
                dialogLo.dismiss()
                if (isRegistro)
                    startActivity(Intent(this@PreferencesActivity, Principal::class.java))
                this@PreferencesActivity.finish()
            }, {
                dialogLo.dismiss()
                OToast.makeText(this, getString(R.string.error_guardar_preferencias)).show()
                it.printStackTrace()
            })
        }
    }

    private fun selectAll(select: Boolean) {
        preferences!!.forEach { pre ->
            pre.isSelected = select
        }
        recyclerPreferencias.adapter?.notifyDataSetChanged()
        validate()
    }

    override fun onBackPressed() {
        if (!isRegistro) {
            super.onBackPressed()
        } else {
            if (!validPreferences)
                OToast.makeText(this, getString(R.string.seleccionar_al_menos_una)).show()
            else {
                updatePreferences()
            }
        }
    }

}

package oomovil.com.appifix.principal

import android.app.AlertDialog
import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import dmax.dialog.SpotsDialog
import io.realm.Realm
import io.realm.RealmResults
import kotlinx.android.synthetic.main.fragment_historial.*
import oomovil.com.appifix.R
import oomovil.com.appifix.general.dialogView
import oomovil.com.appifix.models.*
import oomovil.com.appifix.retrofit.ApiInterface
import oomovil.com.appifix.views.AdapterPedidos
import retrofit2.Call
import retrofit2.Response

/**
 * Created by Alan on 26/10/2017.
 */
class FragmentHistorial : Fragment() {

    private var listenerFragment: OnFragmentListener? = null
    private var listPedidos: RealmResults<Pedido>? = null
    private val FRAGMENT_STATE = "FragmentHistorial"
    private val realm = Realm.getDefaultInstance()
    private val dialogLo: AlertDialog by lazy {
        this.activity!!.dialogView()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?):
            View? = inflater!!.inflate(R.layout.fragment_historial, container, false)

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        try {
            listenerFragment = context as OnFragmentListener
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    private fun showListPedidos() {
        if (context != null) {
            listPedidos = Realm.getDefaultInstance().where(Pedido::class.java).findAll()
            if (listPedidos != null && listPedidos!!.isNotEmpty()) {
                recyclerPedidos.visibility = View.VISIBLE
                laySinPedidos.visibility = View.GONE
                refreshHis.visibility = View.VISIBLE
                recyclerPedidos.adapter = AdapterPedidos(listPedidos, context!!, object : AdapterPedidos.OnClick {
                    override fun onCLick(position: Int,id: String) {
                        if (listenerFragment != null)
                            listenerFragment!!.changeView(OnFragmentListener.FRAGMENT_HISTORIAL_DETAIL, id)
                    }
                })
            } else {
                recyclerPedidos.visibility = View.GONE
                laySinPedidos.visibility = View.VISIBLE
            }
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        loadPedidos()

        recyclerPedidos.layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
       // recyclerPedidos.itemAnimator = DefaultItemAnimator()

        refreshHis.setOnRefreshListener {
            loadPedidos()
        }

        if (!Principal.bundle.containsKey(FRAGMENT_STATE)) {
            println("FragmentCategorias.onViewCreated --> instance null")
            loadPedidos()
            Principal.bundle.putBoolean(FRAGMENT_STATE, true)
        } else {
            println("FragmentCategorias.onViewCreated --> instance not null")
            showListPedidos()
        }
    }

    private fun loadPedidos() {
        dialogLo.show()
        val map = HashMap<String, String>()
        map["idUsuario"] = Realm.getDefaultInstance().where(UserModel::class.java).findFirst()!!.idUser.toString()
        println("FragmentHistorial.loadPedidos --> $map")
        ApiInterface.create(context!!).getOrders(map).enqueue(object : retrofit2.Callback<Model.ResponseHistorial> {
            override fun onFailure(call: Call<Model.ResponseHistorial>?, t: Throwable?) {
                dialogLo.dismiss()
                refreshHis.isRefreshing = false
                println("FragmentCategorias.onFailure")
                t!!.printStackTrace()
                if (activity != null && !activity!!.isFinishing && context != null) {
                    showListPedidos()
                }
            }

            override fun onResponse(call: Call<Model.ResponseHistorial>?, response: Response<Model.ResponseHistorial>?) {
                try {
                    println("FragmentHistorial.onResponse --> ${response!!.body()}")
                } catch (e: Exception) {
                    e.printStackTrace()
                }
                refreshHis.isRefreshing = false
                if (activity != null && !activity!!.isFinishing && context != null) {
                    println("call = [$call], response = [$response]")
                    if (response != null && response.isSuccessful && response.body() != null && response.body()!!.historial.isNotEmpty()) {
                        Realm.getDefaultInstance().executeTransactionAsync({ realbg ->
                            realbg.where(Pedido::class.java).findAll().deleteAllFromRealm()
                            response.body()!!.historial.forEach {
                                println("FragmentHistorial.onResponse --> ${it.id} --> ${it.date}")
                                realbg.copyToRealmOrUpdate(it)
                            }
                        }, {
                            dialogLo.dismiss()
                            println("FragmentHistorial.onResponse --> historial guardado")
                            showListPedidos()
                        }, { t ->
                            dialogLo.dismiss()
                            println("FragmentHistorial.onResponse --> error al guardar el historial")
                            showListPedidos()
                        })
                        /*
                        Realm.getDefaultInstance().use { realm ->
                            realm.executeTransactionAsync({ realmBg ->
                                realmBg.where(Pedido::class.java).findAll().deleteAllFromRealm()
                                println("Historial --> ${response.body()!!.historial}")
                                response.body()!!.historial.forEach {
                                    println("FragmentHistorial.onResponse --> ${it.id} --> ${it.date}")
                                    realmBg.copyToRealmOrUpdate(it)
                                }
                            }, {
                                dialogLo.dismiss()
                                println("FragmentHistorial.onResponse --> historial guardado")
                                showListPedidos()
                            }, { t ->
                                dialogLo.dismiss()
                                println("FragmentHistorial.onResponse --> error al guardar el historial")
                                t.printStackTrace()
                                showListPedidos()
                            })

                        }
                        */
                    } else {
                        dialogLo.dismiss()
                        if (response!!.body()!!.historial.isEmpty()) {
                            realm.executeTransactionAsync({ realbg ->
                                realbg.where(Pedido::class.java).findAll().deleteAllFromRealm()
                            }, {
                                println("No hay lista de pedidos")
                                showListPedidos()
                            }, {
                                it.printStackTrace()
                            })
                        }
                        println("FragmentHistorial.onResponse --> no se descargo el historial")
                        showListPedidos()
                    }
                }
            }
        })
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        println("FragmentHistorial.onSaveInstanceState --> guardar el estado")
        outState.putString(FRAGMENT_STATE, "creado")
    }

    override fun onDestroyView() {
        println("FragmentHistorial.onDestroyView --> vista destruida")
        onSaveInstanceState(Bundle())
        super.onDestroyView()
    }
}
package oomovil.com.appifix

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.content.pm.PackageInfo
import android.content.pm.PackageManager
import android.graphics.BitmapFactory
import android.os.Build
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import com.facebook.appevents.internal.AppEventUtility.bytesToHex
import io.realm.Realm
import io.realm.RealmList
import oomovil.com.appifix.introduccion.Introduccion
import oomovil.com.appifix.principal.Principal
import java.security.MessageDigest
import java.security.NoSuchAlgorithmException
import net.iharder.Base64
import oomovil.com.appifix.general.AppConfig
import oomovil.com.appifix.general.DownloadPreferences
import oomovil.com.appifix.general.ImageUtils
import oomovil.com.appifix.general.MyContextWrapper
import oomovil.com.appifix.models.*
import oomovil.com.appifix.registro.RegistroActivity
import oomovil.com.appifix.retrofit.ApiInterface
import retrofit2.Call
import retrofit2.Response
import java.net.HttpURLConnection
import java.net.URL
import java.security.Signature
import java.util.*

class Splash : AppCompatActivity() {

    private val mHandler: Handler = Handler()
    private var call: Call<Model.ResponseLogin>? = null
    private val config: AppConfig by lazy {
        AppConfig(this)
    }
    private val ACTIVITY_INTRO = 1
    private val ACTIVITY_PRINCIPAL = 2
    private val ACTIVITY_REGISTRO = 3

    override fun attachBaseContext(newBase: Context) {
        super.attachBaseContext(MyContextWrapper.wrap(newBase, AppConfig(newBase).language))
    }

    @SuppressLint("PackageManagerGetSignatures")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)

        println("Splash.onCreate --> ${config.language}")
        getApplicationSignature()
    }

    override fun onPostCreate(savedInstanceState: Bundle?) {
        super.onPostCreate(savedInstanceState)
        mHandler.postDelayed({
            validateInfo()
        }, 1000)
    }

    override fun onDestroy() {
        mHandler.removeCallbacks(mHandler.looper.thread)
        if (call != null)
            call!!.cancel()
        super.onDestroy()
    }

    private fun validateInfo() {
        if (config.userSession) {
            loadUserData()
        } else if (!config.introShown) {
            loadActivity(ACTIVITY_INTRO)
        } else {
            loadActivity(ACTIVITY_REGISTRO)
        }
    }

    private fun loadUserData() {
        val userP: UserModel? = Realm.getDefaultInstance().where(UserModel::class.java).findFirst()
        if (userP != null) {
            println("Splash.loadUserData --> ${userP.email}")
            println("Splash.loadUserData --> ${userP.password}")
            println("Splash.loadUserData --> ${userP.token}")
            val map = HashMap<String, String>()
            map.put("uname", userP.email)
            map.put("lang", AppConfig(this).language)
            val pass = userP.password
            val token = userP.token
            when (userP.getUserReg()) {
                UserReg.NORMAL -> {
                    map.put("upass", pass!!)
                }
                UserReg.FACEBOOK -> {
                    map.put("fb_token", token)
                }
                UserReg.GOOGLE -> {
                    map.put("gp_token", token)
                }
            }

            call = ApiInterface.create(this).login(map)
            call!!.enqueue(object : retrofit2.Callback<Model.ResponseLogin> {
                override fun onFailure(call: Call<Model.ResponseLogin>?, t: Throwable?) {
                    // cargar la pantalla principal, ya que el usuario si existe pero no tiene internet para este punto
                    println("Splash.onFailure --> error al cargar datos")
                    t!!.printStackTrace()
                    loadActivity(ACTIVITY_PRINCIPAL)
                }

                override fun onResponse(call: Call<Model.ResponseLogin>?, response: Response<Model.ResponseLogin>?) {
                    try {
                        println("Splash.onResponse --> ${response!!.body()}")
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                    if (response != null && response.isSuccessful && response.body() != null && response.body()!!.status == 200) {
                        config.userSession = true
                        Realm.getDefaultInstance().executeTransactionAsync({ realm ->
                            var isNewUser = false
                            var user = realm.where(UserModel::class.java).findFirst()
                            if (user == null) {
                                isNewUser = true
                                user = realm.createObject(UserModel::class.java, Integer.parseInt(response.body()!!.user.id))
                            }
                            user!!.email = response.body()!!.user.email
                            user.name = response.body()!!.user.nombre
                            user.password = pass

                            var userReg: UserReg = UserReg.NORMAL
                            if (response.body()!!.user.fb_reg == "SI")
                                userReg = UserReg.FACEBOOK
                            else if (response.body()!!.user.google_reg == "SI")
                                userReg = UserReg.GOOGLE

                            user.setUserReg(userReg)
                            user.setUserType(if (response.body()!!.user.tipo_usuario_id == "1") UserType.NORMAL else UserType.PROVIDER)

                            realm.where(Locations::class.java).findAll().deleteAllFromRealm()
                            if (response.body()!!.user.locations.isNotEmpty()) {
                                response.body()!!.user.locations.forEach { loca ->
                                    val newLoca = realm.createObject(Locations::class.java, loca.id)
                                    newLoca.alias = loca.alias
                                    newLoca.direccion = loca.direccion
                                    newLoca.colonia = loca.colonia
                                    newLoca.ciudad = loca.ciudad
                                    newLoca.estado = loca.estado
                                    newLoca.estadoId = loca.estadoId
                                    newLoca.cp = loca.cp
                                    newLoca.paisId = loca.paisId
                                    newLoca.pais = loca.pais
                                    newLoca.principal = loca.principal
                                    newLoca.telefono = loca.telefono

                                }
                            }

                            realm.where(Carrito::class.java).findAll().deleteAllFromRealm()
                            realm.where(ProductoCarrito::class.java).findAll().deleteAllFromRealm()

                            if (response.body()!!.user.carrito != null) {
                                val car = response.body()!!.user.carrito
                                val carrito = realm.createObject(Carrito::class.java, car!!.pedidoID)
                                carrito.cuponId = car.cuponId
                                carrito.cupon = car.cupon
                                carrito.cuponDesc = car.cuponDesc
                                carrito.subTotal = car.subTotal
                                carrito.total = car.total

                                if (car.productos.isNotEmpty()) {
                                    val list = RealmList<ProductoCarrito>()
                                    car.productos.forEach { p ->
                                        // producto del carrito
                                        val pCarrito = realm.createObject(ProductoCarrito::class.java, p.id)
                                        pCarrito.count = p.count
                                        list.add(pCarrito)
                                        // fin de agregar los productos del carrito

                                        // crear el producto si no existe o actualizarlo
                                        // (esto por si no se han descargado los productos pero si existen en el carrito)
                                        //var proReal = realm.where(Product::class.java).equalTo("id", p.id).findFirst()
                                        realm.where(Product::class.java).equalTo("id", p.id).equalTo("categoriaID", p.categoriaID).findAll().deleteAllFromRealm()
                                        val proReal = realm.createObject(Product::class.java)
                                        /*var isNewP = false
                                        if (proReal == null) {
                                            proReal = realm.createObject(Product::class.java, p.id)
                                            isNewP = true
                                        }*/
                                        proReal.id = p.id
                                        proReal.nameEs = p.nameEs
                                        proReal.nameEng = p.nameEng
                                        proReal.detalleEs = p.detalleEs
                                        proReal.detalleEng = p.detalleEng
                                        proReal.imagen = p.imagen
                                        proReal.precioPublico = p.precioPublico
                                        proReal.precioDistribuidores = p.precioDistribuidores
                                        proReal.categoriaID = p.categoriaID
                                        proReal.descuento = p.descuento
                                        proReal.stock = p.stock
                                        /*if (!isNewP) {
                                            realm.copyToRealmOrUpdate(proReal)
                                        }*/
                                        //fin de producto real
                                    }
                                    carrito.productos = list
                                }
                            }

                            if (response.body()!!.user.foto != null && !response.body()!!.user.foto!!.contains("avatar")) {
                                user.photo = response.body()!!.user.foto
                            }

                            if (!isNewUser) {
                                realm.copyToRealmOrUpdate(user)
                            }

                        }, {
                            println("Splash.onResponse --> datos del usuario guardados correctamente")
                            descargarPreferencias(userP.email)
                        }, { t ->
                            println("Splash.onResponse --> error al guardar datos del usuario")
                            t.printStackTrace()
                            descargarPreferencias(userP.email)
                        })
                    } else {
                        loadActivity(ACTIVITY_REGISTRO)
                    }
                }

            })
        } else {
            config.userSession = false
            loadActivity(ACTIVITY_REGISTRO)
        }
    }

    private fun descargarPreferencias(email: String) {
        DownloadPreferences(this, email, object : DownloadPreferences.OnPreferencesListener {
            override fun onSuccess() {
                println("Splash.onSuccess --> descarga de preferencias chidas")
                downloadCountries()
            }

            override fun onServerError() {
                println("Splash.onServerError --> error al descargarlas del servidor")
                downloadCountries()
            }

            override fun onInternalError() {
                println("Splash.onInternalError --> error al guardar las preferencias")
                downloadCountries()
            }
        })
    }

    private fun downloadCountries() {
        ApiInterface.create(this).getCountries().enqueue(object : retrofit2.Callback<Model.ResponsePaises> {
            override fun onFailure(call: Call<Model.ResponsePaises>?, t: Throwable?) {
                println("Splash.onFailure --> error al descargar paises")
                loadActivity(ACTIVITY_PRINCIPAL)
            }

            override fun onResponse(call: Call<Model.ResponsePaises>?, response: Response<Model.ResponsePaises>?) {
                if (response != null && response.isSuccessful && response.body() != null && response.body()!!.status == 200 && response.body()!!.paises.isNotEmpty()) {
                    Realm.getDefaultInstance().use { realm ->
                        realm.executeTransactionAsync({ realmBg ->
                            realmBg.where(Pais::class.java).findAll().deleteAllFromRealm()
                            response.body()!!.paises.forEach {
                                realmBg.copyToRealmOrUpdate(it)
                            }
                        }, {
                            println("Splash.onResponse --> paises guardados correctamente")
                            loadActivity(ACTIVITY_PRINCIPAL)
                        }, { t ->
                            println("Splash.onResponse --> error al guardar paises")
                            t.printStackTrace()
                            loadActivity(ACTIVITY_PRINCIPAL)
                        })
                    }
                } else
                    loadActivity(ACTIVITY_PRINCIPAL)
            }
        })
    }

    private fun loadActivity(activity: Int) {
        var intent: Intent? = null
        when (activity) {
            ACTIVITY_INTRO -> {
                intent = Intent(this@Splash, Introduccion::class.java)
            }
            ACTIVITY_PRINCIPAL -> {
                intent = Intent(this@Splash, Principal::class.java)
            }
            ACTIVITY_REGISTRO -> {
                intent = Intent(this@Splash, RegistroActivity::class.java)
            }
        }
        intent!!.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
        startActivity(intent)
        this.finish()
    }

    fun getApplicationSignature() {
        try {
            val signatureList: List<String>
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
                val info = this.packageManager.getPackageInfo("oomovil.com.appifix", PackageManager.GET_SIGNING_CERTIFICATES).signingInfo
                signatureList = if (info.hasMultipleSigners()) {
                    info.apkContentsSigners.map {
                        val digest = MessageDigest.getInstance("SHA")
                        digest.update(it.toByteArray())
                        bytesToHex(digest.digest())
                    }

                } else {
                    info.signingCertificateHistory.map {
                        val digest = MessageDigest.getInstance("SHA")
                        digest.update(it.toByteArray())
                        bytesToHex(digest.digest())
                    }
                }
            } else {
                val info = this.packageManager.getPackageInfo("oomovil.com.appifix", PackageManager.GET_SIGNATURES).signatures
                signatureList = info.map {
                    val digest = MessageDigest.getInstance("SHA")
                    digest.update(it.toByteArray())
                    bytesToHex(digest.digest())
                }
            }

            for (i in signatureList) {
                println("Splash.onCreate ---> " + Base64.encodeBytes(i.toByteArray()))
            }

        } catch (e: PackageManager.NameNotFoundException) {
            println("Splash.onCreate errorooooor")
        } catch (e: NoSuchAlgorithmException) {
            println("Splash.onCreate errorooooor 2 alan")
        }
    }
}

package oomovil.com.appifix.views

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.item_search.view.*
import oomovil.com.appifix.R
import oomovil.com.appifix.general.AppConfig
import oomovil.com.appifix.general.inflate
import oomovil.com.appifix.general.loadUrl
import oomovil.com.appifix.models.Product

/**
 * Created by Alan on 04/12/2017.
 */
class AdapterBusqueda(private var items: List<Product>?, val context: Context, private val click: OnClickSearch) : RecyclerView.Adapter<AdapterBusqueda.ViewHolder>() {
    override fun onBindViewHolder(p0: ViewHolder, p1: Int) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    private val config = AppConfig(context)

    interface OnClickSearch{
        fun onClick(product: Product)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder = ViewHolder(parent.inflate(R.layout.item_search, false))

    override fun onBindViewHolder(holder: ViewHolder, position: Int, payloads: MutableList<Any>) {
        val pro = items!![position]

        println("AdapterBusqueda.onBindViewHolder --> ${pro.imagen}")
        holder.itemView.img_prod.loadUrl(pro.imagen, R.color.gris, null)
        holder.itemView.txt_name.text = if(config.language == "ES") pro.nameEs else pro.nameEng

        holder.itemView.laySearch.setOnClickListener {
            click.onClick(items!![holder.adapterPosition])
        }
    }

    override fun getItemCount(): Int = items!!.size

    class ViewHolder(val view: View) : RecyclerView.ViewHolder(view)
}
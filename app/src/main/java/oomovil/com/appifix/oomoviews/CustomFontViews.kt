package oomovil.com.appifix.oomoviews

import android.content.Context
import android.graphics.Typeface
import android.support.design.widget.TextInputEditText
import android.support.v7.widget.*
import android.util.AttributeSet
import oomovil.com.appifix.R

/**
 * Created by Alan on 17/10/2017.
 */

private fun readTypeFace(context: Context, attrs: AttributeSet?): Typeface? {
    attrs ?: return null
    val a = context.theme.obtainStyledAttributes(attrs, R.styleable.OomovilViews, 0, 0)
    return try {
        val fontName = a.getString(R.styleable.OomovilViews_type_face) ?: return null
        Typeface.createFromAsset(context.assets, fontName)
    } catch (e: Exception) {
        null
    } finally {
        a?.recycle()
    }
}

class OTextView @JvmOverloads constructor(
        context: Context,
        attrs: AttributeSet? = null,
        defStyleAttr: Int = android.R.attr.textViewStyle) : AppCompatTextView(context, attrs, defStyleAttr) {
    init {
        val fontface = readTypeFace(context, attrs)
        if (fontface != null) {
            var style = Typeface.NORMAL
            try {
                style = typeface.style
            } catch (e: Exception) {
            }
            setTypeface(fontface, style)
        }
    }
}

class OButton @JvmOverloads constructor(
        context: Context,
        attrs: AttributeSet? = null,
        defStyleAttr: Int = android.R.attr.buttonStyle) : AppCompatButton(context, attrs, defStyleAttr){
    init {
        val fontface = readTypeFace(context, attrs)
        if (fontface != null) {
            var style = Typeface.NORMAL
            try {
                style = typeface.style
            } catch (e: Exception) {
            }
            setTypeface(fontface, style)
        }
    }
}

class OCheckBox @JvmOverloads constructor(
        context: Context,
        attrs: AttributeSet? = null,
        defStyleAttr: Int = android.R.attr.checkboxStyle) : AppCompatCheckBox(context, attrs, defStyleAttr){
    init {
        val fontface = readTypeFace(context, attrs)
        if (fontface != null) {
            var style = Typeface.NORMAL
            try {
                style = typeface.style
            } catch (e: Exception) {
            }
            setTypeface(fontface, style)
        }
    }
}

class OEdittext @JvmOverloads constructor(
        context: Context,
        attrs: AttributeSet? = null,
        defStyleAttr: Int = android.R.attr.editTextStyle) : TextInputEditText(context, attrs, defStyleAttr){
    init {
        val fontface = readTypeFace(context, attrs)
        if (fontface != null) {
            var style = Typeface.NORMAL
            try {
                style = typeface.style
            } catch (e: Exception) {
            }
            setTypeface(fontface, style)
        }
    }
}

class ORadioButton @JvmOverloads constructor(
        context: Context,
        attrs: AttributeSet? = null,
        defStyleAttr: Int = android.R.attr.radioButtonStyle) : AppCompatRadioButton(context, attrs, defStyleAttr){
    init {
        val fontface = readTypeFace(context, attrs)
        if (fontface != null) {
            var style = Typeface.NORMAL
            try {
                style = typeface.style
            } catch (e: Exception) {
            }
            setTypeface(fontface, style)
        }
    }
}

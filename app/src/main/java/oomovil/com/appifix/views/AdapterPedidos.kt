package oomovil.com.appifix.views

import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.graphics.PorterDuff
import android.os.Handler
import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import com.chauthai.swipereveallayout.ViewBinderHelper
import io.realm.RealmResults
import kotlinx.android.synthetic.main.fragment_carrito.*
import kotlinx.android.synthetic.main.item_categorias.view.*
import kotlinx.android.synthetic.main.item_locations.view.*
import kotlinx.android.synthetic.main.item_pedido.view.*
import oomovil.com.appifix.R
import oomovil.com.appifix.general.AppConfig
import oomovil.com.appifix.general.formatNumberDecimal
import oomovil.com.appifix.general.inflate
import oomovil.com.appifix.models.Locations
import oomovil.com.appifix.models.Pedido
import oomovil.com.appifix.principal.ActivityAddLocation
import oomovil.com.appifix.principal.OnFragmentListener

/**
 * Created by Martin on 20/02/18.
 */
class AdapterPedidos(private var items: RealmResults<Pedido>?, val context: Context, private val itemClick: OnClick) : RecyclerView.Adapter<AdapterPedidos.ViewHolder>() {

    override fun onBindViewHolder(p0: ViewHolder, p1: Int) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    interface OnClick {
        fun onCLick(position: Int,id: String)
    }

    override fun getItemCount(): Int = items!!.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder = ViewHolder(parent.inflate(R.layout.item_pedido, false))

    override fun onBindViewHolder(holder: ViewHolder, position: Int, payloads: MutableList<Any>) {
        holder.itemView.txt_fecha.text = items!![position]!!.date
        val total = items!![position]!!.total

        var totalT = "---"
        if (total != null && total.toDoubleOrNull() != null){
            totalT = "$${formatNumberDecimal(total.toDouble())}"
        }
        holder.itemView.txt_total.text = "Total: ${totalT}"
        holder.itemView.txt_status.text = items!![position]!!.status

        holder.itemView.layContainerPedidos.setOnClickListener {
            itemClick.onCLick(position,items!![position]!!.id!!)
        }
    }

    private fun isDoubleInteger(value: Double): Boolean {
        val integer = (value % 1)
        return integer == 0.0
    }

    class ViewHolder(val view: View) : RecyclerView.ViewHolder(view)
}
package oomovil.com.appifix.oomoviews

import android.content.Context
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.widget.Toast
import oomovil.com.appifix.R
import kotlinx.android.synthetic.main.custom_toast.view.*

/**
 * Created by Alan on 17/10/2017.
 */
class OToast(val context: Context) {

    private var toast: Toast? = null

    constructor(context: Context, text: String) : this(context) {
        toast = Toast(context)
        toast!!.duration = Toast.LENGTH_LONG
        toast!!.setGravity(Gravity.BOTTOM or Gravity.FILL_HORIZONTAL, 0, 0)
        toast!!.setMargin(0f, 16f)
        val li : LayoutInflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val vi : View = li.inflate(R.layout.custom_toast, null)
        toast!!.view = vi
        vi.msg_toast.text = text
    }

    fun show() {
        if (toast != null) {
            toast!!.show()
        }
    }

    companion object {

        fun makeText(context: Context, text: String): OToast = OToast(context, text)

        fun makeText(context: Context, text: Int): OToast {
            val txt : String = context.getString(text)
            return OToast(context, txt)
        }

    }
}
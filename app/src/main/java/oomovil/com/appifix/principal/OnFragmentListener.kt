package oomovil.com.appifix.principal

import android.os.Bundle
import android.view.View

/**
 * Created by Alan on 24/10/2017.
 */
interface OnFragmentListener {

    companion object {
        val FRAGMENT_CATEGORIA: Int get() = 1
        val FRAGMENT_PRODUCTOS: Int get() = 2
        val FRAGMENT_PRODUCTO_DETALLE: Int get() = 3
        val FRAGMENT_PERFIL: Int get() = 4
        val FRAGMENT_TARJETAS: Int get() = 5
        val FRAGMENT_UBICACIONES: Int get() = 6
        val FRAGMENT_HISTORIAL: Int get() = 7
        val FRAGMENT_NOTIFICACIONES: Int get() = 8
        val FRAGMENT_NOTIFICACIONES_DETAIL: Int get() = 9
        val FRAGMENT_CARRITO: Int get() = 10
        val FRAGMENT_HISTORIAL_DETAIL: Int get() = 11
        val FRAGMENT_SEGUIMIENTO: Int get() = 12
    }

    /***
     * @fragment -> es la referencia al fragment que se va a cargar
     * @viewList -> lista de vistas con su transitionName
     * @data -> lista de datos <Clave, Valor> para crear un bundle
     */
    fun changeViewTransition(fragment: Int, id: String, viewList: List<Pair<View, String>>, data: List<Pair<String, String?>>)
    fun changeView(fragment: Int, id: String)
}
package oomovil.com.appifix.models

import io.realm.RealmObject
import io.realm.annotations.PrimaryKey

/**
 * Created by Alan on 09/11/2017.
 */

open class UserModel : RealmObject() {

    @PrimaryKey
    open var idUser: Int = -1
    open var name: String = ""
    open var email: String = ""
    open var password: String? = null
    open var photo: String? = null
    open var userType: String = UserType.NORMAL.toString()
    open var userReg: String = UserReg.NORMAL.toString()
    open var token: String = ""

    fun setUserType(type: UserType) {
        this.userType = type.toString()
    }

    fun getUserType(): UserType = UserType.valueOf(this.userType)

    fun setUserReg(reg: UserReg) {
        this.userReg = reg.toString()
    }

    fun getUserReg(): UserReg = UserReg.valueOf(this.userReg)
}

enum class UserReg {
    NORMAL, GOOGLE, FACEBOOK
}

enum class UserType {
    NORMAL, PROVIDER
}
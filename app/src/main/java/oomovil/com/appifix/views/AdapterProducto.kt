package oomovil.com.appifix.views

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.os.Build
import android.support.v7.app.AlertDialog
import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import io.realm.Realm
import io.realm.RealmResults
import kotlinx.android.synthetic.main.item_producto.view.*
import oomovil.com.appifix.R
import oomovil.com.appifix.general.*
import oomovil.com.appifix.models.Product
import oomovil.com.appifix.models.UserModel
import oomovil.com.appifix.models.UserType
import oomovil.com.appifix.principal.ActivityAddCarrito
import oomovil.com.appifix.principal.OnFragmentListener
import java.text.DecimalFormat
import java.text.DecimalFormatSymbols
import java.util.*

/**
 * Created by Alan on 24/11/2017.
 */
class AdapterProducto(private var items: RealmResults<Product>?, val context: Context, private val itemClick: OnRecyclerItemClick) : RecyclerView.Adapter<AdapterProducto.ViewHolder>() {

    override fun onBindViewHolder(p0: ViewHolder, p1: Int) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    private val config = AppConfig(context)
    private val user = Realm.getDefaultInstance().where(UserModel::class.java).findFirst()

    override fun getItemCount() = items!!.size

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: ViewHolder, position: Int, payloads: MutableList<Any>) {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            holder.itemView.img_prod.transitionName = "transitionImg" + holder.adapterPosition
            holder.itemView.txt_precio.transitionName = "transitionTxtPrecio" + holder.adapterPosition
            holder.itemView.title_prod.transitionName = "transitionTxtTitle" + holder.adapterPosition
        }

        val pro = items!![position]

        val precio: Double = if (user!!.getUserType() == UserType.NORMAL) pro!!.precioPublico.toDouble() else pro!!.precioDistribuidores.toDouble() //TODO WATCH THIS

        holder.itemView.txt_precio.text = "$" + if (isDoubleInteger(precio)) formatNumberDecimal(precio.toInt()) else formatNumberDecimal(precio)

        if (pro.descuento != null && pro.descuento != "0.00") {
            val desc = round(pro.descuento!!)
            val descChido = "" + if (isDoubleInteger(desc)) formatNumberDecimal(desc.toInt()) else {
                formatNumberDecimal(desc)
            }
            holder.itemView.txt_desc.text = "$descChido% ${if (config.language == "ES") " desc" else " off"}"
        } else {
            holder.itemView.txt_desc.visibility = View.GONE
        }
        holder.itemView.img_prod.loadUrl(pro.imagen, R.drawable.img_no_disponible, null)
        holder.itemView.title_prod.text = if (config.language == "ES") pro.nameEs else pro.nameEng

        holder.itemView.btn_add_carrito.setOnClickListener {

            holder.itemView.btn_add_carrito.isClickable = !(holder.itemView.hasOnClickListeners())
            itemClick.onClick(-1, pro.id, pro.categoriaID)

        }

        holder.itemView.cardProd.setOnClickListener {
            itemClick.onClick(OnFragmentListener.FRAGMENT_PRODUCTO_DETALLE, pro.id + "¬oOMovil¬" + pro.categoriaID,
                    listOf(Pair(holder.itemView.img_prod, "transitionImg" + holder.adapterPosition), Pair(holder.itemView.txt_precio, "transitionTxtPrecio" + holder.adapterPosition), Pair(holder.itemView.title_prod, "transitionTxtTitle" + holder.adapterPosition)),
                    listOf(Pair("imgUrl", pro.imagen), Pair("transitionNameImg", "transitionImg" + holder.adapterPosition), Pair("transitionTxtPrecio", "transitionTxtPrecio" + holder.adapterPosition), Pair("transitionTxtTitle", "transitionTxtTitle" + holder.adapterPosition)))
        }
    }

    private fun isDoubleInteger(value: Double): Boolean {
        val integer = (value % 3)
        println("AdapterProducto.isDoubleInteger $integer --> ${integer == 0.000}")
        return integer == 0.000
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder = ViewHolder(parent.inflate(R.layout.item_producto, false))

    class ViewHolder(val view: View) : RecyclerView.ViewHolder(view)

}
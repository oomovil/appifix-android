package oomovil.com.appifix.principal

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.item_image.*
import oomovil.com.appifix.R
import oomovil.com.appifix.general.loadUrl

/**
 * Created by Alan on 07/12/2017.
 */
class FragmentImage : Fragment() {

    private lateinit var image: String

    companion object {
        fun getInstance(url: String): FragmentImage {
            val b = Bundle()
            b.putString("url", url)
            val f = FragmentImage()
            f.arguments = b
            return f
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?):
            View? = inflater!!.inflate(R.layout.item_image, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        image = arguments!!.getString("url") ?: img_noti.loadUrl(image, R.drawable.img_no_disponible, null).toString()
    }

}
package oomovil.com.appifix.principal

import android.app.Activity
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import kotlinx.android.synthetic.main.activity_pop_up_paqueterias.*
import oomovil.com.appifix.R
import oomovil.com.appifix.models.Model
import oomovil.com.appifix.retrofit.ApiInterface
import oomovil.com.appifix.views.AdapterPaqueterias
import retrofit2.Call
import retrofit2.Response

class PopUpPaqueterias : Activity(), AdapterPaqueterias.onPaqSelected {

    companion object {
        var delegatePop: onPaqSelectedPopup? = null
    }

    private var idPaqueteria: String? = null
    private var shortName: String = ""
    private var cargo: String = ""
    private var dias: String = ""
    private var namePaqueteria: String = ""

    interface onPaqSelectedPopup {
        fun onPaqSelectedPopup(idPaqueteria: String, shortName: String, cargo: String, dias: String, name: String)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_pop_up_paqueterias)
        window.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        window.attributes.windowAnimations = R.style.dialog_animation
        recyclerPaqueterias.layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        btn_cancelar_paq.setOnClickListener {
            onBackPressed()
        }
        btn_aplicar_paq.setOnClickListener {
            if (delegatePop != null && idPaqueteria != null) {

                delegatePop!!.onPaqSelectedPopup(idPaqueteria!!, shortName, cargo, dias, namePaqueteria)
                onBackPressed()
            }
        }
        btn_reintentar.setOnClickListener {
            loadProductos(intent.getStringExtra("idOrden"), intent.getStringExtra("idUbicacion"))
        }
    }

    override fun onResume() {
        super.onResume()
        loadProductos(intent.getStringExtra("idOrden"), intent.getStringExtra("idUbicacion"))
    }

    private fun loadProductos(idOrden: String, idUbicacion: String) {
        progressPaq.visibility = View.VISIBLE
        txt_error.visibility = View.INVISIBLE
        btn_reintentar.visibility = View.INVISIBLE
        val map = HashMap<String, String>()
        map.put("idOrder", idOrden)
        map.put("idUbicacion", idUbicacion)
        println("PopUpPaqueterias.loadProductos --> $map")
        val call = ApiInterface.create(this).getCuotaDHL(map)
        call.enqueue(object : retrofit2.Callback<Model.ResponseCuotasPaqueterias> {
            override fun onResponse(call: Call<Model.ResponseCuotasPaqueterias>?, response: Response<Model.ResponseCuotasPaqueterias>?) {
                progressPaq.visibility = View.GONE
                if (response != null && response.isSuccessful && response.body() != null && response.body()!!.status == 200 /*&& response.body()!!.envios.isNotEmpty()*/) {
                    recyclerPaqueterias.visibility = View.VISIBLE
                    recyclerPaqueterias.adapter = AdapterPaqueterias(response.body()!!.envios, this@PopUpPaqueterias, this@PopUpPaqueterias)
                } else {
                    txt_error.visibility = View.VISIBLE
                    btn_reintentar.visibility = View.VISIBLE
                }
            }

            override fun onFailure(call: Call<Model.ResponseCuotasPaqueterias>?, t: Throwable?) {
                progressPaq.visibility = View.GONE
                txt_error.visibility = View.VISIBLE
                btn_reintentar.visibility = View.VISIBLE
            }
        })
    }

    override fun onPaqSelected(idPaqueteria: String?, shortName: String, cargo: String, days: String, name: String) {
        this.idPaqueteria = idPaqueteria
        this.shortName = shortName
        this.cargo = cargo
        this.dias = days
        this.namePaqueteria = name
        if (idPaqueteria != null) {
            btn_aplicar_paq.setBackgroundResource(R.drawable.btn_round_fb)
        } else {
            btn_aplicar_paq.setBackgroundResource(R.drawable.btn_round_gris)
        }
    }
}

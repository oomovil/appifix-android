package oomovil.com.appifix.principal

import android.content.Context
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_pop_up_success.*
import oomovil.com.appifix.R
import oomovil.com.appifix.general.AppConfig
import oomovil.com.appifix.general.MyContextWrapper

class PopUpSuccess : AppCompatActivity() {

    override fun attachBaseContext(newBase: Context) {
        super.attachBaseContext(MyContextWrapper.wrap(newBase, AppConfig(newBase).language))
    }

    companion object{
        val TYPE = "type"
        val UBICACION: Int = 1
        val UBICACION_UPDATE: Int = 2
        val TARJETA: Int = 3
        val TARJETA_UPDATE: Int = 4
        val COMPRA: Int = 5
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_pop_up_success)

        val type = intent.getIntExtra(TYPE, UBICACION)

        when (type) {
            UBICACION -> {
                img_type.setImageResource(R.mipmap.grafico_ubicacion_guardada)
                txt_type.text = getString(R.string.ubicacion_guardada)
            }
            UBICACION_UPDATE -> {
                img_type.setImageResource(R.mipmap.grafico_ubicacion_guardada)
                txt_type.text = getString(R.string.ubicacion_actualizada_correcta)
            }
            TARJETA -> {
                img_type.setImageResource(R.mipmap.grafico_tarjeta_guardada)
                txt_type.text = getString(R.string.tarjeta_guardada)
            }
            TARJETA_UPDATE -> {
                img_type.setImageResource(R.mipmap.grafico_tarjeta_guardada)
                txt_type.text = getString(R.string.tarjeta_actualizada_exito)
            }
            COMPRA -> {
                img_type.setImageResource(R.mipmap.grafico_confirmacion_compra)
                txt_type.text = getString(R.string.compra_exitosa)
            }
        }

        btn_aceptar.setOnClickListener {
            onBackPressed()
        }
    }
}

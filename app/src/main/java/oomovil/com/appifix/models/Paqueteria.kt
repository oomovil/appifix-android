package oomovil.com.appifix.models

import com.google.gson.annotations.SerializedName
import io.realm.RealmObject
import io.realm.annotations.PrimaryKey

/**
 * Created by Martin on 23/02/18.
 */
class Envio {
    @SerializedName("ProductShortName")
    var nombre: String? = null
    @SerializedName("TotalTransitDays")
    var dias: String? = null
    @SerializedName("ShippingCharge")
    var precio: String? = null
    @SerializedName("GlobalProductCode")
    var codigo: String? = null
    @SerializedName("nombre")
    var paqueteria: String? = null
    var isSelected: Boolean = false
}
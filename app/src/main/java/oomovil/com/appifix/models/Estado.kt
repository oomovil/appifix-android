package oomovil.com.appifix.models

import com.google.gson.annotations.SerializedName
import io.realm.RealmObject
import io.realm.annotations.PrimaryKey

/**
 * Created by Noe on 09/18/2018.
 */
open class Estado: RealmObject() {
    @PrimaryKey
    @SerializedName("id")
    var id: String? = null
    @SerializedName("id_pais")
    var id_pais: String? = null
    @SerializedName("estado")
    var name: String? = null
    @SerializedName("estado_eng")
    var name_eng: String? = null
    @SerializedName("codigo")
    var codigo: String? = null

    fun toMyString(): String = "Estado(id=$id, name=$name)"
}
package oomovil.com.appifix.general

/**
 * Created by Alan on 22/11/2017.
 */
class CreditCardType {
    enum class Type{
        VISA, MASTER, AEXPRESS, DINERS, DISCOVER, JBC, INVALID
    }

    companion object {

        private var regVisa = Regex("^4[0-9]{12}(?:[0-9]{3})?$")
        private var regMaster = Regex("^5[1-5][0-9]{14}$")
        private var regExpress = Regex("^3[47][0-9]{13}$")
        private var regDiners = Regex("^3(?:0[0-5]|[68][0-9])[0-9]{11}$")
        private var regDiscover = Regex("^6(?:011|5[0-9]{2})[0-9]{12}$")
        private var regJCB = Regex("^(?:2131|1800|35\\d{3})\\d{11}$")

        fun getCreditCardType(number: String): Type = when {
            regVisa.matches(number) -> Type.VISA
            regMaster.matches(number) -> Type.MASTER
            regExpress.matches(number) -> Type.AEXPRESS
            regDiners.matches(number) -> Type.DINERS
            regDiscover.matches(number) -> Type.DISCOVER
            regJCB.matches(number) -> Type.JBC
            else -> Type.INVALID
        }
    }
}
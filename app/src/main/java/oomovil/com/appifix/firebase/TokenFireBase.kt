package oomovil.com.appifix.firebase

import android.content.Context
import io.realm.Realm
import oomovil.com.appifix.general.AppConfig
import oomovil.com.appifix.models.Model
import oomovil.com.appifix.models.UserModel
import oomovil.com.appifix.retrofit.ApiInterface
import retrofit2.Call
import retrofit2.Callback
import java.util.HashMap

/**
 * Created by Alan on 16/11/2017.
 */
class TokenFireBase {

    fun sendToken(token: String, context: Context) {
        val map = HashMap<String, String>()
        map.put("token", token)
        map.put("android", "SI")
        map.put("lang", AppConfig(context).language)
        map.put("email", Realm.getDefaultInstance().where(UserModel::class.java).findFirst()!!.email)

        println("TokenFireBase.sendToken --> " + map)

        val apiService = ApiInterface.create(context)
        val call = apiService.regToken(map)
        call.enqueue(object : Callback<Model.Response> {
            override fun onResponse(call: Call<Model.Response>, response: retrofit2.Response<Model.Response>) {
                System.out.println("TokenFireBase.onResponse --> " + response.body()!!.message)
            }

            override fun onFailure(call: Call<Model.Response>, t: Throwable) {
                println("TokenFireBase.onResponse --> " + t.message)
            }
        })
    }
}
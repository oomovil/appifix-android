package oomovil.com.appifix.principal

import android.animation.ValueAnimator
import android.annotation.SuppressLint
import android.app.Activity
import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.content.pm.ActivityInfo
import android.graphics.Bitmap
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.Drawable
import android.os.Bundle
import android.support.annotation.StringRes
import android.view.View
import android.view.Window
import android.view.WindowManager
import android.widget.Toast
import com.android.volley.DefaultRetryPolicy
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.squareup.picasso.Picasso
import com.squareup.picasso.Request
import com.squareup.picasso.Target
import io.realm.Realm
import io.realm.RealmList
import kotlinx.android.synthetic.main.activity_add_carrito.*
import kotlinx.android.synthetic.main.item_producto.view.*
import oomovil.com.appifix.R
import oomovil.com.appifix.general.*
import oomovil.com.appifix.models.*
import oomovil.com.appifix.oomoviews.OToast
import oomovil.com.appifix.retrofit.ApiClient
import oomovil.com.appifix.retrofit.ApiInterface
import oomovil.com.appifix.retrofit.VolleyS
import org.json.JSONArray
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Response

class ActivityAddCarrito : Activity(), View.OnClickListener {

    private var call: Call<Model.ResponseProductsById>? = null
    private var numProdus = 1
    private val MIM_NUM_PRODS = 1
    private var MAX_NUM_PRODS = 1
    private var prod: Product? = null
    private val user = Realm.getDefaultInstance().where(UserModel::class.java).findFirst()
    private var action = ADD
    private var flag = false
    private var carrito: ProductoCarrito? = null
    private val dialogLo: AlertDialog by lazy {
        this.dialogView()
    }

    companion object {
        val ADD = 1
        val UPDATE = 2
        val INTENT_ACTION = "action"
        val INTENT_NUM_PRODS = "num_prods"
    }

    override fun attachBaseContext(newBase: Context) {
        super.attachBaseContext(MyContextWrapper.wrap(newBase, AppConfig(newBase).language))
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        setContentView(R.layout.activity_add_carrito)
        window.setBackgroundDrawable(ColorDrawable(0x7000000))
        window.addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND)
        val lp = window.attributes
        lp.dimAmount = 0.65f
        //window.attributes.windowAnimations = R.style.dialog_animation
        setFinishOnTouchOutside(false)

        btn_close.setColorFilter(Color.BLACK)

        prod = Realm.getDefaultInstance().where(Product::class.java)
                .equalTo("id", intent.getStringExtra("id"))
                .equalTo("categoriaID", intent.getStringExtra("idCategoria"))
                .findFirst()

        action = intent.getIntExtra(INTENT_ACTION, ADD)
        numProdus = intent.getIntExtra(INTENT_NUM_PRODS, MIM_NUM_PRODS)
        println("ActivityAddCarrito.onCreate --> $numProdus --> ${prod!!.id} --> $action")

        /*
        if (action == UPDATE) {
            btn_add.text = getString(R.string.guardar)
        }
        */

        btn_close.setOnClickListener(this)
        btn_menos.setOnClickListener(this)
        btn_mas.setOnClickListener(this)
        btn_add.setOnClickListener(this)
        //showData()
        loadStock()



    }

    override fun onResume() {
        super.onResume()
    }

    @SuppressLint("SetTextI18n")
    private fun showData() {
        if (prod != null) {

            carrito = Realm.getDefaultInstance().where(ProductoCarrito::class.java).equalTo("id", intent.getStringExtra("id")).findFirst()

            val config = AppConfig(this)
            //img_product_add.loadUrl(prod!!.imagen, R.drawable.img_no_disponible, null)
            txt_name.text = if (config.language == "ES") prod!!.nameEs else prod!!.nameEng

            val precio: Double = if (user!!.getUserType() == UserType.NORMAL) prod!!.precioPublico.toDouble() else prod!!.precioDistribuidores.toDouble() //TODO WATCH THIS
            txt_precio.text = "$" + if (isDoubleInteger(precio)) formatNumberDecimal(precio.toInt()) else formatNumberDecimal(precio)

            if (prod!!.descuento != null && prod!!.descuento != "0.00") {
                val desc = round(prod!!.descuento!!)
                val descChido = "" + if (isDoubleInteger(desc)) formatNumberDecimal(desc.toInt()) else {
                    formatNumberDecimal(desc)
                }
                txt_descuento.text = "$descChido% ${if (config.language == "ES") " desc" else " off"}"
            } else {
                txt_descuento.visibility = View.GONE
            }

            title_total.text = "Disponibles: ${prod!!.stock}"
            println("Disponibles: ${prod!!.stock}")
            //MAX_NUM_PRODS = prod!!.stock

            Picasso.get().load(prod!!.imagen).into(object : Target {
                override fun onPrepareLoad(placeHolderDrawable: Drawable?) {

                }

                override fun onBitmapFailed(e: java.lang.Exception?, errorDrawable: Drawable?) {
                    img_product_add.setImageResource(R.drawable.img_no_disponible)
                }

                override fun onBitmapLoaded(bitmap: Bitmap?, from: Picasso.LoadedFrom?) {
                    var bmp = bitmap!!
                    bmp = ImageUtils.blurRenderScript(bmp, 22, 8, this@ActivityAddCarrito)
                    img_product_add.setImageBitmap(bmp)
                }

            })
            //txt_count.text = "$numProdus"
            if (carrito != null)
                numProdus = carrito?.count!!.toInt()
            txt_count.text = "$numProdus"
            calculateTotal()
        } else {
            onBackPressed()
        }
    }

    @SuppressLint("SetTextI18n")
    private fun calculateTotal() {
        var descuento = 0.0
        val precio: Double = if (user!!.getUserType() == UserType.NORMAL) prod!!.precioPublico.toDouble() else prod!!.precioDistribuidores.toDouble() //TODO WATCH  THIS
        /**
         * Quite multiplicacion de descuento
         */
        /*
        if (prod!!.descuento != null && prod!!.descuento != "0.00") {
            var desc = round(prod!!.descuento!!)
            desc /= 100
            descuento = desc * precio
        }
        */

        var total = precio - descuento
        total *= numProdus
        //total = round(total) //TODO WATCH  THIS
        animateTotal(precio, total)
    }

    @SuppressLint("SetTextI18n")
    private fun animateTotal(init: Double, final: Double) {
        val valueAnimator = ValueAnimator.ofFloat(init.toFloat(), final.toFloat())
        valueAnimator.duration = 300
        valueAnimator.addUpdateListener {
            txt_total.text = "Total = $" + formatNumberDecimal(it.animatedValue)
        }
        valueAnimator.start()
    }

    private fun isDoubleInteger(value: Double): Boolean {
        val integer = (value % 1)
        return integer == 0.0
    }

    override fun onClick(p0: View?) {
        when (p0!!.id) {
            R.id.btn_close -> {
                onBackPressed()
            }
            R.id.btn_menos -> {
                if (numProdus > MIM_NUM_PRODS) {
                    numProdus -= 1
                    txt_count.text = "$numProdus"
                    calculateTotal()
                }
            }
            R.id.btn_mas -> {
                if (numProdus < MAX_NUM_PRODS) {
                    numProdus += 1
                    txt_count.text = "$numProdus"
                    calculateTotal()
                }
            }
            R.id.btn_add -> {
                validarCarrito()
            }
        }
    }

    private fun validarCarrito() {
        val carrito = Realm.getDefaultInstance().where(Carrito::class.java).findFirst()
        when {
            carrito == null -> crearCarrito()
            action == ADD -> addToCArrito(carrito)
            action == UPDATE -> actualizarCarrito(carrito)
        }
    }

    private fun crearCarrito() {
        println("ActivityAddCarrito.crearCarrito --> crearCarrito")
        showProgress()
        val map = HashMap<String, String>()
        map.put("email", user!!.email)
        map.put("lang", AppConfig(this).language)
        map.put("distribuidor", if (user.getUserType() == UserType.PROVIDER) "SI" else "NO")
        val array = JSONArray()
        val obj = JSONObject()
        obj.put("producto_id", prod!!.id)
        obj.put("cantidad", "$numProdus")
        array.put(obj)
        map.put("prods", array.toString())
        println("ActivityAddCarrito.crearCarrito --> $map")
        ApiInterface.create(this).saveNewCarrito(map).enqueue(object : retrofit2.Callback<Model.ResponseSaveCarrito> {
            override fun onFailure(call: Call<Model.ResponseSaveCarrito>?, t: Throwable?) {
                hideProgress()
                OToast.makeText(this@ActivityAddCarrito, R.string.error_conexion).show()
            }

            override fun onResponse(call: Call<Model.ResponseSaveCarrito>?, response: Response<Model.ResponseSaveCarrito>?) {
                try {
                    println("ActivityAddCarrito.onResponse --> ${response!!.body()}")
                } catch (e: Exception) {
                    e.printStackTrace()
                }
                if (response != null && response.isSuccessful && response.body() != null && response.body()!!.status == 200) {
                    saveNewCarrito(response.body()!!.carrito)
                } else {
                    hideProgress()
                    val msg: String = response?.body()?.message ?: getString(R.string.error_guardar_carrito)
                    OToast.makeText(this@ActivityAddCarrito, msg).show()
                }
            }

        })
    }

    private fun addToCArrito(carrito: Carrito) {
        println("ActivityAddCarrito.addToCArrito --> agregar al carrito")
        showProgress()
        val map = HashMap<String, String>()
        map.put("distribuidor", if (user!!.getUserType() == UserType.PROVIDER) "SI" else "NO")
        map.put("pedido_id", "${carrito.pedidoID}")
        map.put("cant", "$numProdus")
        map.put("prod_id", prod!!.id)
        map.put("lang", AppConfig(this).language)
        println("ActivityAddCarrito.addToCArrito --> $map")
        ApiInterface.create(this).addToCarrito(map).enqueue(object : retrofit2.Callback<Model.ResponseSaveCarrito> {
            override fun onFailure(call: Call<Model.ResponseSaveCarrito>?, t: Throwable?) {
                t?.printStackTrace()
                hideProgress()
                OToast.makeText(this@ActivityAddCarrito, R.string.error_conexion).show()
            }

            override fun onResponse(call: Call<Model.ResponseSaveCarrito>?, response: Response<Model.ResponseSaveCarrito>?) {
                try {
                    println("ActivityAddCarrito.onResponse --> ${response!!.body()}")
                } catch (e: Exception) {
                    e.printStackTrace()
                }
                if (response != null && response.isSuccessful && response.body() != null && response.body()!!.status == 200) {
                    UpdateProdToCarrito(response.body()!!.carrito!!, carrito.pedidoID)
                } else {
                    hideProgress()
                    val msg: String = response?.body()?.message ?: getString(R.string.error_guardar_carrito)
                    OToast.makeText(this@ActivityAddCarrito, msg).show()
                }
            }
        })
    }

    private fun actualizarCarrito(carrito: Carrito) {
        println("ActivityAddCarrito.actualizarCarrito --> actualizar parametros del carrito")

        val pCarrito = Realm.getDefaultInstance().where(ProductoCarrito::class.java).equalTo("id", prod!!.id).findFirst()
        /*
        if (pCarrito != null && pCarrito.count.toInt() + numProdus > MAX_NUM_PRODS) {
            OToast.makeText(this, "Se ha excedido la cantidad de productos").show()
            return
        }
        */
        showProgress()
        val map = HashMap<String, String>()
        map.put("distribuidor", if (user!!.getUserType() == UserType.PROVIDER) "SI" else "NO")
        map.put("pedido_id", "${carrito.pedidoID}")
        map.put("cant", "$numProdus")
        map.put("prod_id", prod!!.id)
        map.put("lang", AppConfig(this).language)
        println("ActivityAddCarrito.actualizarCarrito --> $map")
        ApiInterface.create(this).updateToCarrito(map).enqueue(object : retrofit2.Callback<Model.ResponseSaveCarrito> {
            override fun onFailure(call: Call<Model.ResponseSaveCarrito>?, t: Throwable?) {
                hideProgress()
                OToast.makeText(this@ActivityAddCarrito, R.string.error_conexion).show()
            }

            override fun onResponse(call: Call<Model.ResponseSaveCarrito>?, response: Response<Model.ResponseSaveCarrito>?) {
                try {
                    println("ActivityAddCarrito.onResponse --> ${response!!.body()}")
                } catch (e: Exception) {
                    e.printStackTrace()
                }
                if (response != null && response.isSuccessful && response.body() != null && response.body()!!.status == 200) {
                    addOrUpdateProdToCarrito(response.body()!!.carrito!!, carrito.pedidoID)
                } else {
                    hideProgress()
                    val msg: String = response?.body()?.message ?: getString(R.string.error_guardar_carrito)
                    OToast.makeText(this@ActivityAddCarrito, msg).show()
                }
            }

        })
    }

    private fun saveNewCarrito(carrito: Carrito?) {
        Realm.getDefaultInstance().executeTransactionAsync({ realmBg ->
            realmBg.where(Carrito::class.java).findAll().deleteAllFromRealm()
            realmBg.where(ProductoCarrito::class.java).findAll().deleteAllFromRealm()
            val car = realmBg.createObject(Carrito::class.java, carrito!!.pedidoID)

            car.cuponId = carrito.cuponId
            car.cupon = carrito.cupon
            car.cuponDesc = carrito.cuponDesc
            car.subTotal = carrito.subTotal
            car.total = carrito.total


            if (carrito.productos.isNotEmpty()) {
                val list = RealmList<ProductoCarrito>()
                carrito.productos.forEach { p ->
                    val pCarrito = realmBg.createObject(ProductoCarrito::class.java, p.id)
                    pCarrito.count = p.count
                    /*pCarrito.precio = p.precio
                    pCarrito.subtotal = p.subtotal
                    pCarrito.descuento = p.descuento*/
                    list.add(pCarrito)
                }
                car.productos = list
            }
        }, {
            hideProgress()
            backResultOk()
        }, { t ->
            t.printStackTrace()
            hideProgress()
            OToast.makeText(this@ActivityAddCarrito, getString(R.string.error_guardar_carrito)).show()
        })
    }

    private fun addOrUpdateProdToCarrito(carrito: Carrito, id: Int) {
        val idProd = prod!!.id
        Realm.getDefaultInstance().executeTransactionAsync({ realmBg ->
            val car = realmBg.where(Carrito::class.java).equalTo("pedidoID", id).findFirst()

            car!!.cupon = carrito.cupon
            car.cuponDesc = carrito.cuponDesc
            car.subTotal = carrito.subTotal
            car.total = carrito.total

            var pCarrito = realmBg.where(ProductoCarrito::class.java).equalTo("id", idProd).findFirst()
            if (pCarrito == null) {
                pCarrito = realmBg.createObject(ProductoCarrito::class.java, idProd)
                pCarrito!!.count = "$numProdus"
                car.productos.add(pCarrito)
            } else {
                val aux = numProdus
                println("$aux")
                if (aux > MAX_NUM_PRODS) {
                    OToast(this, "Se ha excedido la cantidad de productos").show()
                    return@executeTransactionAsync
                }
                pCarrito.count = aux.toString()
                realmBg.copyToRealmOrUpdate(pCarrito)
            }

        }, {
            hideProgress()
            backResultOk()
        }, { t ->
            t.printStackTrace()
            hideProgress()
            OToast.makeText(this@ActivityAddCarrito, getString(R.string.error_guardar_carrito)).show()
        })
    }

    private fun UpdateProdToCarrito(carrito: Carrito, id: Int) {
        val idProd = prod!!.id
        Realm.getDefaultInstance().executeTransactionAsync({ realmBg ->
            val car = realmBg.where(Carrito::class.java).equalTo("pedidoID", id).findFirst()

            car!!.cupon = carrito.cupon
            car.cuponDesc = carrito.cuponDesc
            car.subTotal = carrito.subTotal
            car.total = carrito.total

            var pCarrito = realmBg.where(ProductoCarrito::class.java).equalTo("id", idProd).findFirst()
            if (pCarrito == null) {
                pCarrito = realmBg.createObject(ProductoCarrito::class.java, idProd)
                pCarrito!!.count = "$numProdus"
                car.productos.add(pCarrito)
            } else {
                //val aux = pCarrito.count.toInt() + numProdus
                val aux = numProdus
                pCarrito.count = aux.toString()
                realmBg.copyToRealmOrUpdate(pCarrito)
            }

        }, {
            hideProgress()
            backResultOk()
        }, { t ->
            t.printStackTrace()
            hideProgress()
            OToast.makeText(this@ActivityAddCarrito, getString(R.string.error_guardar_carrito)).show()
        })
    }

    private fun showProgress() {
        progressPlane.visibility = View.VISIBLE
        layInfo.visibility = View.GONE
        progressPlane.playAnimation()

        btn_menos.isEnabled = false
        btn_mas.isEnabled = false
        btn_add.isEnabled = false
        btn_menos.setColorFilter(Color.GRAY)
        btn_mas.setColorFilter(Color.GRAY)
        btn_add.text = getString(R.string.guardando)
        txt_count.setTextColor(Color.GRAY)
        title_total.setTextColor(Color.GRAY)
        txt_total.setTextColor(Color.GRAY)
    }

    private fun hideProgress() {
        progressPlane.visibility = View.GONE
        layInfo.visibility = View.VISIBLE
        progressPlane.cancelAnimation()

        btn_menos.isEnabled = true
        btn_mas.isEnabled = true
        btn_add.isEnabled = true
        btn_menos.clearColorFilter()//colorFilter = null
        btn_mas.clearColorFilter() //colorFilter = null
        btn_add.text = getString(R.string.agregar)
        txt_count.setTextColor(Color.BLACK)
        title_total.setTextColor(Color.BLACK)
        txt_total.setTextColor(Color.BLACK)
    }

    private fun backResultOk() {

        val config = AppConfig(this)

        val builder = android.support.v7.app.AlertDialog.Builder(this)
        if (config.language == "ES") {

            builder.setTitle("¡Se agregó este producto a tu carrito!")
            builder.setNegativeButton("Seguir comprando") {dialog, which ->
                dialog.dismiss();
            }
            builder.setPositiveButton("Ir a mi carrito") { dialog, which ->
                val i = intent
                setResult(Activity.RESULT_OK, i)
                this.finish()
            }
        } else {

            builder.setTitle("This product was added to your cart!")
            builder.setNegativeButton("Continue shopping") { dialog, which ->
                dialog.dismiss()
            }
            builder.setPositiveButton("Go to my cart") { dialog, which ->
                val i = intent
                setResult(Activity.RESULT_OK, i)
                this.finish()
            }
        }

        val dialog: android.support.v7.app.AlertDialog = builder.create()

        dialog.show()



    }

    override fun onBackPressed() {
        if (progressPlane.visibility == View.GONE)
            super.onBackPressed()
    }

    private fun loadStock() {

        /*
        dialogLo.show()
        val post = object : StringRequest(com.android.volley.Request.Method.POST, ApiClient.BASE_URL + "getProdsById", { response ->
            if (response.isNotEmpty()) {
                dialogLo.dismiss()
                println("ActivityAddCarrito.loadStock --> $response")
                val msg = response
                try {
                    val json = JSONObject(response)
                    if (json.has("status") && json.getInt("status") == 200) {

                        //val producto = Product.create()

                        saveStockById(producto)
                    }
                } catch (e: java.lang.Exception) {
                    e.printStackTrace()
                    OToast.makeText(this@ActivityAddCarrito, "Error conexion ;) Guiño guiño").show()
                }
            } else {
                dialogLo.dismiss()
                OToast.makeText(this@ActivityAddCarrito, "Error al cargar Stock").show()
            }
        }, {
            it.printStackTrace()
            dialogLo.dismiss()
            OToast.makeText(this@ActivityAddCarrito, "Error al cargar Stock").show()
        }) {
            override fun getParams(): MutableMap<String, String> {
                val map = HashMap<String, String?>()
                map["lang"] = AppConfig(applicationContext).language
                map["product_id"] = prod?.id
            }
        }

        post.retryPolicy = DefaultRetryPolicy( 20 * 1000, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT)
        VolleyS(this).getRequest().add(post)
        */


        dialogLo.show()
        println("ActivityAddCarrito.Stock --> ${prod?.id}")
        println("ActivityAddCarrito.Stock --> ${prod?.nameEs}")
        println("ActivityAddCarrito.Stock --> ${prod?.nameEng}")
        println("ActivityAddCarrito.Stock --> ${prod?.detalleEs}")
        println("ActivityAddCarrito.Stock --> ${prod?.detalleEng}")
        println("ActivityAddCarrito.Stock --> ${prod?.categoriaID}")
        println("ActivityAddCarrito.Stock --> ${prod?.precioPublico}")
        println("ActivityAddCarrito.Stock --> ${prod?.precioDistribuidores}")
        println("ActivityAddCarrito.Stock --> ${prod?.descuento}")
        println("ActivityAddCarrito.Stock --> ${prod?.imagen}")
        println("ActivityAddCarrito.Stock --> ${prod?.stock}")

        val map = HashMap<String, String?>()
        map.put("lang", AppConfig(this).language)
        map.put("product_id", prod?.id)
        map.put("email", user!!.email)

        call = ApiInterface.create(this).getProdsById(map)
        call!!.enqueue(object : retrofit2.Callback<Model.ResponseProductsById> {

            override fun onResponse(call: Call<Model.ResponseProductsById>, response: Response<Model.ResponseProductsById>) {
                try {
                    println("Product by Id response --> ${response.body()}")
                } catch (e: Exception) {
                    e.printStackTrace()
                }
                if (response.isSuccessful && response.body() != null && response.body()!!.status == 200) {
                    saveStockById(response.body()!!.producto, prod?.id)
                } else {
                    val msg: String = "Response null or fail"
                    OToast.makeText(this@ActivityAddCarrito, msg).show()
                }
            }

            override fun onFailure(call: Call<Model.ResponseProductsById>, t: Throwable) {
                dialogLo.dismiss()
                println("ActivityAddCarrito --> Error al cargar Stock(Fail service stock)")
                t.printStackTrace()
                OToast.makeText(this@ActivityAddCarrito, "Error de conexion ;) guiño guiño").show()
            }
        })
    }

    private fun saveStockById(producto: Product, id: String?) {
        Realm.getDefaultInstance().executeTransactionAsync ({ realmBg ->

            val p = realmBg.where(Product::class.java).equalTo("id", id).findFirst()

            p!!.categoriaID = producto.categoriaID
            p.descuento = producto.descuento
            p.precioDistribuidores = producto.precioDistribuidores
            p.precioPublico = producto.precioPublico
            p.detalleEs = producto.detalleEs
            p.detalleEng = producto.detalleEng
            p.imagen = producto.imagen
            p.stock = producto.stock

            MAX_NUM_PRODS = producto.stock

        }, {
            dialogLo.dismiss()
            println("ActivityAddCarrito.saveStockById --> save successful")
            showData()
        }, { t ->
            dialogLo.dismiss()
            t.printStackTrace()
        })
    }

}

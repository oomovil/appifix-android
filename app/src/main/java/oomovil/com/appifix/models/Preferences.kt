package oomovil.com.appifix.models

import com.google.gson.annotations.SerializedName
import io.realm.RealmObject
import io.realm.annotations.PrimaryKey

/**
 * Created by Alan on 18/10/2017.
 */
open class Preferences() : RealmObject() {

    @SerializedName("id")
    @PrimaryKey
    var id: String? = null
    @SerializedName("preferencia")
    var name: String? = null
    @SerializedName("pref_eng")
    var nameEn: String? = null
    @SerializedName("imagen")
    var urlFoto: String? = null
    var isSelected: Boolean = false

    constructor(id: String?, name: String?, nameEng: String?, urlFoto: String?, isSelected: Boolean) : this() {
        this.id = id
        this.name = name
        this.nameEn = nameEng
        this.urlFoto = urlFoto
        this.isSelected = isSelected
    }

    override fun toString(): String = "Preferences(id=$id, name=$name, nameEn=$nameEn, urlFoto=$urlFoto, isSelected=$isSelected)"

}
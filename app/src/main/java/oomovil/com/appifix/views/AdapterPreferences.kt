package oomovil.com.appifix.views

import android.animation.Animator
import android.content.Context
import android.os.Handler
import android.support.v7.widget.RecyclerView
import android.view.Gravity
import android.view.View
import android.view.ViewGroup
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.widget.FrameLayout
import android.widget.ImageView
import com.airbnb.lottie.animation.keyframe.BaseKeyframeAnimation
import com.transitionseverywhere.ArcMotion
import com.transitionseverywhere.ChangeBounds
import com.transitionseverywhere.TransitionManager
import oomovil.com.appifix.R
import oomovil.com.appifix.models.Preferences
import kotlinx.android.synthetic.main.item_preferences.view.*
import oomovil.com.appifix.general.inflate
import oomovil.com.appifix.general.loadUrl
import oomovil.com.appifix.preferences.OnPreferencesListener
import android.view.animation.DecelerateInterpolator
import android.view.animation.AlphaAnimation
import oomovil.com.appifix.general.AppConfig


/**
 * Created by Alan on 18/10/2017.
 */
class AdapterPreferences(var items: ArrayList<Preferences>?, val context: Context, val onClick: OnPreferencesListener) : RecyclerView.Adapter<AdapterPreferences.ViewHolder>() {

    override fun onBindViewHolder(p0: ViewHolder, p1: Int) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    private var posAnim: Int = -1
    private val config = AppConfig(context)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder = ViewHolder(parent.inflate(R.layout.item_preferences, false))

    override fun onBindViewHolder(holder: ViewHolder, position: Int, payloads: MutableList<Any>) {
        val p = items!![position]
        holder.itemView.title_prefe.text = if (config.language == "ES") p.name else p.nameEn
        p.urlFoto?.let { holder.itemView.img_prefe.loadUrl(it, R.drawable.img_no_disponible, null) }

        if (p.isSelected) {
            holder.itemView.checked.visibility = View.VISIBLE
            holder.itemView.viewAlpha.visibility = View.VISIBLE
            holder.itemView.animation_view.progress = 0f
        } else {
            holder.itemView.checked.visibility = View.GONE
            holder.itemView.viewAlpha.visibility = View.GONE
            holder.itemView.animation_view.progress = 0f
        }

        if (holder.adapterPosition > posAnim) {
            println("AdapterPreferences.onBindViewHolder --> anim position: ${holder.adapterPosition}")
            posAnim = holder.adapterPosition
            val fadeIn = AlphaAnimation(0f, 1f)
            fadeIn.interpolator = DecelerateInterpolator()
            fadeIn.duration = 1000
            holder.itemView.cardView.startAnimation(fadeIn)
        }

        holder.itemView.cardView.setOnClickListener {
            holder.itemView.cardView.clearAnimation()
            holder.itemView.cardView.startAnimation(AnimationUtils.loadAnimation(context, R.anim.card_animation))
            if (!p.isSelected) {
                moveToCenter(holder.itemView.checked)
                holder.itemView.animation_view.cancelAnimation()
                if (holder.itemView.checked.animation != null) holder.itemView.checked.animation.cancel()
                holder.itemView.animation_view.visibility = View.VISIBLE
                holder.itemView.animation_view.addAnimatorListener(object : Animator.AnimatorListener, BaseKeyframeAnimation.AnimationListener {
                    override fun onValueChanged() {}

                    override fun onAnimationCancel(p0: Animator?) {
                        holder.itemView.animation_view.visibility = View.GONE
                        holder.itemView.viewAlpha.visibility = View.VISIBLE
                        fadeView(holder.itemView.checked, true, holder.itemView.container)
                    }

                    override fun onAnimationRepeat(p0: Animator?) {}

                    override fun onAnimationEnd(p0: Animator?) {
                        holder.itemView.animation_view.visibility = View.GONE
                        holder.itemView.viewAlpha.visibility = View.VISIBLE
                        fadeView(holder.itemView.checked, true, holder.itemView.container)
                    }

                    override fun onAnimationStart(p0: Animator?) {}
                })

                holder.itemView.animation_view.playAnimation()
                p.isSelected = true
            } else {
                //holder.itemView.checked.visibility = View.GONE
                holder.itemView.animation_view.visibility = View.GONE
                holder.itemView.animation_view.progress = 0f
                p.isSelected = false
                holder.itemView.viewAlpha.visibility = View.GONE
                fadeView(holder.itemView.checked, false, holder.itemView.container)
            }
            onClick.onClick(holder.adapterPosition)
        }
    }

    private fun moveToCenter(view: View) {
        val params = view.layoutParams as FrameLayout.LayoutParams
        params.gravity = Gravity.CENTER
        view.layoutParams = params
        view.visibility = View.INVISIBLE
    }

    fun fadeView(v: ImageView, show: Boolean, view: FrameLayout) {
        v.clearAnimation()
        view.clearAnimation()
        if (!show) {
            val animator: Animation? = AnimationUtils.loadAnimation(context, R.anim.fade_out)
            animator!!.setAnimationListener(object : Animation.AnimationListener {
                override fun onAnimationRepeat(p0: Animation?) {}

                override fun onAnimationEnd(p0: Animation?) {
                    v.visibility = View.GONE
                }

                override fun onAnimationStart(p0: Animation?) {
                    Handler().postDelayed({
                        v.visibility = View.GONE
                    }, animator.duration)
                }

            })
            v.startAnimation(animator)
        } else {
            v.visibility = View.VISIBLE
            TransitionManager.beginDelayedTransition(view, ChangeBounds().setPathMotion(ArcMotion()).setDuration(300))
            val params = v.layoutParams as FrameLayout.LayoutParams
            params.gravity = Gravity.TOP or Gravity.END
            v.layoutParams = params
        }

    }

    override fun getItemCount() = items!!.size

    class ViewHolder(val view: View) : RecyclerView.ViewHolder(view)
}
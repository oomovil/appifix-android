package oomovil.com.appifix.principal

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import io.realm.Realm
import kotlinx.android.synthetic.main.fragment_productos.*
import oomovil.com.appifix.R
import oomovil.com.appifix.general.AppConfig
import oomovil.com.appifix.general.ImageUtils
import oomovil.com.appifix.general.loadUrl
import oomovil.com.appifix.models.Model
import oomovil.com.appifix.models.Product
import oomovil.com.appifix.models.UserModel
import oomovil.com.appifix.retrofit.ApiInterface
import oomovil.com.appifix.views.AdapterProducto
import oomovil.com.appifix.views.OnRecyclerItemClick
import retrofit2.Call
import retrofit2.Response

/**
 * Created by Alan on 24/10/2017.
 */
class FragmentProductos : Fragment(), OnRecyclerItemClick {

    private var id: String? = null
    private val FRAGMENT_STATE = "FragmentProductos"

    private var listenerFragment: OnFragmentListener? = null

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        try {
            listenerFragment = context as OnFragmentListener
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?):
            View? = inflater!!.inflate(R.layout.fragment_productos, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        btn_reload_prods.setOnClickListener {
            loadProductos()
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            val transitionNameTxt = arguments!!.getString("transitionNameTxt")
            txt_cate.transitionName = transitionNameTxt
        }

        val title = arguments!!.getString("titulo")
        id = arguments!!.getString("id")
        println("FragmentProductos.onCreate --> $title --> $id")
        txt_cate.text = title

        recyclerProdus.layoutManager = GridLayoutManager(context, 2, GridLayoutManager.VERTICAL, false)
        refreshProds.setOnRefreshListener {
            loadProductos()
        }
        if (!Principal.bundle.containsKey(FRAGMENT_STATE)) {
            loadProductos()
            Principal.bundle.putBoolean(FRAGMENT_STATE, true)
            Principal.bundle.putString("productosId", id!!)
        } else {
            val lastId = Principal.bundle.getString("productosId", "")
            if (!lastId.isEmpty() && lastId != id) {
                Principal.bundle.putString("productosId", id!!)
                loadProductos()
            } else {
                showProducts(false)
            }
        }
    }

    private fun loadProductos() {
        progressProds.visibility = View.VISIBLE
        layErrorProds.visibility = View.GONE
        refreshProds.visibility = View.GONE

        val map = HashMap<String, String>()
        map.put("cat_id", id!!)
        map.put("lang", "ES") // porque siempre manda las dos versiones
        map.put("email", Realm.getDefaultInstance().where(UserModel::class.java).findFirst()!!.email)
        val call = ApiInterface.create(context!!).getProdsFromCat(map)
        call.enqueue(object : retrofit2.Callback<Model.ResponseProductFromCategory> {
            override fun onResponse(call: Call<Model.ResponseProductFromCategory>?, response: Response<Model.ResponseProductFromCategory>?) {
                if (activity != null && !activity!!.isFinishing && context != null) {
                    if (response != null && response.isSuccessful && response.body() != null && response.body()!!.status == 200 && response.body()!!.productos.isNotEmpty()) {
                        saveProductsFromCat(response.body()!!.productos)
                    } else {
                        showProducts(false)
                    }
                }
            }

            override fun onFailure(call: Call<Model.ResponseProductFromCategory>?, t: Throwable?) {
                if (activity != null && !activity!!.isFinishing && context != null) {
                    showProducts(true)
                }
            }
        })
    }

    private fun saveProductsFromCat(productos: List<Product>) {
        Realm.getDefaultInstance().executeTransactionAsync({ realmBg ->
            realmBg.where(Product::class.java).equalTo("categoriaID", id).findAll().deleteAllFromRealm()
            productos.forEach { pro ->
                val p = realmBg.createObject(Product::class.java)
                p.id = pro.id
                p.nameEs = pro.nameEs
                p.nameEng = pro.nameEng
                p.detalleEs = pro.detalleEs
                p.detalleEng = pro.detalleEng
                p.imagen = pro.imagen
                p.precioPublico = pro.precioPublico
                p.precioDistribuidores = pro.precioDistribuidores
                p.categoriaID = pro.categoriaID
                p.descuento = pro.descuento
                p.stock = pro.stock
            }
        }, {
            println("FragmentProductos.saveProductsFromCat --> save successful")
            showProducts(false)
        }, { t ->
            t.printStackTrace()
            showProducts(false)
        })
    }

    private fun showProducts(isErroServer: Boolean) {
        val pros = Realm.getDefaultInstance().where(Product::class.java).equalTo("categoriaID", id).findAll()
        when {
            pros.isNotEmpty() -> {
                refreshProds.isRefreshing = false
                progressProds.visibility = View.GONE
                refreshProds.visibility = View.VISIBLE
                layErrorProds.visibility = View.GONE

                recyclerProdus.adapter = AdapterProducto(pros, context!!, this)
            }
            isErroServer -> showError()
            else -> showEmpty()
        }
    }

    private fun showError() {
        refreshProds.isRefreshing = false
        progressProds.visibility = View.GONE
        refreshProds.visibility = View.GONE
        layErrorProds.visibility = View.VISIBLE
        txt_error.text = getString(R.string.error_servidor)
    }

    private fun showEmpty() {
        refreshProds.isRefreshing = false
        progressProds.visibility = View.GONE
        refreshProds.visibility = View.GONE
        layErrorProds.visibility = View.VISIBLE
        txt_error.text = getString(R.string.ocurrii_un_error_al_cargar_los_productos)
    }

    override fun onClick(fragment: Int, id: String, viewList: List<Pair<View, String>>, data: List<Pair<String, String?>>) {
        if (listenerFragment != null)
            listenerFragment!!.changeViewTransition(fragment, id, viewList, data)
    }

    override fun onClick(fragment: Int, id: String, idCategoria: String, nombreCat: String) {
        val intent = Intent(context, ActivityAddCarrito::class.java)
        intent.putExtra("id", id)
        intent.putExtra("idCategoria", idCategoria)
        intent.flags = Intent.FLAG_ACTIVITY_NO_ANIMATION
        intent.putExtra(ActivityAddCarrito.INTENT_ACTION, ActivityAddCarrito.ADD)
        startActivityForResult(intent, 150)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 150 && resultCode == Activity.RESULT_OK) {
            listenerFragment?.changeView(OnFragmentListener.FRAGMENT_CARRITO, "-1")
        }
    }

}
package oomovil.com.appifix.general

import android.content.Context
import android.content.SharedPreferences
import android.graphics.Bitmap
import android.util.Base64
import java.io.ByteArrayOutputStream
import android.graphics.BitmapFactory
import java.util.*


/**
 * Created by Alan on 10/11/2017.
 */
class AppConfig(context: Context) {

    private val PREFS_FILENAME = "oomovil.com.appifix.prefs"
    private val LANGUAGE = "language"
    private val INTRO_SHOWN = "intro_shown"
    private val SESSION_ACTIVE = "session_active"
    private val ITEM_TARJET_SHOWED = "tarjeta_showed"
    private val ITEM_LOCATION_SHOWED = "location_showed"

    private val prefs: SharedPreferences = context.getSharedPreferences(PREFS_FILENAME, 0)

    private fun setBooleanPrefs(key: String, value: Boolean) {
        prefs.edit().putBoolean(key, value).apply()
    }

    private fun getBooleanPrefs(key: String): Boolean = prefs.getBoolean(key, false)

    private fun setStringPrefs(key: String, value: String) {
        prefs.edit().putString(key, value).apply()
    }

    private fun getStringPrefs(key: String): String = prefs.getString(key, "")

    fun getPreferences(): SharedPreferences = prefs

    var userSession: Boolean
        get() = getBooleanPrefs(SESSION_ACTIVE)
        set(value) = setBooleanPrefs(SESSION_ACTIVE, value)

    var introShown: Boolean
        get() = getBooleanPrefs(INTRO_SHOWN)
        set(value) = setBooleanPrefs(INTRO_SHOWN, value)

    var tarjetaShown: Boolean
        get() = getBooleanPrefs(ITEM_TARJET_SHOWED)
        set(value) = setBooleanPrefs(ITEM_TARJET_SHOWED, false)

    var locationShown: Boolean
        get() = getBooleanPrefs(ITEM_LOCATION_SHOWED)
        set(value) = setBooleanPrefs(ITEM_LOCATION_SHOWED, value)

    var language: String
        get() = Locale.getDefault().language.toUpperCase()
        set(value) = setStringPrefs(LANGUAGE, value.toUpperCase())

}
package oomovil.com.appifix.registro

import android.app.Activity
import android.app.AlertDialog
import android.app.ProgressDialog
import android.content.Context
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.Window
import dmax.dialog.SpotsDialog
import kotlinx.android.synthetic.main.activity_pop_recuperar_pass.*
import oomovil.com.appifix.R
import oomovil.com.appifix.general.*
import oomovil.com.appifix.models.Model
import oomovil.com.appifix.oomoviews.OToast
import oomovil.com.appifix.retrofit.ApiInterface
import retrofit2.Call
import retrofit2.Response

class PopRecuperarPass : Activity() {

    private val dialogLo: AlertDialog by lazy {
        this.dialogView()
    }

    override fun attachBaseContext(newBase: Context) {
        super.attachBaseContext(MyContextWrapper.wrap(newBase, AppConfig(newBase).language))
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        setContentView(R.layout.activity_pop_recuperar_pass)
        window.setBackgroundDrawable(ColorDrawable(android.graphics.Color.TRANSPARENT))
        window.attributes.windowAnimations = R.style.dialog_animation

        btnPopAccion.setOnClickListener {
            if (isEmpty(caja_correo)) {
                OToast.makeText(this, getString(R.string.email_empty)).show()
            } else {
                dialogLo.show()
                val map = HashMap<String, String>()
                map.put("email", getText(caja_correo)!!)
                map.put("lang", AppConfig(this).language)
                println("PopRecuperarPass.onCreate --> $map")
                ApiInterface.create(this).recoverPassword(map).enqueue(object : retrofit2.Callback<Model.Response> {
                    override fun onFailure(call: Call<Model.Response>?, t: Throwable?) {
                        dialogLo.dismiss()
                        OToast.makeText(this@PopRecuperarPass, getString(R.string.error_conexion)).show()
                    }

                    override fun onResponse(call: Call<Model.Response>?, response: Response<Model.Response>?) {
                        dialogLo.dismiss()
                        try {
                            println("PopRecuperarPass.onResponse --> ${response!!.body()}")
                            println("PopRecuperarPass.onResponse --> ${response.message()}")
                        } catch (e: Exception) {
                            e.printStackTrace()
                        }
                        if (response != null && response.isSuccessful && response.body() != null && response.body()!!.status == 200) {
                            OToast.makeText(this@PopRecuperarPass, getString(R.string.recover_pass_successful)).show()
                            this@PopRecuperarPass.finish()
                        } else {
                            OToast.makeText(this@PopRecuperarPass, response?.body()!!.message).show()
                        }
                    }
                })
            }
        }
    }

    override fun onStart() {
        super.onStart()
        KeyboardManager.init(this)
    }
}

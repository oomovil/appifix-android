package oomovil.com.appifix.views

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import io.realm.RealmResults
import kotlinx.android.synthetic.main.item_notificaciones.view.*
import oomovil.com.appifix.R
import oomovil.com.appifix.general.AppConfig
import oomovil.com.appifix.general.inflate
import oomovil.com.appifix.general.loadUrl
import oomovil.com.appifix.models.Notificacion
import oomovil.com.appifix.principal.FragmentNotificaciones

/**
 * Created by Alan on 04/12/2017.
 */
class AdapterNotis(private var items: RealmResults<Notificacion>?, val context: Context, val click: FragmentNotificaciones.OnClickNoti) : RecyclerView.Adapter<AdapterNotis.ViewHolder>(){

    override fun onBindViewHolder(p0: ViewHolder, p1: Int) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    private val config = AppConfig(context)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder = ViewHolder(parent.inflate(R.layout.item_notificaciones, false))

    override fun onBindViewHolder(holder: ViewHolder, position: Int, payloads: MutableList<Any>) {
        val n = items!![position]

        holder.itemView.img_noti.loadUrl(n!!.images[0]!!.url, R.drawable.img_no_disponible, null)
        holder.itemView.txt_title_noti.text = if(config.language == "ES") n.titulo else n.title
        holder.itemView.txt_subtitle_noti.text = if(config.language == "ES") n.mensaje else n.message

        holder.itemView.cardNoti.setOnClickListener {
            click.onClick("${items!![holder.adapterPosition]!!.id}")
        }
    }

    override fun getItemCount(): Int = items!!.size

    class ViewHolder(val view: View) : RecyclerView.ViewHolder(view)
}
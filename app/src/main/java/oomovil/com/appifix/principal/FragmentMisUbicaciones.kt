package oomovil.com.appifix.principal

import android.app.AlertDialog
import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import dmax.dialog.SpotsDialog
import io.realm.Realm
import io.realm.RealmResults
import io.realm.Sort
import kotlinx.android.synthetic.main.fragment_mis_ubicaciones.*
import oomovil.com.appifix.R
import oomovil.com.appifix.general.AppConfig
import oomovil.com.appifix.general.dialogView
import oomovil.com.appifix.models.Locations
import oomovil.com.appifix.models.Model
import oomovil.com.appifix.models.UserModel
import oomovil.com.appifix.oomoviews.OToast
import oomovil.com.appifix.retrofit.ApiInterface
import oomovil.com.appifix.views.AdapterLocations
import retrofit2.Call
import retrofit2.Response

/**
 * Created by Alan on 26/10/2017.
 */
class FragmentMisUbicaciones : Fragment() {

    private var listLocas: RealmResults<Locations>? = null
    private val dialogLo: AlertDialog by lazy {
        this.activity!!.dialogView()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?):
            View? = inflater!!.inflate(R.layout.fragment_mis_ubicaciones, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        recyclerLocations.layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
        recyclerLocations.itemAnimator = DefaultItemAnimator()
        btn_add_location.setOnClickListener {
            addLocation()
        }

    }

    override fun onResume() {
        super.onResume()
        showListLocations()
    }

    private fun showListLocations() {
        listLocas = Realm.getDefaultInstance().where(Locations::class.java).findAll().sort("principal", Sort.DESCENDING)
        if (listLocas != null && listLocas!!.isNotEmpty()) {
            recyclerLocations.visibility = View.VISIBLE
            laySinUbis.visibility = View.GONE
            recyclerLocations.adapter = AdapterLocations(listLocas, context!!, object : AdapterLocations.OnClick {
                override fun onCLick(id: String, delete: Boolean) {
                    //pregunta para saber si se va a eliminar o va a ser la nueva favorita
                    if (delete) {
                        askIfDelete(id)
                    } else {
                        updateLocation(id)
                    }
                }
            })
        } else {
            recyclerLocations.visibility = View.GONE
            laySinUbis.visibility = View.VISIBLE
        }
    }

    private fun updateLocation(id: String) {
        dialogLo.show()
        val ubi = Realm.getDefaultInstance().where(Locations::class.java).equalTo("id", id).findFirst()
        val map = HashMap<String, String>()
        map.put("email", Realm.getDefaultInstance().where(UserModel::class.java).findFirst()!!.email)
        map.put("lang", AppConfig(context!!).language)
        map.put("ubicacion_id", id)
        map.put("dir_principal", if (ubi!!.principal == "SI") "NO" else "SI")
        println("FragmentMisUbicaciones.updateLocation --> $map")
        ApiInterface.create(context!!).updateLocation(map).enqueue(object : retrofit2.Callback<Model.Response> {
            override fun onResponse(call: Call<Model.Response>?, response: Response<Model.Response>?) {
                try {
                    println("FragmentMisUbicaciones.onResponse --> ${response!!.body()}")
                } catch (e: Exception) {
                    e.printStackTrace()
                }
                if (response != null && response.isSuccessful && response.body() != null && response.body()!!.status == 200) {
                    setNewFavorite(if (ubi.principal == "SI") "-1" else id)
                } else {
                    dialogLo.dismiss()
                    OToast.makeText(context!!, getString(R.string.error_actualizar_ubicacion)).show()
                }
            }

            override fun onFailure(call: Call<Model.Response>?, t: Throwable?) {
                dialogLo.dismiss()
                OToast.makeText(context!!, getString(R.string.error_conexion)).show()
            }

        })
    }

    private fun setNewFavorite(id: String) {
        val realm = Realm.getDefaultInstance()
        realm.beginTransaction()
        listLocas!!.forEach { l ->
            if (l.id == id)
                l.principal = "SI"
            else
                l.principal = "NO"
            realm.copyToRealmOrUpdate(l)
        }
        realm.commitTransaction()
        dialogLo.dismiss()
        recyclerLocations.adapter!!.notifyDataSetChanged()
    }

    private fun askIfDelete(id: String) {
        val builder = AlertDialog.Builder(context)
                .setIcon(R.mipmap.alerta_pop)
                .setTitle(R.string.pop_eliminar_titulo)
                .setMessage(R.string.pop_eliminar_pregunta)
                .setPositiveButton(getString(R.string.eliminar), { dialog, x ->
                    println("FragmentMisUbicaciones.askIfDelete --> $x")
                    dialog.dismiss()
                    deleteLocation(id)
                })
                .setNegativeButton(R.string.cancelar, { dialog, x ->
                    println("FragmentMisUbicaciones.askIfDelete --> $x")
                    dialog.dismiss()
                })
        val alert = builder.create()
        alert!!.show()
    }

    private fun deleteLocation(id: String) {
        dialogLo.show()
        val map = HashMap<String, String>()
        map.put("lang", AppConfig(context!!).language)
        map.put("email", Realm.getDefaultInstance().where(UserModel::class.java).findFirst()!!.email)
        map.put("ubicacion_id", id)
        ApiInterface.create(context!!).deleteLocation(map).enqueue(object : retrofit2.Callback<Model.Response> {
            override fun onFailure(call: Call<Model.Response>?, t: Throwable?) {
                dialogLo.dismiss()
                OToast.makeText(context!!, R.string.error_conexion).show()
            }

            override fun onResponse(call: Call<Model.Response>?, response: Response<Model.Response>?) {
                try {
                    println("FragmentMisUbicaciones.onResponse --> ${response!!.body()!!.status} --> ${response.body()!!.message}")
                } catch (e: Exception) {
                    e.printStackTrace()
                }
                if (response != null && response.isSuccessful && response.body() != null && response.body()!!.status == 200) {
                    Realm.getDefaultInstance().executeTransactionAsync({ realmBG ->
                        val loca = realmBG.where(Locations::class.java).equalTo("id", id).findFirst()
                        loca?.deleteFromRealm()
                    }, {
                        dialogLo.dismiss()
                        showListLocations()
                    }, { t ->
                        dialogLo.dismiss()
                        t.printStackTrace()
                    })
                } else {
                    dialogLo.dismiss()
                    OToast.makeText(context!!, getString(R.string.error_eliminar_ubicacion)).show()
                }
            }

        })
    }

    private fun addLocation() {
        val intent = Intent(context, ActivityAddLocation::class.java)
        intent.putExtra("isNewLocation", true)
        startActivity(intent)
    }

}
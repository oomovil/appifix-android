package oomovil.com.appifix.models

import com.google.gson.annotations.SerializedName
import io.realm.RealmObject
import io.realm.annotations.PrimaryKey

/**
 * Created by Alan on 23/10/2017.
 */
open class Categoria : RealmObject() {
    @PrimaryKey
    @SerializedName("id")
    var id: String? = null
    @SerializedName("categoria")
    var name: String? = null
    @SerializedName("cat_eng")
    var nameEn: String? = null
    @SerializedName("imagen")
    var urlFoto: String? = null
    @SerializedName("num_prods")
    var cantProds: String? = null
    @SerializedName("padre")
    var padre: String? = null
}
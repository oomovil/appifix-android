package oomovil.com.appifix.general

import android.animation.Animator
import android.app.Activity
import android.app.AlertDialog
import android.content.Context
import android.graphics.*
import android.os.Build
import android.os.Handler
import android.support.annotation.LayoutRes
import android.support.v4.view.ViewCompat
import android.text.Html
import android.text.SpannableString
import android.text.TextUtils
import android.text.style.UnderlineSpan
import android.util.DisplayMetrics
import android.view.LayoutInflater
import android.view.View
import android.view.ViewAnimationUtils
import android.view.ViewGroup
import android.view.animation.Animation
import android.view.animation.LinearInterpolator
import android.view.animation.ScaleAnimation
import android.widget.Button
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import com.squareup.picasso.Picasso
import com.squareup.picasso.Transformation
import dmax.dialog.SpotsDialog
import retrofit2.Response
import java.text.DecimalFormat
import java.text.DecimalFormatSymbols
import java.text.SimpleDateFormat
import java.util.*
import java.util.regex.Pattern

/**
 * Created by Alan on 23/10/2017.
 */

private val PATTERN_EMAIL = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@" + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$"
private val PATTERN_TEXT = "^[a-zA-Z]+(\\s*[a-zA-Z]*)*[a-zA-Z]+"

fun fixURL(url: String): String = url.replace(" ", "%20").replace("<", "").replace(">", "").replace("Ñ", "N").replace("ñ", "n")

fun getTextFromHtml(text: String): String {
    return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
        Html.fromHtml(text, Html.FROM_HTML_MODE_LEGACY).toString()
    } else {
        Html.fromHtml(text).toString()
    }
}

/**
 * Validate given email with regular expression.
 *
 * @param email email for validation
 * @return true valid email, otherwise false
 */
fun validateEmail(email: String): Boolean {

    // Compiles the given regular expression into a pattern.
    val pattern = Pattern.compile(PATTERN_EMAIL)

    // Match the given input against this pattern
    //val matcher = pattern.matcher(email)

    return true//matcher.matches()
}

fun validOnlyText(txt: String): Boolean {
    // Compiles the given regular expression into a pattern.
    val pattern = Pattern.compile(PATTERN_TEXT)

    // Match the given input against this pattern
    val matcher = pattern.matcher(txt)

    return matcher.matches()
}

/***
 * @return - fecha actual: dd/MM/yyyy HH:mm:ss
 */
fun getCurrentDate(format: String): String {
    val hourdateFormat = SimpleDateFormat(format, Locale.getDefault())
    return hourdateFormat.format(Date())
}

fun getMes(num: String): String? {
    val n = Integer.parseInt(num)
    when (n) {
        1 -> return "Ene"
        2 -> return "Feb"
        3 -> return "Mar"
        4 -> return "Abr"
        5 -> return "May"
        6 -> return "Jun"
        7 -> return "Jul"
        8 -> return "Ago"
        9 -> return "Sep"
        10 -> return "Oct"
        11 -> return "Nov"
        12 -> return "Dic"
    }
    return null
}

fun View.setUnderLine() {
    if (this is EditText || this is TextView) {
        val text = getText(this)
        val content = SpannableString(text)
        content.setSpan(UnderlineSpan(), 0, content.length, 0)
        if (this is EditText) {
            this.setText(content)
            this.paintFlags = this.paintFlags or Paint.UNDERLINE_TEXT_FLAG
        } else if (this is TextView) {
            this.text = content
            this.paintFlags = this.paintFlags or Paint.UNDERLINE_TEXT_FLAG
        }
    }
}

fun getText(view: View): String? = (view as? EditText)?.text?.toString() ?: (view as? TextView)?.text?.toString()

fun isEmpty(view: View): Boolean = TextUtils.isEmpty(getText(view))

fun equals(view1: View, view2: View): Boolean =
        isEmpty(view1) && isEmpty(view2) || getText(view1) == getText(view2)

// convertir DP a Pixeles
fun dpToPx(dp: Int, context: Context): Int {
    val displayMetrics = context.resources.displayMetrics
    return Math.round(dp * (displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT))
}

fun pxToDp(dp: Int, context: Context): Int {
    val displayMetrics = context.resources.displayMetrics.density
    return ((displayMetrics * dp) + 0.5f).toInt()
}

// Solo se manda llamar: parent.inflate(R.layout.my_layout)
fun ViewGroup.inflate(@LayoutRes layoutRes: Int, attachToRoot: Boolean = false): View = LayoutInflater.from(context).inflate(layoutRes, this, attachToRoot)

/***
 * url = Imagen a descargar y poner en la imagen
 * error = id de Drawable a poner en caso de que exista un error al descargar la imagen. R.drawable.my_icon
 * transformation = en caso de ser CircleTransform o alguna otra transformación a la imagen descargada
 */
fun ImageView.loadUrl(url: String?, error: Int, transformation: Transformation?) {
    if (!url.isNullOrEmpty()) {
        if (transformation == null)
            Picasso.get().load(url).error(error).into(this)
        else
            Picasso.get().load(url).error(error).transform(transformation).into(this)
    }
}

// animación para registro
fun revalView(view: View?) {
    if (view == null) return
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
        val cx = view.width.toFloat() - ((view.left + view.right) / 2)
        val cy = 0 // (view.top + view.bottom) / 2

        val finalRadius = view.width.toFloat()

        val anim: Animator?
        if (view.visibility == View.INVISIBLE) {
            anim = ViewAnimationUtils.createCircularReveal(view, cx.toInt(), cy, 0f, finalRadius)
            // make the view visible and start the animation
            view.visibility = View.VISIBLE
            anim.start()
        } else {
            anim = ViewAnimationUtils.createCircularReveal(view, cx.toInt(), cy, finalRadius, 0f)
            anim!!.addListener(object : Animator.AnimatorListener {
                override fun onAnimationRepeat(p0: Animator?) {}

                override fun onAnimationCancel(p0: Animator?) {}

                override fun onAnimationStart(p0: Animator?) {}

                override fun onAnimationEnd(p0: Animator?) {
                    view.visibility = View.INVISIBLE
                }
            })
            anim.start()
        }
    } else {
        if (view.visibility == View.INVISIBLE) {
            view.visibility = View.VISIBLE
        } else {
            view.visibility = View.INVISIBLE
        }
    }
}

// aunque hace lo mismo que la otra :D, esta es para la caja de buscar
fun revalViewSearch(view: View?) {
    if (view == null) return
    view.post {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            val cx = view.width.toFloat() - ((view.left + view.right) / 6)
            val cy = (view.top + view.bottom) / 2

            val finalRadius = view.width.toFloat()

            val anim: Animator?
            if (view.visibility == View.INVISIBLE) {
                anim = ViewAnimationUtils.createCircularReveal(view, cx.toInt(), cy, 0f, finalRadius)
                // make the view visible and start the animation
                view.visibility = View.VISIBLE
                anim.start()
            } else {
                anim = ViewAnimationUtils.createCircularReveal(view, cx.toInt(), cy, finalRadius, 0f)
                anim!!.addListener(object : Animator.AnimatorListener {
                    override fun onAnimationRepeat(p0: Animator?) {}

                    override fun onAnimationCancel(p0: Animator?) {}

                    override fun onAnimationStart(p0: Animator?) {}

                    override fun onAnimationEnd(p0: Animator?) {
                        view.visibility = View.INVISIBLE
                    }
                })
                anim.start()
            }
        } else {
            if (view.visibility == View.INVISIBLE) {
                view.visibility = View.VISIBLE
            } else {
                view.visibility = View.INVISIBLE
            }
        }
    }
}

fun View.setOElevation(elevation: Int) {
    val finalPx = dpToPx(elevation, context)
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
        this.elevation = finalPx.toFloat()
    } else {
        ViewCompat.setElevation(this, finalPx.toFloat())
    }
}

fun round(num: Double): Double = Math.floor(num * 100) / 100

fun round(num: String): Double = round(num.toDouble())

fun justifyText(text: String): String {
    var t = "<html>\n" +
            "<head>" +
            "<style type=\"text/css\">" +
            "@font-face {\n" +
            "    font-family: MyFont;\n" +
            "    src: url(\"file:///android_asset/fonts/MundoLight.otf\")\n" +
            "}\n" +
            "body {\n" +
            "    font-family: MyFont;\n" +
            "    font-size: medium;\n" +
            "    text-align: justify;\n" +
            "}\n" +
            "</style>" +
            "</head>\n" +
            "<body bgcolor=\"#f7f7f7\">\n" +
            "<label>00</label>" +
            "</body>\n" +
            "</html>"
    t = t.replace("00", text)
    return t
}

fun formatNumberDecimal(number: Any) : String{
    val otherSymbols = DecimalFormatSymbols(Locale.getDefault())
    otherSymbols.decimalSeparator = '.'
    otherSymbols.groupingSeparator = ','
    val formatea = DecimalFormat("###,###,###.###", otherSymbols)
    return formatea.format(number)
}

fun scaleView(view: View, startScale: Float, endScale: Float, visibility: Int, duration: Int) {
    val anim = ScaleAnimation(1f, 1f, startScale, endScale, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f)
    anim.fillAfter = false
    anim.duration = duration.toLong()
    anim.interpolator = LinearInterpolator()
    anim.setAnimationListener(object : Animation.AnimationListener {
        override fun onAnimationRepeat(p0: Animation?) {}
        override fun onAnimationEnd(p0: Animation?) {
            view.visibility = visibility
        }

        override fun onAnimationStart(p0: Animation?) {
            if (visibility == View.VISIBLE)
                view.visibility = visibility
            Handler().postDelayed({
                view.visibility = visibility
            }, anim.duration)
        }
    })
    view.startAnimation(anim)
}

fun Activity.dialogView() : AlertDialog {
    val dialogLo = SpotsDialog.Builder().setContext(this).setCancelable(false).build()
    return dialogLo
}

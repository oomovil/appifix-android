package oomovil.com.appifix.views

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Color
import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import com.chauthai.swipereveallayout.ViewBinderHelper
import kotlinx.android.synthetic.main.item_shipping_buy.view.*
import kotlinx.android.synthetic.main.item_text_header.view.*
import oomovil.com.appifix.R
import oomovil.com.appifix.general.formatNumberDecimal
import oomovil.com.appifix.general.inflate
import oomovil.com.appifix.general.round
import oomovil.com.appifix.models.MakePayment

/**
 * Created by Alan on 11/12/2017.
 */
class AdapterShippingBuy(private val items: List<MakePayment.Shipping>, val context: Context) : RecyclerView.Adapter<AdapterShippingBuy.ViewHolder>() {

    override fun onBindViewHolder(p0: ViewHolder, p1: Int) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    private val titleItem = 0
    private val normalItem = 1

    override fun getItemCount(): Int = items.size

    override fun getItemViewType(position: Int): Int {
        val i = items[position]
        return when (i.type) {
            MakePayment.ShippingType.TITLE -> titleItem
            MakePayment.ShippingType.NORMAL -> normalItem
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder = when (viewType) {
        titleItem ->
            ViewHolder(parent.inflate(R.layout.item_text_header, false))
        else ->
            ViewHolder(parent.inflate(R.layout.item_shipping_buy, false))
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: ViewHolder, position: Int, payloads: MutableList<Any>) {
        when (holder.itemViewType) {
            titleItem -> {
                holder.itemView.txt_title_normal.visibility = View.GONE
                holder.itemView.txt_title_shipping.visibility = View.VISIBLE
            }
            else -> {
                val shippi = items[position]

                holder.itemView.txt_paquete.text = shippi.name
                val cost = round(shippi.additionalCost)
                if(cost == 0.0){
                    holder.itemView.txt_costo_adi.text = context.getString(R.string.sin_costo_adicional)
                } else{
                    val costChido = "$" + if (isDoubleInteger(cost)) formatNumberDecimal(cost.toInt()) else formatNumberDecimal(cost)
                    holder.itemView.txt_costo_adi.text = context.getString(R.string.costo_adicional) + costChido
                }

                holder.itemView.img_paquete.setImageResource(shippi.imge)

                if (shippi.isSelected) {
                    holder.itemView.btn_check.setColorFilter(Color.parseColor("#3e4e94"))
                } else {
                    holder.itemView.btn_check.colorFilter = null
                }

                holder.itemView.layLoca.setOnClickListener {
                    val select = !shippi.isSelected
                    items.forEach { l ->
                        l.isSelected = false
                    }
                    shippi.isSelected = select
                    notifyDataSetChanged()
                }

            }
        }
    }

    private fun isDoubleInteger(value: Double): Boolean {
        val integer = (value % 1)
        return integer == 0.0
    }

    class ViewHolder(val view: View) : RecyclerView.ViewHolder(view)
}
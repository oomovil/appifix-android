package oomovil.com.appifix.general

import android.graphics.Color
import android.text.Editable
import android.text.InputFilter
import android.text.TextWatcher
import android.widget.ImageView
import oomovil.com.appifix.R
import oomovil.com.appifix.oomoviews.OEdittext

/**
 * Created by Alan on 22/11/2017.
 */
class CreditCardNumberFormattingTextWatcher(private val img: ImageView) : TextWatcher {
    private var current = ""

    override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
    }

    override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {
    }

    override fun afterTextChanged(s: Editable) {
        if (s.toString() != current) {
            val userInput = s.toString().replace("[^\\d]".toRegex(), "")
            if (userInput.length <= 16) {
                val sb = StringBuilder()
                for (i in 0 until userInput.length) {
                    if (i % 4 == 0 && i > 0) {
                        sb.append(" ")
                    }
                    sb.append(userInput[i])
                }
                current = sb.toString()

                s.filters = arrayOfNulls<InputFilter>(0)
            }
            s.replace(0, s.length, current, 0, current.length)
            setImageType(current)
        }
    }

    private fun setImageType(number: String) {
        val type = CreditCardType.getCreditCardType(number.replace(" ", ""))
        when (type) {
            CreditCardType.Type.VISA -> {
                img.setImageResource(R.mipmap.visa_)
                img.colorFilter = null
            }
            CreditCardType.Type.MASTER -> {
                img.setImageResource(R.mipmap.logo_mastercard)
                img.colorFilter = null
            }
        /*CreditCardType.Type.AEXPRESS -> {
            holder.itemView.img_card.setImageResource(R.mipmap.ic_credit_card)
        }
        CreditCardType.Type.DINERS -> {
            holder.itemView.img_card.setImageResource(R.mipmap.ic_credit_card)
        }
        CreditCardType.Type.DISCOVER -> {
            holder.itemView.img_card.setImageResource(R.mipmap.ic_credit_card)
        }
        CreditCardType.Type.JBC -> {
            holder.itemView.img_card.setImageResource(R.mipmap.ic_credit_card)
        }*/
            else -> {
                img.setImageResource(R.mipmap.ic_credit_card)
                img.setColorFilter(Color.parseColor("#00287f"))
            }
        }
    }
}
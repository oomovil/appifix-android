package oomovil.com.appifix.views

import android.view.View

/**
 * Created by Alan on 24/10/2017.
 */
interface OnRecyclerItemClick {
    /***
     * @fragment -> es la referencia al fragment que se va a cargar
     * @id -> este es por si es necesario enviar un ID
     * @viewList -> lista de vistas con su transitionName
     * @data -> lista de datos <Clave, Valor> para crear un bundle
     */
    fun onClick(fragment: Int, id: String, viewList: List<Pair<View, String>>, data: List<Pair<String, String?>>)

    fun onClick(fragment: Int, id: String, idCategoria: String, nombreCat: String = "")
}
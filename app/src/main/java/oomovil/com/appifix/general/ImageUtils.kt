package oomovil.com.appifix.general

import android.graphics.*
import java.io.ByteArrayOutputStream
import android.graphics.Bitmap
import android.content.Context
import android.renderscript.ScriptIntrinsicBlur
import android.renderscript.Allocation
import android.renderscript.RenderScript
import android.renderscript.Element
import android.util.Base64
import android.view.View


/**
 * Created by Alan on 10/11/2017.
 */
class ImageUtils {
    companion object {
        fun bitmapToByte(bitmap: Bitmap): ByteArray {
            val stream = ByteArrayOutputStream()
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream)
            return stream.toByteArray()
        }


        fun circleTransform(source: Bitmap): Bitmap {
            val size = Math.min(source.width, source.height)

            val x = (source.width - size) / 2
            val y = (source.height - size) / 2

            val squaredBitmap = Bitmap.createBitmap(source, x, y, size, size)
            if (squaredBitmap != source) {
                source.recycle()
            }

            val bitmap = Bitmap.createBitmap(size, size, source.config)

            val canvas = Canvas(bitmap)
            val paint = Paint()
            val shader = BitmapShader(squaredBitmap, Shader.TileMode.CLAMP, Shader.TileMode.CLAMP)
            paint.shader = shader
            paint.isAntiAlias = true

            val r = size / 2f
            canvas.drawCircle(r, r, r, paint)

            squaredBitmap.recycle()
            return bitmap
        }

        fun blurRenderScript(smallBitmap: Bitmap, radius: Int, repeat: Int, context: Context) : Bitmap {
            var bmp = smallBitmap
            try {
                bmp = rgb565ToArgb888(smallBitmap)
            } catch (e: Exception) {
                e.printStackTrace()
            }

            val bitmap = Bitmap.createBitmap( bmp.width, bmp.height, Bitmap.Config.ARGB_8888)

            val renderScript = RenderScript.create(context)

            val blurInput = Allocation.createFromBitmap(renderScript, smallBitmap)
            val blurOutput = Allocation.createFromBitmap(renderScript, bitmap)

            val blur = ScriptIntrinsicBlur.create(renderScript, Element.U8_4(renderScript))
            blur.setInput(blurInput)
            blur.setRadius(radius.toFloat()) // radius must be 0 < r <= 25
            // invoke the script to blur
            blur.forEach(blurOutput)
            // Repeat the blur for extra effect
           for (i in 0 until repeat) {
               blur.forEach(blurOutput)
           }

            blurOutput.copyTo(bitmap)
            renderScript.destroy()

            return bitmap
        }

        private fun rgb565ToArgb888(img: Bitmap) : Bitmap {
            val numPixels = img.width * img.height
            val pixels = IntArray(numPixels)
            //Get JPEG pixels.  Each int is the color values for one pixel.
            img.getPixels(pixels, 0, img.width, 0, 0, img.width, img.height)
            //Create a Bitmap of the appropriate format.
            val result = Bitmap.createBitmap(img.width, img.height, Bitmap.Config.ARGB_8888)
            //Set RGB pixels.
            result.setPixels(pixels, 0, result.width, 0, 0, result.width, result.height)
            return result
        }

        fun getBitmapFromView(view: View): Bitmap {
            val bitmap = Bitmap.createBitmap(view.width, view.height, Bitmap.Config.ARGB_8888)
            val c = Canvas(bitmap)
            view.layout(view.left, view.top, view.right, view.bottom)
            view.draw(c)
            return bitmap
        }

        fun bitMapToString(bitmap: Bitmap): String {
            val baos = ByteArrayOutputStream()
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, baos)
            val b = baos.toByteArray()
            return Base64.encodeToString(b, Base64.DEFAULT)
        }

        fun stringToBitMap(encodedString: String): Bitmap? {
            return try {
                val encodeByte = Base64.decode(encodedString, Base64.DEFAULT)
                BitmapFactory.decodeByteArray(encodeByte, 0, encodeByte.size)
            } catch (e: Exception) {
                e.message
                null
            }

        }

    }

}
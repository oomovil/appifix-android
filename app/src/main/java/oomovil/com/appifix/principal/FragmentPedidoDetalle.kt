package oomovil.com.appifix.principal


import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import io.realm.Realm
import kotlinx.android.synthetic.main.fragment_fragment_pedido_detalle.*
import kotlinx.android.synthetic.main.fragment_historial.*
import kotlinx.android.synthetic.main.fragment_productos.*

import oomovil.com.appifix.R
import oomovil.com.appifix.models.Model
import oomovil.com.appifix.models.UserModel
import oomovil.com.appifix.retrofit.ApiInterface
import oomovil.com.appifix.views.AdapterDetallePedido
import oomovil.com.appifix.views.OnRecyclerItemClick
import retrofit2.Call
import retrofit2.Response


/**
 * A simple [Fragment] subclass.
 */
class FragmentPedidoDetalle : Fragment() {


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_fragment_pedido_detalle, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        recyclerDetallePedido.layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)

        loadProductos(arguments!!.getString("id",""))
    }

    private fun loadProductos(idOrden: String) {
        progressHist.visibility = View.VISIBLE
        val map = HashMap<String, String>()
        map["idOrder"] = idOrden
        println("FragmentPedidoDetalle.loadProductos --> $map")
        val call = ApiInterface.create(context!!).getOrderDet(map)
        call.enqueue(object : retrofit2.Callback<Model.ResponseDetalleHistorial> {
            override fun onResponse(call: Call<Model.ResponseDetalleHistorial>?, response: Response<Model.ResponseDetalleHistorial>?) {
                progressHist.visibility = View.GONE
                if (activity != null && !activity!!.isFinishing && context != null) {
                    if (response != null && response.isSuccessful && response.body() != null && response.body()!!.status == 200 && response.body()!!.productos.isNotEmpty()) {
                        if (context != null) {
                            recyclerDetallePedido.adapter = AdapterDetallePedido(response.body()!!.productos,response.body()!!.detalle,context!!)
                        }
                    } else {
                        if (context != null) {
                            Snackbar.make(recyclerDetallePedido,"Error del servidor.",Snackbar.LENGTH_LONG)
                        }
                    }
                }
            }

            override fun onFailure(call: Call<Model.ResponseDetalleHistorial>?, t: Throwable?) {
                progressHist.visibility = View.GONE
                if (activity != null && !activity!!.isFinishing && context != null) {
                    Snackbar.make(recyclerDetallePedido,"Error del servidor.",Snackbar.LENGTH_LONG)
                }
            }
        })
    }

}

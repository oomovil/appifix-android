package oomovil.com.appifix.principal

import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.Color
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutCompat
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.squareup.picasso.Picasso
import com.vansuita.pickimage.bean.PickResult
import com.vansuita.pickimage.bundle.PickSetup
import com.vansuita.pickimage.dialog.PickImageDialog
import com.vansuita.pickimage.enums.EPickType
import com.vansuita.pickimage.listeners.IPickResult
import dmax.dialog.SpotsDialog
import io.realm.Realm
import kotlinx.android.synthetic.main.fragment_perfil.*
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import oomovil.com.appifix.R
import oomovil.com.appifix.general.*
import oomovil.com.appifix.models.Model
import oomovil.com.appifix.models.UserModel
import oomovil.com.appifix.models.UserReg
import oomovil.com.appifix.oomoviews.OToast
import oomovil.com.appifix.preferences.PreferencesActivity
import oomovil.com.appifix.retrofit.ApiInterface
import retrofit2.Call
import retrofit2.Response

/**
 * Created by Alan on 26/10/2017.
 */
class FragmentPerfil : Fragment(), View.OnClickListener, IPickResult {

    private var user: UserModel? = null
    private var canEdit = false
    private var listener: UserChangeListener? = null
    private val dialogLo: AlertDialog by lazy {
        this.activity!!.dialogView()
    }

    interface UserChangeListener {
        fun userChange()
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        try {
            listener = context as UserChangeListener
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?):
            View? = inflater!!.inflate(R.layout.fragment_perfil, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        KeyboardManager.init(activity!!, view!!)

        setUserInfo()

        campo_nombre.addTextChangedListener(Watcher())
        campo_pass.addTextChangedListener(Watcher())

        btn_editar_prefs.setOnClickListener(this)
        btn_actualizar.setOnClickListener(this)
        btn_editar_pic.setOnClickListener(this)

    }

    inner class Watcher : TextWatcher {
        override fun afterTextChanged(p0: Editable?) {
            validateButton()
        }

        override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

        }

        override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

        }

    }

    override fun onClick(p0: View?) {
        when (p0?.id) {
            R.id.btn_editar_prefs -> {
                startActivity(Intent(context, PreferencesActivity::class.java))
            }
            R.id.btn_actualizar -> {
                if (canEdit) {
                    validateUserData()
                }
            }
            R.id.btn_editar_pic -> {
                choosePicture()
            }
        }
    }

    private fun choosePicture() {
        val setup = PickSetup()

        setup.title = getString(R.string.seleccionar_imagen)
        setup.titleColor = Color.parseColor("#3e4e94")

        setup.backgroundColor = Color.parseColor("#f7f7f7")

        setup.cameraButtonText = getString(R.string.camara)
        setup.cameraIcon = R.mipmap.camera_colored
        setup.galleryIcon = R.mipmap.gallery_colored
        setup.galleryButtonText = getString(R.string.galeria)
        setup.buttonTextColor = Color.parseColor("#3e4e94")

        setup.cancelText = getString(R.string.cancelar)
        setup.cancelTextColor = Color.parseColor("#8b8b8b")

        setup.setPickTypes(EPickType.GALLERY, EPickType.CAMERA)
        setup.buttonOrientation = LinearLayoutCompat.HORIZONTAL

        PickImageDialog.build(setup, this)
                .show(childFragmentManager)
    }

    override fun onPickResult(p0: PickResult?) {
        if (p0 != null && p0.error == null) {
            uploadNewPicture(p0.bitmap)
        } else {
            OToast.makeText(context!!, getString(R.string.error_cargar_imagen)).show()
        }
    }

    private fun validateButton() {
        var nameValid = false
        var passValid = false
        if (user != null && getText(campo_nombre)!! != user!!.name && !getText(campo_nombre)!!.isEmpty()) {
            nameValid = true
        }
        if (user!!.getUserReg() == UserReg.NORMAL) {
            if (user != null && getText(campo_pass)!! != user!!.password && !getText(campo_pass)!!.isEmpty()) {
                passValid = true
            }
        }
        canEdit = nameValid || passValid
        if (canEdit) {
            btn_actualizar.setBackgroundResource(R.drawable.btn_gradient_blue)
        } else {
            btn_actualizar.setBackgroundResource(R.drawable.btn_enabled)
        }
    }

    private fun setUserInfo() {
        user = Realm.getDefaultInstance().where(UserModel::class.java).findFirst()
        if (user != null) {
            campo_nombre.setText(user!!.name)
            campo_correo.text = user!!.email

            when {
                user!!.getUserReg() == UserReg.NORMAL -> {
                    img_social.visibility = View.GONE
                    campo_pass.setText(user!!.password)
                }
                user!!.getUserReg() == UserReg.FACEBOOK -> {
                    img_social.visibility = View.VISIBLE
                    img_social.setImageResource(R.mipmap.ic_f)
                    if (user!!.email.contains("@Facebook")) {
                        campo_correo.visibility = View.GONE
                    }
                    til_pass.visibility = View.GONE
                }
                user!!.getUserReg() == UserReg.GOOGLE -> {
                    img_social.visibility = View.VISIBLE
                    img_social.setImageResource(R.drawable.ic_g)
                    if (user!!.email.contains("@Google")) {
                        campo_correo.visibility = View.GONE
                    }
                    til_pass.visibility = View.GONE
                }
                else -> {
                    til_pass.visibility = View.GONE
                    img_social.visibility = View.GONE
                }
            }

            if (user!!.photo != null && user!!.photo!!.isNotEmpty()) {
                Picasso.get().load(user!!.photo).into(img_user)
            } else {
                img_user.setImageResource(R.mipmap.img_perfil_default)
                img_user.borderWidth = 0
            }
        }
    }

    private fun validateUserData() {
        if (canEdit) {
            dialogLo.show()
            val map = HashMap<String, String>()
            map.put("email", user!!.email)
            map.put("lang", AppConfig(context!!!!).language)
            map.put("nombre", getText(campo_nombre)!!)
            if (user!!.getUserReg() == UserReg.NORMAL) {
                map.put("pass", getText(campo_pass)!!)
            }
            println("FragmentPerfil.validateUserData --> $map")
            ApiInterface.create(context!!).updateProfile(map).enqueue(object : retrofit2.Callback<Model.Response> {
                override fun onFailure(call: Call<Model.Response>?, t: Throwable?) {
                    dialogLo.dismiss()
                    OToast.makeText(context!!, R.string.error_conexion).show()
                }

                override fun onResponse(call: Call<Model.Response>?, response: Response<Model.Response>?) {
                    try {
                        println("FragmentPerfil.onResponse --> ${response!!.body()}")
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                    if (response != null && response.isSuccessful && response.body() != null && response.body()!!.status == 200) {
                        val realm = Realm.getDefaultInstance()
                        realm.beginTransaction()
                        user!!.name = getText(campo_nombre)!!
                        if (user!!.getUserReg() == UserReg.NORMAL) {
                            user!!.password = getText(campo_pass)
                        }
                        realm.commitTransaction()
                        dialogLo.dismiss()
                        OToast.makeText(context!!, getString(R.string.perfil_actualizado)).show()
                        setUserInfo()
                        updateListener()
                    } else {
                        dialogLo.dismiss()
                        OToast.makeText(context!!, getString(R.string.error_actualizar_perfil)).show()
                    }
                }

            })
        }
    }

    private fun uploadNewPicture(bmp: Bitmap) {
        dialogLo.show()
        val rbEmail: RequestBody = RequestBody.create(MediaType.parse("text/plain"), Realm.getDefaultInstance().where(UserModel::class.java).findFirst()!!.email)
        val rblang: RequestBody = RequestBody.create(MediaType.parse("text/plain"), AppConfig(context!!).language)
        val imgByte: ByteArray = ImageUtils.bitmapToByte(bmp)
        val reqFile = RequestBody.create(MediaType.parse("image/*"), imgByte)
        val foto: MultipartBody.Part = MultipartBody.Part.createFormData("foto", "perfilPic.png", reqFile)

        val call = ApiInterface.create(context!!).uploadNewPicture(rbEmail, rblang, foto)
        call.enqueue(object : retrofit2.Callback<Model.ResponseUploadPicture> {
            override fun onResponse(call: Call<Model.ResponseUploadPicture>?, response: Response<Model.ResponseUploadPicture>?) {
                try {
                    println("FragmentPerfil.onResponse --> ${response!!.body()}")
                } catch (e: Exception) {
                    e.printStackTrace()
                }
                if (response != null && response.isSuccessful && response.body() != null && response.body()!!.img_status == 200) {
                    val realm = Realm.getDefaultInstance()
                    realm.beginTransaction()
                    user!!.photo = response.body()!!.picture
                    realm.commitTransaction()
                    dialogLo.dismiss()
                    setUserInfo()
                    updateListener()
                } else {
                    dialogLo.dismiss()
                    OToast.makeText(context!!, getString(R.string.error_actualizar_foto)).show()
                }
            }

            override fun onFailure(call: Call<Model.ResponseUploadPicture>?, t: Throwable?) {
                dialogLo.dismiss()
                OToast.makeText(context!!, getString(R.string.error_conexion)).show()
            }

        })
    }

    private fun updateListener() {
        listener?.userChange()
    }

}
package oomovil.com.appifix.principal

import android.annotation.SuppressLint
import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import io.realm.Realm
import kotlinx.android.synthetic.main.fragment_notificacion_detalle.*
import kotlinx.android.synthetic.main.item_image.*
import oomovil.com.appifix.R
import oomovil.com.appifix.general.AppConfig
import oomovil.com.appifix.general.JustifyTextView
import oomovil.com.appifix.models.Notificacion
import oomovil.com.appifix.views.FragmentStatePagerAdapter

/**
 * Created by Alan on 07/12/2017.
 */
class FragmentNotificacionesDetalle : Fragment() {

    private var listener: OnFragmentListener? = null
    private lateinit var id: String
    private val fragments: ArrayList<Fragment> = ArrayList()
    private val notifi: Notificacion by lazy {
        Realm.getDefaultInstance().where(Notificacion::class.java).equalTo("id", id.toInt()).findFirst()!!
    }
    private val config: AppConfig by lazy {
        AppConfig(context!!)
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        try {
            listener = context as OnFragmentListener
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?):
            View? = inflater.inflate(R.layout.fragment_notificacion_detalle, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        btn_principal.setOnClickListener {
            listener?.changeView(OnFragmentListener.FRAGMENT_CATEGORIA, "-1")
        }

        id = arguments!!.getString("id")
        showDetailsNotification()
    }

    private fun showDetailsNotification() {
        if (config.language == "ES") {
            txt_title_noti.text = notifi.titulo
            txt_descrip_noti.text = notifi.mensaje
        } else {
            txt_title_noti.text = notifi.title
            txt_descrip_noti.text = notifi.message
        }

        JustifyTextView.justify(txt_descrip_noti)

        loadFragments()
        viewpagerNoti.adapter = FragmentStatePagerAdapter(fragments, childFragmentManager)
        pagerIndicator.setViewPager(viewpagerNoti)
    }

    private fun loadFragments() {
        notifi.images.forEach { url ->
            fragments.add(FragmentImage.getInstance(url!!.url))
        }
    }


}
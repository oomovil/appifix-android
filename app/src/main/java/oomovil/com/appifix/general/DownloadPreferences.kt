package oomovil.com.appifix.general

import android.content.Context
import io.realm.Realm
import oomovil.com.appifix.models.Categoria
import oomovil.com.appifix.models.Model
import oomovil.com.appifix.models.Preferences
import oomovil.com.appifix.retrofit.ApiInterface
import retrofit2.Call
import retrofit2.Response

/**
 * Created by Alan on 16/11/2017.
 */
class DownloadPreferences(private val ctx: Context, private var email: String?, private var listener: OnPreferencesListener) {

    interface OnPreferencesListener {
        fun onSuccess()
        fun onServerError()
        fun onInternalError()
    }

    init {
        downloadAllPreferences()
    }

    private fun downloadAllPreferences() {
        val map = HashMap<String, String>()
        map["id_papa"] = "2"
        map["lang"] = AppConfig(ctx).language
        val call = ApiInterface.create(ctx).getPreferences(map)
        call.enqueue(object : retrofit2.Callback<Model.ResponseCategorias> {
            override fun onFailure(call: Call<Model.ResponseCategorias>?, t: Throwable?) {
                serverError()
            }

            override fun onResponse(call: Call<Model.ResponseCategorias>?, response: Response<Model.ResponseCategorias>?) {
                if (response != null && response.isSuccessful && response.body() != null && response.body()!!.status == 200) {
                    saveApiPreferences(response.body()!!.prefs)
                } else {
                    internalError()
                }
            }

        })
    }

    private fun saveApiPreferences(prefs: List<Categoria>) {
        Realm.getDefaultInstance().executeTransactionAsync({ bgRealm ->
            bgRealm.where(Preferences::class.java).findAll().deleteAllFromRealm()
            if (prefs.isNotEmpty()) {
                prefs.forEach { p ->
                    val prefe = bgRealm.createObject(Preferences::class.java, p.id)
                    prefe.name = p.name
                    prefe.nameEn = p.nameEn
                    prefe.urlFoto = p.urlFoto
                }
            }
        }, {
            if (email == null) {
                success()
            } else {
                downloadUserPreferences()
            }
        }, { t ->
            t.printStackTrace()
            internalError()
        })
    }

    private fun downloadUserPreferences() {
        val map = HashMap<String, String>()
        map["email"] = email!!
        map["id_papa"] = "2"
        map["lang"] = AppConfig(ctx).language
        val call = ApiInterface.create(ctx).getUserPreferences(map)
        call.enqueue(object : retrofit2.Callback<Model.ResponseCategorias> {
            override fun onResponse(call: Call<Model.ResponseCategorias>?, response: Response<Model.ResponseCategorias>?) {
                if (response != null && response.isSuccessful && response.body() != null && response.body()!!.status == 200) {
                    saveUserPreferences(response.body()!!.prefs)
                } else {
                    internalError()
                }
            }

            override fun onFailure(call: Call<Model.ResponseCategorias>?, t: Throwable?) {
                serverError()
            }
        })
    }

    private fun saveUserPreferences(prefs: List<Categoria>) {
        Realm.getDefaultInstance().executeTransactionAsync({ realmBg ->
            if (prefs.isNotEmpty()) {
                prefs.forEach { pre ->
                    println("RegistroActivity.onResponse --> ${pre.name}")
                    val uPre = realmBg.where(Preferences::class.java).equalTo("id", pre.id).findFirst()
                    if (uPre != null) {
                        uPre.isSelected = true
                        realmBg.copyToRealmOrUpdate(uPre)
                    }
                }
            }
        }, {
            success()
        }, { t ->
            t.printStackTrace()
            internalError()
        })
    }

    private fun success() {
        listener.onSuccess()
    }

    private fun serverError() {
        listener.onServerError()
    }

    private fun internalError() {
        listener.onInternalError()
    }

}
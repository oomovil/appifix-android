package oomovil.com.appifix.registro

import android.animation.*
import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.drawable.Drawable
import android.net.Uri
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.view.View
import android.view.animation.*
import kotlinx.android.synthetic.main.activity_registro.*
import oomovil.com.appifix.R
import oomovil.com.appifix.general.*
import oomovil.com.appifix.oomoviews.OToast
import oomovil.com.appifix.preferences.PreferencesActivity
import com.facebook.*
import com.facebook.login.LoginManager
import com.facebook.login.LoginResult
import com.google.android.gms.auth.api.Auth
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.auth.api.signin.GoogleSignInResult
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.api.GoogleApiClient
import com.google.firebase.auth.*
import com.squareup.picasso.Picasso
import dmax.dialog.SpotsDialog
import io.realm.Realm
import io.realm.RealmList
import oomovil.com.appifix.models.*
import oomovil.com.appifix.principal.Principal
import oomovil.com.appifix.retrofit.ApiInterface
import retrofit2.Call
import retrofit2.Response


class RegistroActivity : AppCompatActivity(), View.OnClickListener, GoogleApiClient.OnConnectionFailedListener {

    private var isLogin = false
    private var isRegistering = false
    private var callbackManager: CallbackManager? = null
    private var gso: GoogleSignInOptions? = null
    private var mGoogleSignInClient: GoogleApiClient? = null
    private var bitmapProfileIfExist: Bitmap? = null
    private val OUR_REQUEST_CODE: Int = 1217
    private val dialogLo: AlertDialog by lazy {
        this@RegistroActivity.dialogView()
    }

    override fun attachBaseContext(newBase: Context) {
        super.attachBaseContext(MyContextWrapper.wrap(newBase, AppConfig(newBase).language))
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_registro)

        animateIntro()

        btn_recuperar_pass.setUnderLine()
        btn_registrar.setOElevation(2)
        btn_fb.setOElevation(2)
        btn_google.setOElevation(2)
        setListeners()
    }

    override fun onConnectionFailed(p0: ConnectionResult) {
        println("RegistroActivity.onConnectionFailed")
        try {
            dialogLo.dismiss()
            OToast.makeText(this, getString(R.string.error_connecting_google)).show()
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    override fun onStart() {
        super.onStart()
        KeyboardManager.init(this)

        loadCallBackFacebook()

        val realm = Realm.getDefaultInstance()
        realm.beginTransaction()
        realm.deleteAll()
        realm.commitTransaction()
    }

    override fun onPostCreate(savedInstanceState: Bundle?) {
        super.onPostCreate(savedInstanceState)
        loadClientGoogle()
    }

    private fun setListeners() {
        btn_recuperar_pass.setOnClickListener(this)
        btn_registrar.setOnClickListener(this)
        btn_fb.setOnClickListener(this)
        btn_google.setOnClickListener(this)
        txt_flip.setOnClickListener(this)
    }

    private fun loadCallBackFacebook() {
        if (callbackManager == null) {
            login_button.setReadPermissions("public_profile", "email")
            callbackManager = CallbackManager.Factory.create()
            login_button.registerCallback(callbackManager, object : FacebookCallback<LoginResult> {
                override fun onSuccess(loginResult: LoginResult) {
                    println("RegistroActivity.onSuccess --> " + loginResult.accessToken)
                    obtenerFB(loginResult.accessToken)
                }

                override fun onCancel() {
                    println("RegistroActivity.onCancel")
                }

                override fun onError(e: FacebookException) {
                    e.printStackTrace()
                    isRegistering = false
                    OToast.makeText(this@RegistroActivity, getString(R.string.error_obtener_datos_redes)).show()
                }
            })
        }
    }

    private fun obtenerFB(accessToken: AccessToken) {
        loginUser("", "", "", UserReg.FACEBOOK, accessToken.token)
        /*
        val credential = FacebookAuthProvider.getCredential(accessToken.token)
        FirebaseAuth.getInstance().signInWithCredential(credential)
                .addOnCompleteListener(this) { task ->
                    if (task.isSuccessful) {
                        val user = task.result.user
                        println("RegistroActivity.obtenerFB --> ${user.email}")
                        println("RegistroActivity.obtenerFB --> ${user.displayName}")
                        println("RegistroActivity.obtenerFB --> ${user.photoUrl}")
                        FirebaseAuth.getInstance().signOut()
                        LoginManager.getInstance().logOut()

                        val email = if (user.email == null) "${user.uid}@Facebook.com" else user.email
                        bitmapProfileIfExist = null
                        if (user.photoUrl != null) {
                            downloadProfilePic(user.displayName!!, email!!, user.photoUrl!!, UserReg.FACEBOOK, accessToken.token)
                        } else {

                        }
                    } else {
                        pd.cancel()
                        OToast.makeText(this@RegistroActivity, getString(R.string.error_obtener_datos_redes)).show()
                        LoginManager.getInstance().logOut()
                    }
                }
        */
    }

    /*private fun getUserPicFB(imageURL: Uri, name: String, email: String) {
        //val imageURL = "https://graph.facebook.com/$userID/picture?type=normal"
        println("RegistroActivity.getUserPicFB --> $imageURL")
        Picasso.with(this).load(imageURL)
                .into(object : com.squareup.picasso.Target {
                    override fun onBitmapFailed(errorDrawable: Drawable?) {
                        println("RegistroActivity.onBitmapFailed")
                        pd.cancel()
                        loginUser(name, email, null, UserReg.FACEBOOK)
                    }

                    override fun onPrepareLoad(placeHolderDrawable: Drawable?) {
                        println("RegistroActivity.onPrepareLoad")
                    }

                    override fun onBitmapLoaded(bitmap: Bitmap?, from: Picasso.LoadedFrom?) {
                        println("RegistroActivity.onBitmapLoaded --> ${if (bitmap == null) "bitmap null" else "bitmap chiiidooooo"}")
                        bitmapProfileIfExist = bitmap
                        pd.cancel()
                        loginUser(name, email, null, UserReg.FACEBOOK)
                    }
                })
    }*/

    private fun loadClientGoogle() {
        if (mGoogleSignInClient == null || gso == null) {
            gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                    .requestIdToken(getString(R.string.default_web_client_id))
                    .requestEmail()
                    .requestId()
                    .requestProfile().build()
            mGoogleSignInClient = GoogleApiClient.Builder(this)
                    .enableAutoManage(this, this)
                    .addApi(Auth.GOOGLE_SIGN_IN_API, gso!!)
                    .build()
        }
    }

    override fun onClick(v: View?) {
        when (v!!.id) {
            R.id.btn_recuperar_pass -> {
                startActivity(Intent(this, PopRecuperarPass::class.java))
            }
            R.id.btn_registrar -> {
                if (!isRegistering) {
                    isRegistering = true
                    validarCampos()
                }
            }
            R.id.btn_fb -> {
                login_button.performClick()
            }
            R.id.btn_google -> {
                dialogLo.show()
                Auth.GoogleSignInApi.signOut(mGoogleSignInClient)
                val signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleSignInClient!!)
                startActivityForResult(signInIntent, OUR_REQUEST_CODE)
            }
            R.id.txt_flip -> {
                flipView(200)
            }
        }
    }
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        try {
            if (requestCode == OUR_REQUEST_CODE) {
                // Resolve the intent into a GoogleSignInResult we can process.
                val result = Auth.GoogleSignInApi.getSignInResultFromIntent(data)
                handleSignInResult(result!!)
            } else
                callbackManager!!.onActivityResult(requestCode, resultCode, data)
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    private fun handleSignInResult(result: GoogleSignInResult) {
        try {
            if (result.isSuccess) {
                val acct = result.signInAccount
                firebaseAuthWithGoogle(acct!!)

                /*println("RegistroActivity.handleSignInResult --> ${acct!!.displayName} --> ${acct.email} --> ${acct.photoUrl}")
                Auth.GoogleSignInApi.signOut(mGoogleSignInClient)
                downloadProfilePicGoogle(acct.displayName!!, acct.email!!, acct.photoUrl!!)*/
            } else {
                dialogLo.dismiss()
                OToast.makeText(this, getString(R.string.error_acces_google_data)).show()
                println("RegistroActivity.handleSignInResult --> no se pudo acceder a los datos de google")
                FirebaseAuth.getInstance().signOut()
            }
        } catch (e: Exception) {
            dialogLo.dismiss()
            OToast.makeText(this, getString(R.string.error_acces_google_data)).show()
            FirebaseAuth.getInstance().signOut()
            e.printStackTrace()
        }
    }

    private fun firebaseAuthWithGoogle(acct: GoogleSignInAccount) {
        dialogLo.dismiss()
        loginUser("", "", "", UserReg.GOOGLE, acct.idToken!!)
        println("RegistroActivity.firebaseAuthWithGoogle --> ${acct.idToken}")

        /*
        val credential = GoogleAuthProvider.getCredential(acct.idToken, null)
        FirebaseAuth.getInstance().signInWithCredential(credential)
                .addOnCompleteListener(this) { task ->
                    if (task.isSuccessful) {
                        val user = task.result.user
                        println("RegistroActivity.obtenerGoogle --> ${user.email}")
                        println("RegistroActivity.obtenerGoogle --> ${user.displayName}")
                        println("RegistroActivity.obtenerGoogle --> ${user.photoUrl}")
                        FirebaseAuth.getInstance().signOut()
                        Auth.GoogleSignInApi.signOut(mGoogleSignInClient)

                        val email = if (user.email == null) "${user.uid}@Google.com" else user.email
                        bitmapProfileIfExist = null
                        if (user.photoUrl != null) {
                            println("RegistroActivity.firebaseAuthWithGoogle --> descargar foto")
                            downloadProfilePic(user.displayName!!, email!!, user.photoUrl!!, UserReg.GOOGLE, acct.idToken!!)
                        } else {
                            println("RegistroActivity.firebaseAuthWithGoogle --> hacer login")

                        }
                    } else {
                        pd.cancel()
                        OToast.makeText(this, getString(R.string.error_acces_google_data)).show()
                        FirebaseAuth.getInstance().signOut()
                        Auth.GoogleSignInApi.signOut(mGoogleSignInClient)
                    }
                }
        */
    }

    private fun downloadProfilePic(name: String, email: String, urlPhoto: Uri, userReg: UserReg, token: String) {
        Picasso.get().load(urlPhoto)
                .into(object : com.squareup.picasso.Target {
                    override fun onBitmapFailed(e: java.lang.Exception?, errorDrawable: Drawable?) {
                        println("RegistroActivity.onBitmapFailed")
                        dialogLo.dismiss()
                        FirebaseAuth.getInstance().signOut()
                        loginUser(name, email, null, userReg, token)
                    }

                    override fun onPrepareLoad(placeHolderDrawable: Drawable?) {
                        println("RegistroActivity.onPrepareLoad")
                    }

                    override fun onBitmapLoaded(bitmap: Bitmap?, from: Picasso.LoadedFrom?) {
                        dialogLo.dismiss()
                        FirebaseAuth.getInstance().signOut()
                        println("RegistroActivity.onBitmapLoaded --> ${if (bitmap == null) "bitmap null" else "bitmap chiiidooooo"}")
                        bitmapProfileIfExist = bitmap
                        loginUser(name, email, null, userReg, token)
                    }
                })
    }

    private fun validarCampos() {
        val nameValid = !isEmpty(campo_nombre)// && validOnlyText(getText(campo_nombre)!!)
        val emailValid = !isEmpty(campo_correo) && validateEmail(getText(campo_correo)!!)
        val passValid = !isEmpty(campo_pass) && getText(campo_pass)!!.length >= 4

        if (isLogin) {
            if (!nameValid) {
                isRegistering = false
                OToast.makeText(this, getString(R.string.invalid_name)).show()
            } else if (!emailValid) {
                isRegistering = false
                OToast.makeText(this, getString(R.string.invalid_email)).show()
            } else if (!passValid) {
                isRegistering = false
                OToast.makeText(this, getString(R.string.invalid_pass)).show()
            } else {
                registerUser(getText(campo_nombre)!!, getText(campo_correo)!!, getText(campo_pass)!!, UserReg.NORMAL, "")
            }
        } else {
            if (!emailValid) {
                isRegistering = false
                OToast.makeText(this, getString(R.string.invalid_email)).show()
            } else if (!passValid) {
                isRegistering = false
                OToast.makeText(this, getString(R.string.invalid_pass)).show()
            } else {
                loginUser("", getText(campo_correo)!!, getText(campo_pass)!!, UserReg.NORMAL, "")
            }
        }
    }

    private fun registerUser(name: String, email: String, pass: String?, userReg: UserReg, token: String) {
        dialogLo.show()

        val map: HashMap<String, String> = hashMapOf()
        when (userReg) {
            UserReg.NORMAL -> {
                map["nombre"] = name
                map["email"] = email
                map["pass"] = pass ?: ""
            }
            UserReg.FACEBOOK -> {
                map["fb_token"] = token
            }
            UserReg.GOOGLE -> {
                map["gp_token"] = token
            }
        }

        map["lang"] = AppConfig(this).language

        println("RegistroActivity.registerUser --> $map")

        ApiInterface.create(this).regUserNew(map)
                .enqueue(object : retrofit2.Callback<Model.ResponseRegistro> {
                    override fun onFailure(call: Call<Model.ResponseRegistro>, t: Throwable) {
                        isRegistering = false
                        dialogLo.dismiss()
                        println("RegistroActivity.onFailure Registro: --> ${t.message!!}")
                        t.printStackTrace()
                        println("RegistroActivity.onFailure --> ${call.request()?.body()?.contentType()?.toString()}")
                        OToast.makeText(this@RegistroActivity, getString(R.string.error_conexion)).show()
                        LoginManager.getInstance().logOut()
                    }

                    override fun onResponse(call: Call<Model.ResponseRegistro>, response: Response<Model.ResponseRegistro>) {
                        isRegistering = false

                        if (response != null && response.isSuccessful && response.body() != null && response.body()!!.status == 200) {
                            println("RegistroActivity.onResponse --> ${response.body()!!}")
                            Realm.getDefaultInstance().executeTransactionAsync({ realm ->
                                val user = realm.createObject(UserModel::class.java, response.body()!!.userId)

                                user.email = response.body()!!.email
                                user.password = pass
                                user.name = response.body()!!.nombre
                                user.token = token
                                user.photo = response.body()!!.foto
                                user.setUserReg(userReg)
                                user.setUserType(UserType.NORMAL)

                            }, {
                                LoginManager.getInstance().logOut()
                                dialogLo.dismiss()
                                println("RegistroActivity.onResponse --> Success")
                                pasToPreferences()
                            }, { t ->
                                dialogLo.dismiss()
                                OToast.makeText(this@RegistroActivity, getString(R.string.error_save_data_login)).show()
                                t.printStackTrace()
                            })
                        } else {
                            dialogLo.dismiss()
                            LoginManager.getInstance().logOut()
                            try {
                                println("RegistroActivity.onResponse --> ${response.errorBody()}")
                                println("RegistroActivity.onResponse --> ${response.body()!!}")

                                val msg = response.body()!!.message
                                if (msg == "El correo ingresado ya esta registrado en el sistema.")
                                    OToast.makeText(this@RegistroActivity, getString(R.string.usuario_existente_bd)).show()
                                else
                                    OToast.makeText(this@RegistroActivity, getString(R.string.usuario_no_registrado_error)).show()
                            } catch (e: Exception) {
                                e.printStackTrace()
                                OToast.makeText(this@RegistroActivity, getString(R.string.usuario_no_registrado_error)).show()
                            }
                        }
                    }

                })

        /*var rbName: RequestBody? = null
        var rbEmail: RequestBody? = null
        val rblang: RequestBody = RequestBody.create(MediaType.parse("text/plain"), AppConfig(this).language)
        var rbPass: RequestBody? = null
        var rbFbToken: RequestBody? = null
        var rbGToken: RequestBody? = null
        var foto: MultipartBody.Part? = null // esta parte solo se crea si es por Facebook o Google

        when (userReg) {
            UserReg.NORMAL -> {
                rbName = RequestBody.create(MediaType.parse("text/plain"), name)
                rbEmail = RequestBody.create(MediaType.parse("text/plain"), email)
                rbPass = RequestBody.create(MediaType.parse("text/plain"), pass!!)
            }
            UserReg.FACEBOOK -> {
                rbFbToken = RequestBody.create(MediaType.parse("text/plain"), token)
            }
            UserReg.GOOGLE -> {
                rbGToken = RequestBody.create(MediaType.parse("text/plain"), token)
            }
        }*/

        //if (bitmapProfileIfExist != null) {
          //  val imgByte: ByteArray = ImageUtils.bitmapToByte(bitmapProfileIfExist!!)
            //val reqFile = RequestBody.create(MediaType.parse("image/*"), imgByte)
           // foto = MultipartBody.Part.createFormData("foto", "perfilPic.png", reqFile)
        //}


        /*val call = ApiInterface.create(this).regUser(rbName, rbEmail, rbPass, rbFbToken, rbGToken, rblang, foto)
        call.enqueue(object : retrofit2.Callback<Model.ResponseRegistro> {
            override fun onResponse(call: Call<Model.ResponseRegistro>?, response: Response<Model.ResponseRegistro>?) {
                isRegistering = false

                if (response != null && response.isSuccessful && response.body() != null && response.body()!!.status == 200) {
                    println("RegistroActivity.onResponse --> ${response.body()!!}")
                    Realm.getDefaultInstance().executeTransactionAsync({ realm ->
                        val user = realm.createObject(UserModel::class.java, response.body()!!.userId)

                        user.email = email
                        user.password = pass
                        user.name = name
                        user.token = token
                        user.setUserReg(userReg)
                        user.setUserType(UserType.NORMAL)
                        if (bitmapProfileIfExist != null)
                            user.picture = ImageUtils.bitmapToByte(bitmapProfileIfExist!!)

                    }, {
                        //pd.cancel()
                        println("RegistroActivity.onResponse --> Success")
                        pasToPreferences()
                    }, { t ->
                        pd.cancel()
                        OToast.makeText(this@RegistroActivity, getString(R.string.error_save_data_login)).show()
                        t.printStackTrace()
                    })
                } else {
                    pd.cancel()
                    try {
                        println("RegistroActivity.onResponse --> ${response!!.errorBody()}")
                        println("RegistroActivity.onResponse --> ${response.body()!!}")

                        val msg = response.body()!!.message
                        if (msg == "El correo ingresado ya esta registrado en el sistema.")
                            OToast.makeText(this@RegistroActivity, getString(R.string.usuario_existente_bd)).show()
                        else
                            OToast.makeText(this@RegistroActivity, getString(R.string.usuario_no_registrado_error)).show()
                    } catch (e: Exception) {
                        e.printStackTrace()
                        OToast.makeText(this@RegistroActivity, getString(R.string.usuario_no_registrado_error)).show()
                    }
                }
            }

            override fun onFailure(call: Call<Model.ResponseRegistro>?, t: Throwable?) {
                isRegistering = false
                pd.cancel()
                println("RegistroActivity.onFailure Registro: --> ${t!!.message!!}")
                t.printStackTrace()
                println("RegistroActivity.onFailure --> ${call?.request()?.body()?.contentType()?.toString()}")
                OToast.makeText(this@RegistroActivity, getString(R.string.error_conexion)).show()
            }

        })*/
    }

    private fun loginUser(name: String, email: String, pass: String?, userReg: UserReg, token: String) {
        dialogLo.show()
        val map = HashMap<String, String>()
        map["lang"] = AppConfig(this).language
        when (userReg) {
            UserReg.NORMAL -> {
                map["upass"] = pass!!
                map["uname"] = email
            }
            UserReg.FACEBOOK -> {
                map["fb_token"] = token
            }
            UserReg.GOOGLE -> {
                map["gp_token"] = token
            }
        }
        val call = ApiInterface.create(this).login(map)
        call.enqueue(object : retrofit2.Callback<Model.ResponseLogin> {
            override fun onFailure(call: Call<Model.ResponseLogin>?, t: Throwable?) {
                dialogLo.dismiss()
                t!!.printStackTrace()
                try {
                    println("RegistroActivity.onFailure --> ${t.localizedMessage}")
                    println("RegistroActivity.onFailure --> ${t.cause}")
                    println("RegistroActivity.onFailure --> ${t.message}")
                } catch (e : Exception) {
                    e.printStackTrace()
                }
                OToast.makeText(this@RegistroActivity, getString(R.string.error_conexion)).show()
                LoginManager.getInstance().logOut()
            }

            override fun onResponse(call: Call<Model.ResponseLogin>?, response: Response<Model.ResponseLogin>?) {
                try {
                    println("RegistroActivity.onResponse --> ${response!!.body()}")
                } catch (e: Exception) {
                    e.printStackTrace()
                }
                try {
                    if (response != null && response.isSuccessful && response.body() != null && response.body()!!.status == 200) {
                        Realm.getDefaultInstance().executeTransactionAsync({ realm ->

                            val user = realm.createObject(UserModel::class.java, Integer.parseInt(response.body()!!.user.id))

                            user!!.email = response.body()!!.user.email
                            user.name = response.body()!!.user.nombre
                            user.password = pass
                            user.photo = response.body()!!.user.foto
                            user.setUserReg(userReg)

                            if (user.userReg == "FACEBOOK" || user.userReg == "GOOGLE") {
                                LoginManager.getInstance().logOut()
                                user.token = token
                            }

                            user.setUserType(if (response.body()!!.user.tipo_usuario_id == "1") UserType.NORMAL else UserType.PROVIDER)

                            realm.where(Locations::class.java).findAll().deleteAllFromRealm()
                            if (response.body()!!.user.locations.isNotEmpty()) {
                                response.body()!!.user.locations.forEach { loca ->
                                    val newLoca = realm.createObject(Locations::class.java, loca.id)
                                    newLoca.alias = loca.alias
                                    newLoca.direccion = loca.direccion
                                    newLoca.colonia = loca.colonia
                                    newLoca.telefono = loca.telefono
                                    newLoca.ciudad = loca.ciudad
                                    newLoca.estado = loca.estado
                                    newLoca.cp = loca.cp
                                    newLoca.paisId = loca.paisId
                                    newLoca.principal = loca.principal
                                }
                            }

                            realm.where(Carrito::class.java).findAll().deleteAllFromRealm()
                            realm.where(ProductoCarrito::class.java).findAll().deleteAllFromRealm()
                            if (response.body()!!.user.carrito != null) {
                                val car = response.body()!!.user.carrito
                                val carrito = realm.createObject(Carrito::class.java, car!!.pedidoID)
                                carrito.cuponId = car.cuponId
                                carrito.cupon = car.cupon
                                carrito.cuponDesc = car.cuponDesc
                                carrito.subTotal = car.subTotal
                                carrito.total = car.total

                                if (car.productos != null && car.productos.isNotEmpty()) {
                                    val list = RealmList<ProductoCarrito>()
                                    car.productos.forEach { p ->
                                        // producto del carrito
                                        val pCarrito = realm.createObject(ProductoCarrito::class.java, p.id)
                                        pCarrito.count = p.count
                                        list.add(pCarrito)
                                        // fin de agregar los productos del carrito

                                        // crear el producto si no existe o actualizarlo
                                        // (esto por si no se han descargado los productos pero si existen en el carrito)
                                        //var proReal = realm.where(Product::class.java).equalTo("id", p.id).findFirst()
                                        realm.where(Product::class.java).equalTo("id", p.id).equalTo("categoriaID", p.categoriaID).findAll().deleteAllFromRealm()
                                        val proReal = realm.createObject(Product::class.java)
                                        /*var isNewP = false
                                        if (proReal == null) {
                                            proReal = realm.createObject(Product::class.java)
                                            isNewP = true
                                        }*/
                                        proReal!!.id = p.id
                                        proReal.nameEs = p.nameEs
                                        proReal.nameEng = p.nameEng
                                        proReal.detalleEs = p.detalleEs
                                        proReal.detalleEng = p.detalleEng
                                        proReal.imagen = p.imagen
                                        proReal.precioPublico = p.precioPublico
                                        proReal.precioDistribuidores = p.precioDistribuidores
                                        proReal.categoriaID = p.categoriaID
                                        proReal.descuento = p.descuento
                                        proReal.stock = p.stock
                                        /*if (!isNewP) {
                                            realm.copyToRealmOrUpdate(proReal)
                                        }*/
                                        //fin de producto real
                                    }
                                    carrito.productos = list
                                }
                            }

                            /*if (response.body()!!.user.foto != null && !response.body()!!.user.foto!!.contains("avatar")) {
                                val url = URL(response.body()!!.user.foto)
                                val connection = url.openConnection() as HttpURLConnection
                                val inputS = connection.inputStream
                                val bmp = BitmapFactory.decodeStream(inputS)
                                inputS.close()
                                if (bmp != null)
                                    user.picture = ImageUtils.bitmapToByte(bmp)
                            }*/

                        }, {
                            dialogLo.dismiss()
                            LoginManager.getInstance().logOut()
                            println("RegistroActivity.onResponse --> Success")
                            descargarPreferencias(email)
                        }, { t ->
                            isRegistering = false
                            dialogLo.dismiss()
                            LoginManager.getInstance().logOut()
                            OToast.makeText(this@RegistroActivity, getString(R.string.error_save_data_login)).show()
                            t.printStackTrace()
                        })
                    } else {
                        isRegistering = false
                        dialogLo.dismiss()
                        LoginManager.getInstance().logOut()

                        if (userReg != UserReg.NORMAL && (response!!.body()!!.message == "Datos de login incorrectos." || response.body()!!.message == "Incorrect login data.")) {
                            registerUser(name, email, pass, userReg, token)
                        } else {
                            OToast.makeText(this@RegistroActivity, response!!.body()!!.message).show()
                        }

                    }
                } catch (e: Exception) {
                    isRegistering = false
                    dialogLo.dismiss()
                    e.printStackTrace()
                    OToast.makeText(this@RegistroActivity, getString(R.string.error_conexion)).show()
                }
            }
        })
    }

    private fun descargarPreferencias(email: String) {
        DownloadPreferences(this, email, object : DownloadPreferences.OnPreferencesListener {
            override fun onSuccess() {
                println("RegistroActivity.onSuccess --> descarga de preferencias chidas")
                downloadCountries()
            }

            override fun onServerError() {
                println("RegistroActivity.onServerError --> error al descargarlas del servidor")
                downloadCountries()
            }

            override fun onInternalError() {
                println("RegistroActivity.onInternalError --> error al guardar las preferencias")
                downloadCountries()
            }
        })
    }

    private fun downloadCountries() {
        ApiInterface.create(this).getCountries().enqueue(object : retrofit2.Callback<Model.ResponsePaises> {
            override fun onFailure(call: Call<Model.ResponsePaises>?, t: Throwable?) {
                println("Splash.onFailure --> error al descargar paises")
                pasToPrincipal()
            }

            override fun onResponse(call: Call<Model.ResponsePaises>?, response: Response<Model.ResponsePaises>?) {
                if (response != null && response.isSuccessful && response.body() != null && response.body()!!.status == 200 && response.body()!!.paises.isNotEmpty()) {
                    Realm.getDefaultInstance().use { realm ->
                        realm.executeTransactionAsync({ realmBg ->
                            realmBg.where(Pais::class.java).findAll().deleteAllFromRealm()
                            response.body()!!.paises.forEach {
                                realmBg.copyToRealmOrUpdate(it)
                            }
                        }, {
                            println("Splash.onResponse --> paises guardados correctamente")
                            pasToPrincipal()
                        }, { t ->
                            println("Splash.onResponse --> error al guardar paises")
                            t.printStackTrace()
                            pasToPrincipal()
                        })
                    }
                } else
                    pasToPrincipal()
            }
        })
    }

    private fun pasToPrincipal() {
        dialogLo.dismiss()
        AppConfig(this).userSession = true
        val intent = Intent(this, Principal::class.java)
        intent.putExtra("isLogin", true)
        intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK and Intent.FLAG_ACTIVITY_CLEAR_TOP and Intent.FLAG_ACTIVITY_NEW_TASK
        startActivity(intent)
        this@RegistroActivity.finish()
    }

    private fun pasToPreferences() {
        dialogLo.dismiss()
        AppConfig(this).userSession = true
        val intent = Intent(this, PreferencesActivity::class.java)
        intent.putExtra("isRegistro", true)
        intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK and Intent.FLAG_ACTIVITY_CLEAR_TOP and Intent.FLAG_ACTIVITY_NEW_TASK
        startActivity(intent)
        this@RegistroActivity.finish()
    }

    private fun clearCampos() {
        campo_nombre.setText("")
        campo_correo.setText("")
        campo_pass.setText("")
    }

    // animaciones
    private fun animateIntro() {
        view_blue.post {
            revalView(view_blue)
        }
        moveUpToDown(logo, txt_title, cardView)
        moveDownToTop(lay_btn_reg, lay_btn_fb, lay_btn_google, lay_bottom)
    }

    private fun moveUpToDown(vararg views: View) {
        val upToDown = AnimationUtils.loadAnimation(this, R.anim.uptodown)
        views.forEach {
            it.startAnimation(upToDown)
        }
    }

    private fun moveDownToTop(vararg views: View) {
        val downToUp = AnimationUtils.loadAnimation(this, R.anim.downtoup)
        views.forEach {
            it.startAnimation(downToUp)
        }
    }

    private fun flipView(duration: Int) {
        if (isLogin) {
            translateToFade(lay_btn_fb, true)
            translateToFade(lay_btn_google, true)
            translateToFade(til_nombre, false)
            scaleView(btn_recuperar_pass, 0f, 1f, View.VISIBLE, duration)
            btn_registrar.text = getString(R.string.iniciar_sesion)
            txt_flip.text = getString(R.string.registrate_ahora)
            txt_cuenta.text = getString(R.string.aun_no_tienes_cuenta)
            txt_title.text = getString(R.string.inicio_sesion)
            clearCampos()
            isLogin = false
        } else {
            translateToFade(lay_btn_fb, false)
            translateToFade(lay_btn_google, false)
            translateToFade(til_nombre, true)
            scaleView(btn_recuperar_pass, 1f, 0f, View.GONE, duration)
            btn_registrar.text = getString(R.string.registrarme)
            txt_flip.text = getString(R.string.inicia_sesion)
            txt_cuenta.text = getString(R.string.ya_tienes_una_cuenta)
            txt_title.text = getString(R.string.registro)
            clearCampos()
            isLogin = true
        }
    }

    private fun translateToFade(view: View, move: Boolean) {
        val init = if (!move) 0f else -35f
        val end = if (!move) -35f else 0f
        val pvhT = PropertyValuesHolder.ofFloat(View.TRANSLATION_Y, init, end)
        val pvhAlpha = PropertyValuesHolder.ofFloat(View.ALPHA, if (!move) 0f else 1f)
        val translateAnimator = ObjectAnimator.ofPropertyValuesHolder(view, pvhT)
        translateAnimator.duration = 250
        val fadeAnimator = ObjectAnimator.ofPropertyValuesHolder(view, pvhAlpha)
        fadeAnimator.duration = 250
        fadeAnimator.interpolator = LinearInterpolator()

        val animatorSet = AnimatorSet()
        animatorSet.play(translateAnimator).with(fadeAnimator)

        animatorSet.addListener(object : Animator.AnimatorListener {
            override fun onAnimationRepeat(p0: Animator?) {

            }

            override fun onAnimationEnd(p0: Animator?) {
                if (!move) view.visibility = View.GONE else view.visibility = View.VISIBLE
            }

            override fun onAnimationCancel(p0: Animator?) {

            }

            override fun onAnimationStart(p0: Animator?) {
                view.visibility = View.VISIBLE
            }
        })
        animatorSet.start()
    }

    private fun scaleView(view: View, startScale: Float, endScale: Float, visibility: Int, duration: Int) {
        val anim = ScaleAnimation(startScale, endScale, startScale, endScale, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f)
        anim.fillAfter = false
        anim.duration = duration.toLong()
        anim.interpolator = LinearInterpolator()
        anim.setAnimationListener(object : Animation.AnimationListener {
            override fun onAnimationRepeat(p0: Animation?) {}
            override fun onAnimationEnd(p0: Animation?) {
                view.visibility = visibility
            }

            override fun onAnimationStart(p0: Animation?) {
                if (visibility == View.VISIBLE)
                    view.visibility = visibility
                Handler().postDelayed({
                    view.visibility = visibility
                }, anim.duration)
            }
        })
        view.startAnimation(anim)
    }
}

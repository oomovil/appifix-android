package oomovil.com.appifix.principal

import android.annotation.SuppressLint
import android.app.AlertDialog
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.widget.AdapterView
import dmax.dialog.SpotsDialog
import io.card.payment.CardIOActivity
import io.card.payment.CreditCard
import io.realm.Realm
import io.realm.RealmResults
import kotlinx.android.synthetic.main.activity_add_credit_card.*
import oomovil.com.appifix.R
import oomovil.com.appifix.general.*
import oomovil.com.appifix.models.Locations
import oomovil.com.appifix.models.Tarjeta
import oomovil.com.appifix.oomoviews.OToast
import java.util.*
import kotlin.collections.ArrayList

class ActivityAddCreditCard : AppCompatActivity() {

    private var isNewTarjeta = false
    private var idTarjeta: Int = -1
    private val MY_SCAN_REQUEST_CODE = 1217
    private var idLoc: String = ""
    private val dialogLo: AlertDialog by lazy {
        this.dialogView()
    }

    override fun attachBaseContext(newBase: Context) {
        super.attachBaseContext(MyContextWrapper.wrap(newBase, AppConfig(newBase).language))
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_credit_card)

        isNewTarjeta = intent.getBooleanExtra("isNewTarjeta", false)
        if (!isNewTarjeta) {
            txt_title.text = getString(R.string.editar_tarjeta)
        }
        btn_back.setOnClickListener { onBackPressed() }

        btn_scaner.setOnClickListener { initScaner() }

        campo_numero.addTextChangedListener(CreditCardNumberFormattingTextWatcher(img_credit))
        campo_expire.addTextChangedListener(CreditCardDateFormating())

        btn_add_tarjeta.setOnClickListener {
            validarDatos()
        }

        location_card_txt.setOnClickListener{
            selectCardLocation()
        }

        if (!isNewTarjeta) {
            showDataUser()
        }
    }

    override fun onStart() {
        super.onStart()
        KeyboardManager.init(this)
    }

    private fun showDataUser() {

        idTarjeta = intent.getIntExtra("idTarjeta", -1)
        val tarj = Realm.getDefaultInstance().where(Tarjeta::class.java).equalTo("id", idTarjeta).findFirst()
        if (tarj != null) {
            val loc = Realm.getDefaultInstance().where(Locations::class.java).equalTo("id", tarj.idLoc).findFirst()
            campo_alias.setText(tarj.cardName)
            campo_owner_name.setText(tarj.cardOwnerName)
            campo_numero.setText(tarj.cardNumber)
            campo_expire.setText(tarj.expiryDate)
            if (loc != null) {
                location_card_txt.text = loc.alias
                idLoc = loc.id ?: ""
            }
            //campo_cvv.setText(tarj.cardCvv)
            checkPrincipal.isChecked = tarj.isFavorite
        }
    }

    private fun validarDatos() {
        when {
            isEmpty(campo_alias) -> OToast.makeText(this, getString(R.string.ingresa_alias_correcto)).show()
            isEmpty(campo_owner_name) -> OToast.makeText(this, getString(R.string.ingresa_tarjetahabiente)).show()
            isEmpty(campo_numero) or !isValidCard() -> OToast.makeText(this, getString(R.string.ingresa_numero_tarjecta_correcta)).show()
            isEmpty(campo_expire) or (getText(campo_expire)!!.length < 5) -> OToast.makeText(this, getString(R.string.ingresa_fecha_correcta)).show()
            !expireDateValid(getText(campo_expire)!!) -> OToast.makeText(this, getString(R.string.ingresa_fecha_correcta)).show()
            idLoc.isEmpty() -> OToast.makeText(this, getString(R.string.Location_billing)).show()
            isEmpty(campo_cvv) or (getText(campo_cvv)!!.length < 3) -> OToast.makeText(this, getString(R.string.ingresa_cvv_correcto)).show()

            existSameName(getText(campo_alias)!!) and isNewTarjeta -> OToast.makeText(this, getString(R.string.tarjeta_nombre_existente)).show()
            !checkTerminos.isChecked -> OToast.makeText(this, getString(R.string.debes_aceptar_terminos)).show()
            else -> {
                if (isNewTarjeta) {
                    addTarjeta()
                } else {
                    updateTarjeta()
                }
            }
        }
    }

    private fun isValidCard(): Boolean {
        val number = getText(campo_numero)!!.replace(" ", "")
        val type = CreditCardType.getCreditCardType(number)
        val tam = number.length
        return if (type == CreditCardType.Type.MASTER && tam == 16) {
            true
        } else type == CreditCardType.Type.VISA && (tam == 16 || tam == 13)
    }

    private fun existSameName(name: String): Boolean = Realm.getDefaultInstance().where(Tarjeta::class.java).equalTo("cardName", name).findFirst() != null

    private fun expireDateValid(date: String): Boolean {
        val fecha = date.split("/")
        val mes = fecha[0].toInt()
        val year = ("20" + fecha[1]).toInt()
        val calendarY = Calendar.getInstance()!!.get(Calendar.YEAR)
        if (mes == 0 || mes > 12)
            return false
        if (year < calendarY)
            return false
        return true
    }

    private fun initScaner() {
        val scanIntent = Intent(this, CardIOActivity::class.java)
        // customize these values to suit your needs.
        scanIntent.putExtra(CardIOActivity.EXTRA_REQUIRE_EXPIRY, true) // default: false
        scanIntent.putExtra(CardIOActivity.EXTRA_REQUIRE_CVV, true) // default: false
        startActivityForResult(scanIntent, MY_SCAN_REQUEST_CODE)
    }

    @SuppressLint("SetTextI18n")
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == MY_SCAN_REQUEST_CODE) {
            if (data != null && data.hasExtra(CardIOActivity.EXTRA_SCAN_RESULT)) {
                val scanResult: CreditCard = data.getParcelableExtra(CardIOActivity.EXTRA_SCAN_RESULT)
                campo_numero.setText(scanResult.cardNumber)
                if (scanResult.cvv != null) {
                    campo_cvv.setText(scanResult.cvv)
                }
                if (scanResult.isExpiryValid) {
                    val m = scanResult.expiryMonth.toString()
                    val month = if(m.length == 1) "0$m" else m
                    val year = scanResult.expiryYear.toString().substring(2)
                    println("ActivityAddCreditCard.onActivityResult --> $month/$year")
                    campo_expire.setText("$month/$year")
                }
            } else {
                OToast.makeText(this, getString(R.string.error_cargando_datos_tarjeta)).show()
            }
        }
    }

    private fun updateTarjeta() {
        dialogLo.show()
        Realm.getDefaultInstance().executeTransactionAsync({ realmBg ->

            if (checkPrincipal.isChecked) {
                val all = realmBg.where(Tarjeta::class.java).findAll()
                if (all != null && all.isNotEmpty()) {
                    all.forEach { t ->
                        t.isFavorite = false
                        realmBg.copyToRealmOrUpdate(t)
                    }
                }
            }

            val tar = realmBg.where(Tarjeta::class.java).equalTo("id", idTarjeta).findFirst()
            if (tar != null) {
                tar.cardNumber = getText(campo_numero)
                tar.cardName = getText(campo_alias)
                tar.cardOwnerName = getText(campo_owner_name)
                tar.cardCvv = getText(campo_cvv)
                tar.expiryDate = getText(campo_expire)
                tar.idLoc = idLoc
                tar.isFavorite = checkPrincipal.isChecked
                realmBg.copyToRealmOrUpdate(tar)
            }
        }, {
            dialogLo.dismiss()
            clearCampos()
            txt_title.text = getString(R.string.agregar_tarjeta)
            val intent = Intent(this@ActivityAddCreditCard, PopUpSuccess::class.java)
            intent.putExtra(PopUpSuccess.TYPE, PopUpSuccess.TARJETA_UPDATE)
            startActivity(intent)
        }, { t ->
            dialogLo.dismiss()
            t.printStackTrace()
        })
    }

    private fun addTarjeta() {
        dialogLo.show()
        Realm.getDefaultInstance().executeTransactionAsync({ realmBg ->

            if (checkPrincipal.isChecked) {
                val all = realmBg.where(Tarjeta::class.java).findAll()
                if (all != null && all.isNotEmpty()) {
                    all.forEach { t ->
                        t.isFavorite = false
                        realmBg.copyToRealmOrUpdate(t)
                    }
                }
            }
            val tar = realmBg.createObject(Tarjeta::class.java, ApplicationBase.idNext.incrementAndGet())

            tar.cardNumber = getText(campo_numero)
            tar.cardName = getText(campo_alias)
            tar.cardOwnerName = getText(campo_owner_name)
            tar.cardCvv = getText(campo_cvv)
            tar.idLoc = idLoc
            tar.expiryDate = getText(campo_expire)

            tar.isFavorite = checkPrincipal.isChecked
        }, {
            dialogLo.dismiss()
            clearCampos()
            val intent = Intent(this@ActivityAddCreditCard, PopUpSuccess::class.java)
            intent.putExtra(PopUpSuccess.TYPE, PopUpSuccess.TARJETA)
            startActivity(intent)
        }, { t ->
            dialogLo.dismiss()
            t.printStackTrace()
        })
    }

    private fun clearCampos() {
        campo_numero.text = null
        campo_alias.text = null
        campo_owner_name.text = null
        campo_cvv.text = null
        campo_expire.text = null
        location_card_txt.text = null
        checkPrincipal.isChecked = false
    }

    private fun selectCardLocation() {

        val realm: Realm = Realm.getDefaultInstance()
        val locations: RealmResults<Locations> = realm.where(Locations::class.java).findAll()

        /*
        if (locations.isEmpty()) {
            val i = Intent(this, ActivityAddLocation::class.java)
            startActivity(i)
        }
        */

        val locationalias = locations.map { it.alias }.toTypedArray()

        val dialogList: AlertDialog
        val builder = AlertDialog.Builder(this)

        builder.setTitle("Tarjeta")
        builder.setNeutralButton("Agregar nueva ubicación") {dialog, which ->
            dialog.dismiss()

            val i = Intent(this, ActivityAddLocation::class.java)
            i.putExtra("isNewLocation", true)
            startActivity(i)

        }
        builder.setItems(locationalias) { dialog: DialogInterface, which: Int ->
            dialog.dismiss()
            val textLoc = "Ubicación: " + locationalias[which].toString()
            location_card_txt.text = textLoc
            idLoc = locations[which]?.id ?: ""
        }

        dialogList = builder.create()
        dialogList.show()
    }
}
package oomovil.com.appifix.views

import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.graphics.PorterDuff
import android.os.Handler
import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import com.chauthai.swipereveallayout.ViewBinderHelper
import kotlinx.android.synthetic.main.item_button_add_buy.view.*
import kotlinx.android.synthetic.main.item_card_buy.view.*
import kotlinx.android.synthetic.main.item_text_header.view.*
import oomovil.com.appifix.R
import oomovil.com.appifix.general.CreditCardType
import oomovil.com.appifix.general.inflate
import oomovil.com.appifix.models.MakePayment
import oomovil.com.appifix.principal.ActivityAddCreditCard
import oomovil.com.appifix.principal.ActivityRealizarCompra


class AdapterPayment(private val items: List<MakePayment.Payment>, private val context: Context, private val click: ActivityRealizarCompra.OnClickSelected) : RecyclerView.Adapter<AdapterPayment.ViewHolder>() {

    override fun onBindViewHolder(p0: ViewHolder, p1: Int) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    private val titleItem = 0
    private val normalItem = 1
    private val newItem = 2

    private var viewBinderHelper = ViewBinderHelper()

    init {
        viewBinderHelper.setOpenOnlyOne(true)
    }

    override fun getItemCount(): Int = items.size

    override fun getItemViewType(position: Int): Int {
        val i = items[position]
        return when (i.type) {
            MakePayment.PaymentType.TITLE -> titleItem
            MakePayment.PaymentType.NEW -> newItem
            else -> normalItem
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder = when (viewType) {
        titleItem ->
            ViewHolder(parent.inflate(R.layout.item_text_header, false))
        newItem ->
            ViewHolder(parent.inflate(R.layout.item_button_add_buy, false))
        else ->
            ViewHolder(parent.inflate(R.layout.item_card_buy, false))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int, payloads: MutableList<Any>) {
        when (holder.itemViewType) {
            titleItem -> {
                holder.itemView.txt_title_normal.visibility = View.VISIBLE
                holder.itemView.txt_title_shipping.visibility = View.GONE
                holder.itemView.txt_title_normal.text = context.getString(R.string.nav_tarjetas)
            }
            newItem -> {
                holder.itemView.btn_add.text = context.getString(R.string.agregar_tarjeta)
                holder.itemView.btn_add.setOnClickListener {
                    val intent = Intent(context, ActivityAddCreditCard::class.java)
                    intent.putExtra("isNewTarjeta", true)
                    context.startActivity(intent)
                }
            }
            else -> {
                val tarjeta = items[position]
                viewBinderHelper.bind(holder.itemView.swipeRevealLayoutTar, items[position].card.id.toString())

                if (position == 1){
                    Handler().postDelayed({
                        viewBinderHelper.openLayout(tarjeta.card.id.toString())
                        Handler().postDelayed({
                            viewBinderHelper.closeLayout(tarjeta.card.id.toString())
                        }, 500)
                    }, 250)
                }

                holder.itemView.txt_alias.text = tarjeta.card.cardName
                val numberFormat = "**** **** **** ${tarjeta.card.cardNumber!!.split(" ")[3]}"
                holder.itemView.txt_number.text = numberFormat

                val type = CreditCardType.getCreditCardType(tarjeta.card.cardNumber!!.replace(" ", ""))
                when (type) {
                    CreditCardType.Type.VISA -> {
                        holder.itemView.img_card.setImageResource(R.mipmap.visa_)
                        holder.itemView.img_card.colorFilter = null
                    }
                    CreditCardType.Type.MASTER -> {
                        holder.itemView.img_card.setImageResource(R.mipmap.logo_mastercard)
                        holder.itemView.img_card.colorFilter = null
                    }
                    else -> {
                        holder.itemView.img_card.setImageResource(R.mipmap.ic_credit_card)
                        holder.itemView.img_card.setColorFilter(Color.parseColor("#00287f"))
                    }
                }

                holder.itemView.btn_editar_t.setColorFilter(Color.WHITE)

                if (tarjeta.card.isFavorite)
                    holder.itemView.btn_fav.setColorFilter(Color.YELLOW, PorterDuff.Mode.MULTIPLY)
                else
                    holder.itemView.btn_fav.visibility = View.GONE

                if (tarjeta.isSelected)
                    holder.itemView.btn_check.setColorFilter(Color.parseColor("#3e4e94"))
                else
                    holder.itemView.btn_check.colorFilter = null

                holder.itemView.btn_editar_t.setOnClickListener {
                    val intent = Intent(context, ActivityAddCreditCard::class.java)
                    intent.putExtra("isNewTarjeta", false)
                    intent.putExtra("idTarjeta", tarjeta.card.id)
                    context.startActivity(intent)
                }

                holder.itemView.layTarjeta.setOnClickListener {
                    val select = !tarjeta.isSelected
                    items.forEach { l ->
                        l.isSelected = false
                    }
                    tarjeta.isSelected = select
                    notifyDataSetChanged()
                    click.onCLick(tarjeta.card.id.toString(), select)
                }
            }
        }
    }

    class ViewHolder(val view: View) : RecyclerView.ViewHolder(view)
}

package oomovil.com.appifix.principal

import android.annotation.SuppressLint
import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.airbnb.lottie.parser.IntegerParser
import dmax.dialog.SpotsDialog
import io.realm.Realm
import kotlinx.android.synthetic.main.fragment_carrito.*
import oomovil.com.appifix.R
import oomovil.com.appifix.general.AppConfig
import oomovil.com.appifix.general.dialogView
import oomovil.com.appifix.general.formatNumberDecimal
import oomovil.com.appifix.general.round
import oomovil.com.appifix.models.*
import oomovil.com.appifix.oomoviews.OToast
import oomovil.com.appifix.retrofit.ApiInterface
import oomovil.com.appifix.views.AdapterCarrito
import retrofit2.Call
import retrofit2.Response

/**
 * Created by Alan on 29/11/2017.
 */
class FragmentCarrito : Fragment() {

    private var carrito = Realm.getDefaultInstance().where(Carrito::class.java).findFirst()
    private var user = Realm.getDefaultInstance().where(UserModel::class.java).findFirst()
    private var changeArt: OnArticlesChange? = null
    private var fragmentListener: OnFragmentListener? = null
    private var isEmpty = true
    private val dialogLo: AlertDialog by lazy {
        this.activity!!.dialogView()
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        try {
            changeArt = context as OnArticlesChange
            fragmentListener = context as OnFragmentListener
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    interface OnArticlesChange {
        fun onChangeDetected()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? = inflater!!.inflate(R.layout.fragment_carrito, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        recyclerCarrito.layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)

        btn_carrito.setOnClickListener {
            //llamar al fragment de Categorias
            if (isEmpty) {
                fragmentListener?.changeView(OnFragmentListener.FRAGMENT_CATEGORIA, "-1")
            } else {
                // comprar
                val intent = Intent(context, ActivityRealizarCompra::class.java)
                intent.putExtra("total",txt_total.text)
                startActivity(intent)
            }
        }

        btn_codigo.setOnClickListener {
            val i = Intent(context, PopUpCodigo::class.java)
            i.putExtra("pedidoID", "${carrito!!.pedidoID}")
            startActivity(i)
        }
    }

    override fun onPause() {
        super.onPause()

    }

    override fun onResume() {
        super.onResume()
        showProductos()

    }

    private fun showProductos() {
        carrito = Realm.getDefaultInstance().where(Carrito::class.java).findFirst()
        if (carrito == null || carrito!!.productos.isEmpty()) {
            laySinCarrito.visibility = View.VISIBLE
            layProductos.visibility = View.GONE
            btn_carrito.text = getString(R.string.comenzar_a_comprar)
            isEmpty = true
        } else {
            calculateTotals()

            laySinCarrito.visibility = View.GONE
            layProductos.visibility = View.VISIBLE
            btn_carrito.text = getString(R.string.realizar_compra)
            recyclerCarrito.adapter = AdapterCarrito(carrito!!.productos, context!!, object : AdapterCarrito.OnClick {
                override fun onCLick(id: String, idCategoria: String, count: Int) {
                    val intent = Intent(context, ActivityAddCarrito::class.java)
                    intent.putExtra("id", id)
                    intent.putExtra("idCategoria", idCategoria)
                    intent.flags = Intent.FLAG_ACTIVITY_NO_ANIMATION
                    intent.putExtra(ActivityAddCarrito.INTENT_ACTION, ActivityAddCarrito.UPDATE)
                    intent.putExtra(ActivityAddCarrito.INTENT_NUM_PRODS, count)
                    context!!.startActivity(intent)
                }

                override fun onCLick(id: String, idCategoria: String) {
                    eliminarProdToCarrito(id, idCategoria)
                }
            })
            isEmpty = false
        }
    }

    @SuppressLint("SetTextI18n")
    private fun calculateTotals() {

        var total = 0.0
        var subtotal = 0.0

        carrito!!.productos.forEach { prod ->
            val proChido = Realm.getDefaultInstance().where(Product::class.java).equalTo("id", prod.id).findFirst()
            val precio: Double =
                    if (user!!.getUserType() == UserType.NORMAL) proChido!!.precioPublico.toDouble() else proChido!!.precioDistribuidores.toDouble() //TODO WATCH THIS
            total += (precio * prod.count.toInt())
            var descuento = 0.0

            /**
             * Quite multiplicacion de descuento
             */
            /*
            if (proChido.descuento != null && proChido.descuento != "0.00" /*&& prod.count.toInt() >= 3*/) {
                var desc = round(proChido.descuento!!)
                desc /= 100

                descuento = desc * precio
            }
            */

            var t = precio - descuento
            t *= prod.count.toInt()
            subtotal += t //TODO WATCH THIS
        }

        if (!carrito!!.cupon.isNullOrEmpty() && carrito!!.cuponDesc != "0.0") {
            var desCupon = carrito!!.cuponDesc!!.toDouble()
            desCupon /= 100
            val desCup = subtotal * desCupon
            subtotal = round(subtotal - desCup)
        }

        val subTotalT = "" + if (isDoubleInteger(subtotal)) formatNumberDecimal(subtotal.toInt()) else formatNumberDecimal(subtotal)
        val totalT = "" + if (isDoubleInteger(total)) formatNumberDecimal(total.toInt()) else formatNumberDecimal(total)

        txt_subtotal.text = "$$totalT"

        txt_total.text = "$$subTotalT"
    }

    private fun isDoubleInteger(value: Double): Boolean {
        val integer = (value % 1)
        return integer == 0.0
    }

    private fun eliminarProdToCarrito(id: String, idCategoria: String) {
        val builder = AlertDialog.Builder(context)
                .setIcon(R.mipmap.alerta_pop)
                .setTitle(getString(R.string.title_eliminar_articulos))
                .setMessage(getString(R.string.preg_eliminar_prods))
                .setPositiveButton(getString(R.string.eliminar)) { dialog, x ->
                    println("FragmentCarrito.eliminarProdToCarrito --> $x")
                    dialog.dismiss()
                    deleteProduct(id, idCategoria)
                }
                .setNegativeButton(R.string.cancelar) { dialog, x ->
                    println("FragmentCarrito.eliminarProdToCarrito --> $x")
                    dialog.dismiss()
                }
        val alert = builder.create()
        alert!!.show()
    }

    private fun deleteProduct(id: String, idCategoria: String) {
        dialogLo.show()
        val map = HashMap<String, String>()
        map.put("prod_id", id)
        map.put("pedido_id", carrito!!.pedidoID.toString())
        map.put("lang", AppConfig(context!!).language)
        map.put("distribuidor", if (user!!.getUserType() == UserType.PROVIDER) "SI" else "NO")
        println("FragmentCarrito.deleteProduct --> $map")
        ApiInterface.create(context!!).deleteProdFromCar(map).enqueue(object : retrofit2.Callback<Model.ResponseSaveCarrito> {
            override fun onFailure(call: Call<Model.ResponseSaveCarrito>?, t: Throwable?) {
                t?.printStackTrace()
                dialogLo.dismiss()
                OToast.makeText(context!!, R.string.error_conexion).show()
            }

            override fun onResponse(call: Call<Model.ResponseSaveCarrito>?, response: Response<Model.ResponseSaveCarrito>?) {
                if (response != null && response.isSuccessful && response.body() != null && response.body()!!.status == 200) {
                    println("FragmentCarrito.onResponse --> borrar del carrito")
                    val pedidoID = carrito!!.pedidoID
                    Realm.getDefaultInstance().executeTransactionAsync({ realmBg ->
                        val car = realmBg.where(Carrito::class.java).equalTo("pedidoID", pedidoID).findFirst()
                        val carri = response.body()!!.carrito
                        if (carri != null) {
                            car!!.cupon = carri!!.cupon
                            car.cuponDesc = carri.cuponDesc
                            car.subTotal = carri.subTotal
                            car.total = carri.total
                        }
                        val pCarrito = car!!.productos.where().equalTo("id", id).findFirst()
                        if (pCarrito != null) {
                            println("FragmentCarrito.onResponse --> el producto del carrito no es null")
                            pCarrito.deleteFromRealm()
                        } else {
                            println("FragmentCarrito.onResponse --> el producto del carrito es null")
                        }
                        realmBg.copyToRealmOrUpdate(car)
                    }, {
                        println("FragmentCarrito.onResponse --> producto borrado")
                        dialogLo.dismiss()
                        changeArt?.onChangeDetected()
                        showProductos()
                    }, { t ->
                        t.printStackTrace()
                        dialogLo.dismiss()
                        changeArt?.onChangeDetected()
                        showProductos()
                    })
                } else {
                    dialogLo.dismiss()
                    OToast.makeText(context!!, getString(R.string.error_eliminar_producto)).show()
                }
            }

        })
    }
}
package oomovil.com.appifix.firebase

import com.google.firebase.iid.FirebaseInstanceId
import com.google.firebase.iid.FirebaseInstanceIdService
import oomovil.com.appifix.general.AppConfig

/**
 * Created by Alan on 16/11/2017.
 */
class FirebaseIDService : FirebaseInstanceIdService() {

    override fun onTokenRefresh() {
        val refreshedToken = FirebaseInstanceId.getInstance().token
        if (refreshedToken != null)
            sendRegistrationToServer(refreshedToken)
        super.onTokenRefresh()
    }

    private fun sendRegistrationToServer(token: String) {
        // Add custom implementation, as needed.
        if (AppConfig(baseContext).userSession) {
            println("FirebaseIDService.sendRegistrationToServer " + token)
            TokenFireBase().sendToken(token, baseContext)
        }
    }

}
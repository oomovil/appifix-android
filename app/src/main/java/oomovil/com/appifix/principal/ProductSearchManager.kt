package oomovil.com.appifix.principal

import android.app.Activity
import android.content.Context
import android.os.AsyncTask
import android.support.v7.widget.RecyclerView
import android.text.Editable
import android.text.TextWatcher
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import io.realm.Realm
import oomovil.com.appifix.general.AppConfig
import oomovil.com.appifix.general.KeyboardManager
import oomovil.com.appifix.general.getText
import oomovil.com.appifix.models.Model
import oomovil.com.appifix.models.Product
import oomovil.com.appifix.models.UserModel
import oomovil.com.appifix.retrofit.ApiInterface
import oomovil.com.appifix.views.AdapterBusqueda
import retrofit2.Call
import retrofit2.Response

/**
 * Created by Alan on 14/11/2017.
 */
class ProductSearchManager(private val ctx: Activity, private val campoBusqueda: EditText, private val recyclerResult: RecyclerView, private var listenerFragment: OnFragmentListener) : AdapterBusqueda.OnClickSearch {

    private var inputMethodManager: InputMethodManager? = null
    private var call: Call<Model.ResponseSearch>? = null
    private val user = Realm.getDefaultInstance().where(UserModel::class.java).findFirst()
    private val config = AppConfig(ctx)

    init {
        inputMethodManager = ctx.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
        setWatcher()
    }

    private fun setWatcher() {
        campoBusqueda.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {
                // buscar los datos
                beginSearch(getText(campoBusqueda)!!)
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

        })
    }

    private fun beginSearch(text: String) {
        if (call != null)
            call!!.cancel()

        if (text.isEmpty()) {
            showEmpty()
            return
        }

        val map = HashMap<String, String>()
        map.put("email", user!!.email)
        map.put("lang", config.language)
        map.put("to_search", text)

        call = ApiInterface.create(ctx).searchProduct(map)
        call!!.enqueue(object : retrofit2.Callback<Model.ResponseSearch> {
            override fun onResponse(call: Call<Model.ResponseSearch>?, response: Response<Model.ResponseSearch>?) {
                try {
                    println("ProductSearchManager.onResponse --> ${response!!.body()}")
                } catch (e: Exception) {
                    e.printStackTrace()
                }
                if (response != null && response.isSuccessful && response.body() != null && response.body()!!.productos.isNotEmpty()) {
                    recyclerResult.adapter = AdapterBusqueda(response.body()!!.productos, ctx, this@ProductSearchManager)
                } else {
                    showEmpty()
                }
            }

            override fun onFailure(call: Call<Model.ResponseSearch>?, t: Throwable?) {
                t!!.printStackTrace()
                showEmpty()
            }

        })
    }

    override fun onClick(product: Product) {
        inputMethodManager!!.hideSoftInputFromWindow(ctx.currentFocus!!.windowToken, 0)
        Realm.getDefaultInstance().executeTransactionAsync({ realmBG ->
            //var p = realmBG.where(Product::class.java).equalTo("id", product.id).findFirst()
            val p = realmBG.createObject(Product::class.java)
            /*var isNullP = false
            if (p == null) {
                println("ProductSearchManager.onClick --> el producto no existe")
                p = realmBG.createObject(Product::class.java, product.id)
                isNullP = true
            } else {
                println("ProductSearchManager.onClick --> el producto si existe")
            }*/
            p!!.id = product.id
            p.nameEs = product.nameEs
            p.nameEng = product.nameEng
            p.detalleEs = product.detalleEs
            p.detalleEng = product.detalleEng
            p.imagen = product.imagen
            p.precioPublico = product.precioPublico
            p.precioDistribuidores = product.precioDistribuidores
            p.categoriaID = product.categoriaID
            p.descuento = product.descuento
            /*if(!isNullP) {
                realmBG.copyToRealmOrUpdate(p)
            }*/
        }, {
            println("ProductSearchManager.onClick --> se guardó o si existía")
            ctx.onBackPressed()
            campoBusqueda.setText("")
            listenerFragment.changeView(OnFragmentListener.FRAGMENT_PRODUCTO_DETALLE, product.id + "¬oOMovil¬" + product.categoriaID)
        }, { t ->
            t.printStackTrace()
        })
    }

    private fun showEmpty() {
        recyclerResult.adapter = AdapterBusqueda(ArrayList(), ctx, this@ProductSearchManager)
    }

}
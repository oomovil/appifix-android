package oomovil.com.appifix.views

import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.graphics.PorterDuff
import android.os.Handler
import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import com.chauthai.swipereveallayout.ViewBinderHelper
import kotlinx.android.synthetic.main.item_tarjeta.view.*
import oomovil.com.appifix.R
import oomovil.com.appifix.general.AppConfig
import oomovil.com.appifix.general.CreditCardType
import oomovil.com.appifix.general.inflate
import oomovil.com.appifix.models.Tarjeta
import oomovil.com.appifix.principal.ActivityAddCreditCard

/**
 * Created by Alan on 27/10/2017.
 */
class AdapterTarjetas(private var items: List<Tarjeta>?, val context: Context, private val click: OnClick) : RecyclerView.Adapter<AdapterTarjetas.ViewHolder>() {

    override fun onBindViewHolder(p0: ViewHolder, p1: Int) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    private val config = AppConfig(context)

    interface OnClick {
        fun onCLick(id: Int, delete: Boolean)
    }

    private var viewBinderHelper = ViewBinderHelper()

    init {
        viewBinderHelper.setOpenOnlyOne(true)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder = ViewHolder(parent.inflate(R.layout.item_tarjeta, false))

    override fun onBindViewHolder(holder: ViewHolder, position: Int, payloads: MutableList<Any>) {
        val t = items!![position]
        viewBinderHelper.bind(holder.itemView.swipeRevealLayoutTar, "${items!![position].id}")

        if (position == 0) {
            //config.tarjetaShown = false
            Handler().postDelayed({
                viewBinderHelper.openLayout("${items!![position].id}")
                Handler().postDelayed({
                    viewBinderHelper.closeLayout("${items!![position].id}")
                }, 500)
            }, 250)
        }

        val numberFormat = "**** **** **** ${t.cardNumber!!.split(" ")[3]}"

        holder.itemView.txt_number.text = numberFormat
        holder.itemView.txt_alias.text = t.cardName

        holder.itemView.btn_editar_t.setColorFilter(Color.WHITE)
        holder.itemView.btn_borrar_t.setColorFilter(Color.WHITE)

        if (items!![position].isFavorite) {
            holder.itemView.btn_fav.setColorFilter(Color.YELLOW, PorterDuff.Mode.MULTIPLY)
        } else {
            holder.itemView.btn_fav.setColorFilter(Color.BLACK, PorterDuff.Mode.MULTIPLY)
        }

        val type = CreditCardType.getCreditCardType(t.cardNumber!!.replace(" ", ""))
        when (type) {
            CreditCardType.Type.VISA -> {
                holder.itemView.img_card.setImageResource(R.mipmap.visa_)
                holder.itemView.img_card.colorFilter = null
            }
            CreditCardType.Type.MASTER -> {
                holder.itemView.img_card.setImageResource(R.mipmap.logo_mastercard)
                holder.itemView.img_card.colorFilter = null
            }
            else -> {
                holder.itemView.img_card.setImageResource(R.mipmap.ic_credit_card)
                holder.itemView.img_card.setColorFilter(Color.parseColor("#00287f"))
            }
        }

        holder.itemView.btn_editar_t.setOnClickListener {
            val intent = Intent(context, ActivityAddCreditCard::class.java)
            intent.putExtra("isNewTarjeta", false)
            intent.putExtra("idTarjeta", items!![position].id)
            context.startActivity(intent)
        }

        holder.itemView.btn_borrar_t.setOnClickListener {
            click.onCLick(items!![position].id, true)
        }

        holder.itemView.btn_fav.setOnClickListener {
            click.onCLick(items!![position].id, false)
        }

    }

    override fun getItemCount(): Int = items!!.size

    class ViewHolder(val view: View) : RecyclerView.ViewHolder(view)
}
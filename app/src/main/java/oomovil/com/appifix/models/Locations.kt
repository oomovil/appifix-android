package oomovil.com.appifix.models

import com.google.gson.annotations.SerializedName
import io.realm.RealmObject
import io.realm.annotations.PrimaryKey

/**
 * Created by Alan on 21/11/2017.
 */
open class Locations : RealmObject() {
    @PrimaryKey
    @SerializedName("id")
    var id: String? = null
    @SerializedName("alias")
    var alias: String? = null
    @SerializedName("direccion")
    var direccion: String? = null
    @SerializedName("colonia")
    var colonia: String? = null
    @SerializedName("ciudad")
    var ciudad: String? = null
    @SerializedName("estado")
    var estado: String? = null
    @SerializedName("estado_id")
    var estadoId: String? = null
    @SerializedName("cp")
    var cp: String? = null
    @SerializedName("pais_id")
    var paisId: String? = null
    @SerializedName("pais")
    var pais: String? = null
    @SerializedName("dir_principal")
    var principal: String = "NO"
    @SerializedName("telefono")
    var telefono: String? = null

    fun toMyString(): String =
            "Locations(id=$id, alias=$alias, direccion=$direccion, colonia=$colonia, ciudad=$ciudad, estado=$estado, estadoId=$estadoId, cp=$cp, paisId=$paisId), principal=$principal, telefono=$telefono"

}
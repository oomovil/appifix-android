package oomovil.com.appifix.models

import com.google.gson.annotations.SerializedName

/**
 * Created by Martin on 23/02/18.
 */
open class Producto{
    @SerializedName("producto")
    var producto: String? = null
    @SerializedName("precio")
    var precio: String? = null
    @SerializedName("precio_distri")
    var precio_distri: String? = null
    @SerializedName("cantidad")
    var cantidad: String? = null
    @SerializedName("imagen")
    var imagen: String? = null
}

open class DetallePedido{
    @SerializedName("orden")
    var orden: String? = null
    @SerializedName("fecha")
    var fecha: String? = null
    @SerializedName("alias_card")
    var aliasTarjeta: String? = null
    @SerializedName("alias_ubicacion")
    var aliasUbicacion: String? = null
    @SerializedName("total")
    var total: String? = null
    @SerializedName("tipo_envio")
    var tipo_envio: String? = null
    @SerializedName("total_envio")
    var total_envio: String? = null
    @SerializedName("total_prod")
    var total_prod: String? = null
    @SerializedName("num_rastreo")
    var num_rastreo: String? = null
    @SerializedName("dias")
    var dias: String? = null
    @SerializedName("impuestos")
    var impuestos:String="0.0"

    @SerializedName("shippingtype")
    var type: String? = null
    @SerializedName("cupon")
    var cupon: String? = null
    @SerializedName("descuento")
    var descuento: String? = null
}
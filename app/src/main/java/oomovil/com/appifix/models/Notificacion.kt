package oomovil.com.appifix.models

import com.google.gson.annotations.SerializedName
import io.realm.RealmList
import io.realm.RealmObject
import io.realm.annotations.Ignore
import io.realm.annotations.PrimaryKey

/**
 * Created by Alan on 04/12/2017.
 */

open class Notificacion : RealmObject() {
    @PrimaryKey
    open var id : Int = -1
    @SerializedName("titulo")
    open var titulo: String = ""
    @SerializedName("mensaje")
    open var mensaje: String = ""

    @SerializedName("title")
    open var title: String = ""
    @SerializedName("message")
    open var message: String = ""

    @SerializedName("fecha_notif")
    open var fechaNotif: String = ""
    open var images = RealmList<ImgNotis>()

    @SerializedName("imagen1")
    @Ignore
    open var imagen1: String? = null
    @SerializedName("imagen2")
    @Ignore
    open  var imagen2: String? = null
    @SerializedName("imagen3")
    @Ignore
    open var imagen3: String? = null
    @SerializedName("imagen4")
    @Ignore
    open var imagen4: String? = null

}
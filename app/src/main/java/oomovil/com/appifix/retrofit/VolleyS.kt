package oomovil.com.appifix.retrofit

import android.content.Context
import com.android.volley.RequestQueue
import com.android.volley.toolbox.BasicNetwork
import com.android.volley.toolbox.DiskBasedCache
import com.android.volley.toolbox.HurlStack
import com.android.volley.toolbox.Volley

/**
 * Created by Castro-Marquez on 15/02/2018.
 */
class VolleyS (private val context: Context) {

    private val maxCacheSize = 20 * 1024 * 1024

    private var mRequestQueue: RequestQueue?

    init {
        mRequestQueue = Volley.newRequestQueue(context)
    }

    fun getRequest(): RequestQueue {
        if (mRequestQueue == null) {
            val cache = DiskBasedCache(context.cacheDir, maxCacheSize)
            val netWork = BasicNetwork(HurlStack())
            mRequestQueue = RequestQueue(cache, netWork)
            mRequestQueue?.start()
        }
        System.setProperty("http.keepAlive", "false")
        return mRequestQueue!!
    }
}
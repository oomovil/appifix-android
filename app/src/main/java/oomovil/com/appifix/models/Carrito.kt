package oomovil.com.appifix.models

import com.google.gson.annotations.SerializedName
import io.realm.RealmList
import io.realm.RealmObject
import io.realm.annotations.PrimaryKey

/**
 * Created by Alan on 28/11/2017.
 */
open class Carrito : RealmObject() {
    @PrimaryKey
    @SerializedName("pedido_id")
    var pedidoID: Int = -1
    @SerializedName("cupon_id")
    var cuponId: String? = ""
    @SerializedName("cupon")
    var cupon: String? = ""
    @SerializedName("cupon_desc")
    var cuponDesc: String? = ""
    @SerializedName("sub_total")
    var subTotal: Double = 0.0
    @SerializedName("total_pago")
    var total: Double = 0.0
    @SerializedName("productos")
    var productos: RealmList<ProductoCarrito> = RealmList()

    override fun toString(): String = "Carrito(pedidoID=$pedidoID, cuponId='$cuponId', cupon='$cupon', cuponDesc='$cuponDesc', subTotal=$subTotal, total=$total, productos=$productos)"

}
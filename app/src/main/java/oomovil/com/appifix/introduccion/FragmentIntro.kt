package oomovil.com.appifix.introduccion

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.DecelerateInterpolator
import android.view.animation.TranslateAnimation
import kotlinx.android.synthetic.main.fragment_intro.*
import oomovil.com.appifix.R

/**
 * Created by Alan on 17/10/2017.
 */
class FragmentIntro : Fragment() {

    var positionFragment = FRAGMENT_FIRST

    companion object {
        val FRAGMENT_FIRST = 1
        val FRAGMENT_SECOND = 2
        val FRAGMENT_THIRD = 3

        fun getInstance(position: Int): FragmentIntro {
            val b = Bundle()
            b.putInt("position", position)
            val f = FragmentIntro()
            f.arguments = b
            return f
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?):
            View? = inflater!!.inflate(R.layout.fragment_intro, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        positionFragment = arguments!!.getInt("position", FRAGMENT_FIRST)
        createViewFragment()
    }

    private fun createViewFragment() {
        when (positionFragment) {
            FRAGMENT_FIRST -> {
                title_intro.visibility = View.INVISIBLE
                subtitle_intro.text = getString(R.string.intro_fragment_first)
                img_intro.setImageResource(R.mipmap.slide_a)
            }
            FRAGMENT_SECOND -> {
                title_intro.visibility = View.INVISIBLE
                subtitle_intro.text = getString(R.string.intro_fragment_second)
                img_intro.setImageResource(R.mipmap.slide_b)
            }
            FRAGMENT_THIRD -> {
                title_intro.visibility = View.VISIBLE
                title_intro.text = getString(R.string.bienvenido)
                subtitle_intro.visibility = View.INVISIBLE
                img_intro.setImageResource(R.mipmap.slide_c)
            }
        }
    }


}
package oomovil.com.appifix.principal

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.support.v4.app.Fragment
import android.text.Layout.JUSTIFICATION_MODE_INTER_WORD
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import io.realm.Realm
import kotlinx.android.synthetic.main.fragment_producto_detalle.*
import oomovil.com.appifix.R
import oomovil.com.appifix.general.*
import oomovil.com.appifix.models.Product
import oomovil.com.appifix.models.UserModel
import oomovil.com.appifix.models.UserType
import java.text.DecimalFormat

/**
 * Created by Alan on 27/11/2017.
 */
class FragmentProductosDetalle : Fragment() {

    private var id: String? = null
    private var idCategoria: String? = null
    private var product : Product? = null
    private val user = Realm.getDefaultInstance().where(UserModel::class.java).findFirst()
    private var config : AppConfig? = null

    private var listenerFragment: OnFragmentListener? = null

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        try {
            listenerFragment = context as OnFragmentListener
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? = inflater!!.inflate(R.layout.fragment_producto_detalle, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP  && arguments!!.containsKey("transitionNameImg")) {
            val transitionNameImg = arguments!!.getString("transitionNameImg")
            val transitionTxtPrecio = arguments!!.getString("transitionTxtPrecio")
            val transitionTxtTitle = arguments!!.getString("transitionTxtTitle")
            println("FragmentProductosDetalle.onViewCreated --> $transitionNameImg")
            imgProducto.transitionName = transitionNameImg
            txt_precio.transitionName = transitionTxtPrecio
            title_prod.transitionName = transitionTxtTitle
        }

        val results = arguments!!.getString("id").split( "¬oOMovil¬")
        id = results[0]
        idCategoria = results[1]

        println("FragmentProductosDetalle.onViewCreated --> $id")

        config = AppConfig(context!!)
        loadProductData()

        btn_add.setOnClickListener {

            //TODO Desactivar boton
            btn_add.isClickable = !(btn_add.hasOnClickListeners())

            val intent = Intent(context, ActivityAddCarrito::class.java)
            intent.putExtra("id", id!!)
            intent.putExtra("idCategoria", idCategoria!!)
            intent.flags = Intent.FLAG_ACTIVITY_NO_ANIMATION
            intent.putExtra(ActivityAddCarrito.INTENT_ACTION, ActivityAddCarrito.ADD)
            startActivityForResult(intent, 150)
        }
    }

    @SuppressLint("SetTextI18n")
    private fun loadProductData() {
        product = Realm.getDefaultInstance().where(Product::class.java).equalTo("id", id!!).equalTo("categoriaID", idCategoria!!).findFirst()
        if (product != null) {

            imgProducto.loadUrl(product!!.imagen, R.drawable.img_no_disponible, null)

            val precio: Double = if (user!!.getUserType() == UserType.NORMAL) round(product!!.precioPublico) else round(product!!.precioDistribuidores)
            txt_precio.text = "$" + if (isDoubleInteger(precio)) formatNumberDecimal((precio.toInt())) else formatNumberDecimal((precio))
            if (product!!.descuento != null && product!!.descuento != "0.00") {
                val desc = round(product!!.descuento!!)
                val descChido = "" + if (isDoubleInteger(desc)) desc.toInt() else {desc}
               txt_desc.text = "$descChido% ${if (config!!.language == "ES") " desc" else " off"}"
            } else {
                txt_desc.visibility = View.GONE
            }
            title_prod.text = if (config!!.language == "ES") product!!.nameEs else product!!.nameEng
            val descript = if (config!!.language == "ES") product!!.detalleEs else product!!.detalleEng
            descrip_prod.text = getTextFromHtml(descript)
            JustifyTextView.justify(descrip_prod)
        }
    }

    private fun isDoubleInteger(value: Double) : Boolean{
        val integer = (value % 1)
        return integer == 0.0
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 150 && resultCode == Activity.RESULT_OK) {
            listenerFragment?.changeView(OnFragmentListener.FRAGMENT_CARRITO, "-1")
        }
    }
}
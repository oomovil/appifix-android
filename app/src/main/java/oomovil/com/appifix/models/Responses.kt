package oomovil.com.appifix.models

import com.google.gson.annotations.SerializedName
import retrofit2.Call
import java.util.*

/**
 * Created by Alan on 02/11/2017.
 */

object Model {

    open class User(@SerializedName("id") var id: String, @SerializedName("email") var email: String, @SerializedName("nombre") var nombre: String, @SerializedName("pass") var pass: String?,
                    @SerializedName("foto") var foto: String?, @SerializedName("fb_reg") var fb_reg: String, @SerializedName("google_reg") var google_reg: String,
                    @SerializedName("tipo_usuario_id") var tipo_usuario_id: String, @SerializedName("ubicaciones") var locations: List<Locations>, @SerializedName("carrito") var carrito: Carrito?) {
        override fun toString(): String = "User(id='$id', email='$email', nombre='$nombre', pass=$pass, foto=$foto, fb_reg='$fb_reg', google_reg='$google_reg', tipo_usuario_id='$tipo_usuario_id', locations=$locations, carrito=$carrito)"
    }

    open class Cupon(@SerializedName("id") var id: String, @SerializedName("cupon") var cupon: String, @SerializedName("descuento") var descuento: String)

    data class Response(@SerializedName("status") var status: Int, @SerializedName("mensaje") var message: String)
    data class ResponseOrden(@SerializedName("status") var status: Int, @SerializedName("ok") var ok: Int)
    data class ResponsePreferences(@SerializedName("status") var status: Int, @SerializedName("prefs") var prefs: List<Preferences>)
    data class ResponseMyPreferences(@SerializedName("status") var status: Int, @SerializedName("preferencias") var prefs: List<Preferences>)

    data class ResponseRegistro(@SerializedName("status") var status: Int,
                                @SerializedName("mensaje") var message: String,
                                @SerializedName("id") var userId: Int,
                                @SerializedName("email") var email: String,
                                @SerializedName("nombre") var nombre: String,
                                @SerializedName("foto") var foto: String?)

    data class ResponseUploadPicture(@SerializedName("status") var status: Int, @SerializedName("mensaje") var message: String, @SerializedName("img_status") var img_status: Int, @SerializedName("img_msg") var img_msg: String, @SerializedName("picture") var picture: String)
    data class ResponseLogin(@SerializedName("status") var status: Int, @SerializedName("mensaje") var message: String, @SerializedName("user") var user: User)
    data class ResponsePaises(@SerializedName("status") var status: Int, @SerializedName("paises") var paises: List<Pais>)
    data class ResponseCategorias(@SerializedName("status") var status : Int, @SerializedName("cats") var prefs: List<Categoria>)
    data class ResponseAddLocation(@SerializedName("status") var status: Int, @SerializedName("mensaje") var message: String, @SerializedName("id") var id: String)

    data class ResponseProductFromCategory(@SerializedName("status") var status: Int, @SerializedName("productos") val productos: List<Product>)

    data class ResponseSaveCarrito(@SerializedName("status") var status: Int, @SerializedName("mensaje") var message: String, @SerializedName("carrito") var carrito: Carrito?)
    data class ResponseNotis(@SerializedName("status") var status: Int, @SerializedName("mensaje") var message: String, @SerializedName("response") var notis: List<Notificacion>)
    data class ResponseSearch(@SerializedName("status") var status: Int, @SerializedName("productos") val productos: List<Product>)

    data class CarritoR(@SerializedName("cupon_id") var cupon_id: String, @SerializedName("cupon") var cupon: String, @SerializedName("cupon_desc") var cupon_desc: String)
    data class ResponseCupon(@SerializedName("status") var status: Int, @SerializedName("mensaje") var message: String, @SerializedName("carrito") var carrito: CarritoR)

    data class ResponseCuotasPaqueterias(@SerializedName("status") var status: Int, @SerializedName("paqueterias") var envios: List<Envio>)

    data class ResponseHistorial(@SerializedName("status") var status: Int, @SerializedName("ords") var historial: List<Pedido>)
    data class ResponseDetalleHistorial(@SerializedName("status") var status: Int, @SerializedName("productos") var productos: List<Producto>, @SerializedName("detalles") var detalle: DetallePedido)
    data class ResponseStates(@SerializedName("status") var status: Int, @SerializedName("estados") var states: List<Estado>)

    data class ResponseOrderTotal(@SerializedName("status") var status: Int, @SerializedName("getOrderTotal") var total: Double)

    data class ResponseProductsById(@SerializedName("status") var status: Int, @SerializedName("producto") val producto: Product)

}
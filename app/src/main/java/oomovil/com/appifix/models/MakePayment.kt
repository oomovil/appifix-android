package oomovil.com.appifix.models

import com.bignerdranch.expandablerecyclerview.model.Parent

/**
 * Created by Alan on 06/12/2017.
 */
object MakePayment {
    data class Address(var locations: Locations?, var type: AddressType, var isSelected: Boolean)
    data class Shipping(var type: ShippingType, var name: String, var imge: Int, var additionalCost: Double, var isSelected: Boolean)

    data class Payment(var type: PaymentType, var isSelected: Boolean, var card: Tarjeta)

    enum class PaymentType {
        TITLE, CREDIT_CARD, NEW
    }

    enum class ShippingType {
        TITLE, NORMAL
    }

    enum class AddressType {
        TITLE, EXISTING, NEW, LOCAL
    }
}
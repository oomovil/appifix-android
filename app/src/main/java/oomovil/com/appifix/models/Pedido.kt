package oomovil.com.appifix.models

import com.google.gson.annotations.SerializedName
import io.realm.RealmObject
import io.realm.annotations.PrimaryKey

/**
 * Created by Martin on 20/02/18.
 */
open class Pedido : RealmObject(){
    @PrimaryKey
    @SerializedName("id")
    var id: String? = null
    @SerializedName("fecha")
    var date: String? = null
    @SerializedName("estatus")
    var status: String? = null
    @SerializedName("total")
    var total: String? = null
}
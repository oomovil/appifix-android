package oomovil.com.appifix.views

import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.graphics.PorterDuff
import android.os.Handler
import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import io.realm.RealmResults
import kotlinx.android.synthetic.main.item_locations.view.*
import oomovil.com.appifix.R
import oomovil.com.appifix.general.inflate
import oomovil.com.appifix.models.Locations
import com.chauthai.swipereveallayout.ViewBinderHelper
import oomovil.com.appifix.general.AppConfig
import oomovil.com.appifix.principal.ActivityAddLocation


/**
 * Created by Alan on 21/11/2017.
 */
class AdapterLocations(private var items: RealmResults<Locations>?, val context: Context, private val click: OnClick) : RecyclerView.Adapter<AdapterLocations.ViewHolder>() {

    override fun onBindViewHolder(p0: ViewHolder, p1: Int) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    private val config = AppConfig(context)

    interface OnClick {
        fun onCLick(id: String, delete: Boolean)
    }

    private var viewBinderHelper = ViewBinderHelper()

    init {
        viewBinderHelper.setOpenOnlyOne(true)
    }

    override fun getItemCount(): Int = items!!.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder = ViewHolder(parent.inflate(R.layout.item_locations, false))

    override fun onBindViewHolder(holder: ViewHolder, position: Int, payloads: MutableList<Any>) {

        viewBinderHelper.bind(holder.itemView.swipeRevealLayout, "${items!![position]!!.id}")

        if (position == 0) {
            //config.locationShown = false
            Handler().postDelayed({
                viewBinderHelper.openLayout("${items!![position]!!.id}")
                Handler().postDelayed({
                    viewBinderHelper.closeLayout("${items!![position]!!.id}")
                }, 500)
            }, 250)
        }

        holder.itemView.txt_alias.text = items!![position]!!.alias
        holder.itemView.txt_calle.text = items!![position]!!.direccion
        holder.itemView.txt_colonia.text = items!![position]!!.colonia
        var ciudad = ""
        if(items!![position]!!.cp!!.isEmpty()){
            ciudad = "${items!![position]!!.ciudad}, ${items!![position]!!.estado}"
        }else{
            ciudad = "${items!![position]!!.ciudad}, ${items!![position]!!.estado}. C.P. ${items!![position]!!.cp}"
        }
        holder.itemView.txt_ciudad.text = ciudad

        holder.itemView.btn_editar.setColorFilter(Color.WHITE)
        holder.itemView.btn_borrar.setColorFilter(Color.WHITE)

        if (items!![position]!!.principal == "SI") {
            holder.itemView.btnFav.setColorFilter(Color.YELLOW, PorterDuff.Mode.MULTIPLY)
        } else {
            holder.itemView.btnFav.setColorFilter(Color.BLACK, PorterDuff.Mode.MULTIPLY)
        }


        holder.itemView.btn_editar.setOnClickListener {
            val intent = Intent(context, ActivityAddLocation::class.java)
            intent.putExtra("isNewLocation", false)
            intent.putExtra("idLocation", items!![holder.adapterPosition]!!.id)
            context.startActivity(intent)
        }

        holder.itemView.btn_borrar.setOnClickListener {
            click.onCLick(items!![position]!!.id!!, true)
        }

        holder.itemView.btnFav.setOnClickListener {
            click.onCLick(items!![position]!!.id!!, false)
        }
    }

    class ViewHolder(val view: View) : RecyclerView.ViewHolder(view)
}
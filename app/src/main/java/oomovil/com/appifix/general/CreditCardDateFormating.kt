package oomovil.com.appifix.general

import android.text.Editable
import android.text.InputFilter
import android.text.TextWatcher

/**
 * Created by Alan on 22/11/2017.
 */
class CreditCardDateFormating : TextWatcher {

    private var current = ""

    override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

    }

    override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

    }

    override fun afterTextChanged(s: Editable) {
        if (s.toString() != current) {
            val userInput = s.toString().replace("/".toRegex(), "")
            if (userInput.length <= 4) {
                val sb = StringBuilder()
                for (i in 0 until userInput.length) {
                    if (i % 2 == 0 && i > 0) {
                        sb.append("/")
                    }
                    sb.append(userInput[i])
                }
                current = sb.toString()

                s.filters = arrayOfNulls<InputFilter>(0)
            }
            s.replace(0, s.length, current, 0, current.length)
        }
    }

}